import argparse
import subprocess
from threading import Thread

from botmanlib import __version__


def format_output(process, prefix):
    while True:
        line = process.stdout.readline().decode("utf-8").rstrip(' ')
        if line:
            print(prefix, line, end="")
        elif not line and process.poll() is not None:
            break


def build_stretch(deploy_username, deploy_token):
    python_stretch_image_pull_command = ["docker", "pull", f"python:3.6-stretch"]
    build_stretch_command = ["docker", "build", "-f", "Dockerfile-stretch",  "--build-arg", f"DEPLOY_USERNAME={deploy_username}", "--build-arg", f"DEPLOY_TOKEN={deploy_token}",  "-t", f"registry.gitlab.com/botman_bots/botmanlib:{__version__}-stretch", "."]
    stretch_push_command = ["docker", "push", f"registry.gitlab.com/botman_bots/botmanlib:{__version__}-stretch"]

    stretch_pull_process = subprocess.Popen(python_stretch_image_pull_command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    format_output(stretch_pull_process, "\033[1;32;48mSTRETCH:")

    stretch_build_process = subprocess.Popen(build_stretch_command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    format_output(stretch_build_process, "\033[1;32;48mSTRETCH:")
    if stretch_build_process.returncode == 0:
        stretch_push_process = subprocess.Popen(stretch_push_command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        format_output(stretch_push_process, "\033[1;32;48mSTRETCH:")


def build_alpine(deploy_username, deploy_token):
    python_alpine_image_pull_command = ["docker", "pull", f"python:3.6-alpine"]
    build_alpine_command = ["docker", "build", "-f", "Dockerfile-alpine", "--build-arg", f"DEPLOY_USERNAME={deploy_username}", "--build-arg", f"DEPLOY_TOKEN={deploy_token}", "-t", f"registry.gitlab.com/botman_bots/botmanlib:{__version__}-alpine", "."]
    alpine_push_command = ["docker", "push", f"registry.gitlab.com/botman_bots/botmanlib:{__version__}-alpine"]

    alpine_pull_process = subprocess.Popen(python_alpine_image_pull_command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    format_output(alpine_pull_process, "\033[1;32;48mALPINE:")

    alpine_build_process = subprocess.Popen(build_alpine_command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    format_output(alpine_build_process, "\033[1;34;48mALPINE:")
    if alpine_build_process.returncode == 0:
        alpine_push_process = subprocess.Popen(alpine_push_command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        format_output(alpine_push_process, "\033[1;34;48mALPINE:")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('-u', '--deploy-username', dest='username', type=str, help='Deploy username')

    parser.add_argument('-t', '--deploy-token', dest='token', type=str, help='Deploy token')

    args = parser.parse_args()

    build_stretch_thread = Thread(target=build_stretch, args=(args.username, args.token))
    build_alpine_thread = Thread(target=build_alpine, args=(args.username, args.token))

    build_stretch_thread.start()
    build_alpine_thread.start()

    build_stretch_thread.join()
    build_alpine_thread.join()
