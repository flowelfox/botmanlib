from abc import ABC

from telegram import InlineKeyboardButton, InlineKeyboardMarkup, TelegramError, Update
from telegram.ext import ConversationHandler, CallbackQueryHandler, MessageHandler, Filters, CallbackContext

from botmanlib.messages import send_or_edit, delete_interface, remove_interface
from .list_menu import ListMenu
from ..helpers import remove_from_db
from ...models import Database


class OneListMenu(ListMenu, ABC):
    """Use this class to create menu for listing any objects. Each page represents one objects.

    Attributes:
        menu_name (:obj:`str`): Menu name which would be used in user_data and some buttons callback data
        search_key_param (:obj:`str`, optional): Object attribute which value would be used to filter data
        model: Listing model. Needed if add_delete_button == True
        add_delete_button (:obj:`bool`, optional): When True, delete button would be added to message. To change delete button use :meth:`delete_button` method
    """
    menu_name = 'one_list_menu'
    search_key_param = None
    model = None
    add_delete_button = False

    def message_text(self, context: CallbackContext, obj):
        """Method that returns message text for current selected object

        Args:
            context (:class:`~telegram.ext.CallbackContext`): Context from callback.
            obj: Current selected object

        Returns:
            (:obj:`str`): Text which would be displayed in telegram message for `obj`
        """
        _ = context.user_data['user'].translator

        if obj:
            message_text = _("Object") + "{id}".format(id=obj.id) + '\n'
        else:
            message_text = _("There is nothing to list") + '\n'

        return message_text

    def search_message_text(self, context: CallbackContext, obj):
        """Method that returns message text when user enter in search state and search is not empty

        Args:
            context (:class:`~telegram.ext.CallbackContext`): Context from callback.
            obj: Current selected object

        Returns:
            (:obj:`str`): Text which would be displayed in search message 
        """
        _ = context.user_data['user'].translator
        _ = context.user_data['_']
        return None

    def _load(self, context: CallbackContext):
        user_data = context.user_data
        if self.menu_name not in user_data:
            user_data[self.menu_name] = {}
        if self.reset_on_entry or 'selected_object' not in user_data[self.menu_name]:
            user_data[self.menu_name]['selected_object'] = 0

        if self.reset_on_entry or 'objects' not in user_data[self.menu_name]:
            user_data[self.menu_name]['objects'] = []

        if self.reset_on_entry or 'filtered_objects' not in user_data[self.menu_name]:
            user_data[self.menu_name]['filtered_objects'] = []

        if self.reset_on_entry or 'max_page' not in user_data[self.menu_name]:
            user_data[self.menu_name]['max_page'] = 0

    def delete_button(self, context: CallbackContext):
        """Method that returns message inline delete button when add_delete_button == True

        Args:
            context (:class:`~telegram.ext.CallbackContext`): Context from callback.

        Returns:
            (:class:`~telegram.InlineKeyboardButton`): Text which would be displayed in search message
        """

        _ = context.user_data['user'].translator
        return InlineKeyboardButton(_("Delete"), callback_data=f'delete_{self.menu_name}')

    def selected_object(self, context):
        """Use this method to get current selected object

        Args:
            context (:class:`~telegram.ext.CallbackContext`): Context from callback.
        """
        objects = self._get_objects(context)
        if len(objects) > context.user_data[self.menu_name]['selected_object']:
            return objects[context.user_data[self.menu_name]['selected_object']]
        else:
            return None

    def object_buttons(self, context: CallbackContext, obj):
        """Forms a buttons list which linked to objects

        Args:
            context (:class:`~telegram.ext.CallbackContext`): Context from callback.
            obj: Current selected object

        Returns:
            (List[List[:class:`~telegram.InlineKeyboardButton`]]]): List of buttons
        """
        return []

    def send_message(self, context: CallbackContext):
        """Collecting all message related data and sends this telegram message to user

        Args:
            context (:class:`~telegram.ext.CallbackContext`): Context from callback.

        Returns:
            (:class:`~telegram.Message`): Sent telegram message or exception
        """

        user_data = context.user_data
        user = user_data['user']

        back_button = self.back_button(context)

        if user_data[self.menu_name]['filtered_objects']:
            back_button = self.search_back_button(context)

        objects = self._get_objects(context)

        buttons = []
        # add user defined top buttons
        top_buttons = self.top_buttons(context)
        if top_buttons:
            buttons.extend(top_buttons)

        if not objects:
            # define message text for empty menu
            message_text = None
            if user_data[self.menu_name]['filtered_objects']:
                message_text = self.search_message_text(context, None)

            if message_text is None:
                message_text = self.message_text(context, None)

            # define list controls
            controls = self.controls(context)
            if controls:
                buttons.append(controls)

            # add additional user buttons
            object_buttons = self.object_buttons(context, None)
            if object_buttons:
                buttons.extend(object_buttons)
        else:
            # get current selected object
            selected_object = objects[user_data[self.menu_name]['selected_object']]

            # define message text when objects exists
            message_text = None
            if user_data[self.menu_name]['filtered_objects']:
                message_text = self.search_message_text(context, selected_object)

            if message_text is None:
                message_text = self.message_text(context, selected_object)

            # define list controls
            controls = self.controls(context, o=selected_object)
            if controls:
                buttons.append(controls)

            # add additional user buttons
            object_buttons = self.object_buttons(context, selected_object)
            if object_buttons:
                buttons.extend(object_buttons)

            # add delete button if needed
            delete_button = self.delete_button(context)
            if self.add_delete_button and delete_button:
                buttons.append([delete_button])

            # add page text for message text
            message_text += self.page_text(user_data[self.menu_name]['selected_object'] + 1, user_data[self.menu_name]['max_page'], context)

        # add back button
        if back_button:
            buttons.append([back_button])

        # add user defined bottom buttons
        bottom_buttons = self.bottom_buttons(context)
        if bottom_buttons:
            buttons.extend(bottom_buttons)

        markup = InlineKeyboardMarkup(buttons)

        # try to send message
        try:
            return send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=markup, parse_mode=self.parse_mode, disable_web_page_preview=self.disable_web_page_preview)
        except TelegramError as e:
            if e.message.startswith("Can't parse entities"):
                message_text = message_text.replace('\\', "")
                return send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=markup, parse_mode=None, disable_web_page_preview=self.disable_web_page_preview)
            else:
                raise e

    def next_object(self, update: Update, context: CallbackContext):
        """Switching current selected object to next object.
        When current object is last in list then next would be first object from list.

        Args:
            update (:class:`~telegram.Update`): Update from callback.
            context (:class:`~telegram.ext.CallbackContext`): Context from callback.

        Returns:
            (:class:`enum.Enum`): Next state, usualy self.States.ACTION
        """

        user_data = context.user_data
        size = user_data[self.menu_name]['max_page'] - 1

        if user_data[self.menu_name]['selected_object'] < size:
            user_data[self.menu_name]['selected_object'] += 1
        else:
            user_data[self.menu_name]['selected_object'] = 0

        self.send_message(context)
        self.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def prev_object(self, update: Update, context: CallbackContext):
        """Switching current selected object to previous object.
        When current object is first in list then previous would be last object from list.

        Args:
            update (:class:`~telegram.Update`): Update from callback.
            context (:class:`~telegram.ext.CallbackContext`): Context from callback.

        Returns:
            (:class:`enum.Enum`): Next state, usualy self.States.ACTION
        """

        user_data = context.user_data
        size = user_data[self.menu_name]['max_page'] - 1

        if user_data[self.menu_name]['selected_object'] > 0:
            user_data[self.menu_name]['selected_object'] -= 1
        else:
            user_data[self.menu_name]['selected_object'] = size

        self.send_message(context)
        self.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def search(self, update: Update, context: CallbackContext):
        """Callback method for filtering all menu objects by user text

        Args:
            update (:class:`~telegram.Update`): Update from callback.
            context (:class:`~telegram.ext.CallbackContext`): Context from callback.
        """
        user_data = context.user_data
        user = user_data['user']
        text = update.message.text

        objects = self.query_objects(context)

        filtered_objects = []
        for obj in objects:
            value = getattr(obj, self.search_key_param, False)
            if value and text.lower() in value.lower():
                filtered_objects.append(obj)

        if filtered_objects:
            user_data[self.menu_name]['filtered_objects'] = filtered_objects
            user_data[self.menu_name]['selected_object'] = 0
            delete_interface(context)
            self.send_message(context)
        else:
            delete_interface(context)
            send_or_edit(context, chat_id=user.chat_id, text=self.empty_search_text(context))
            remove_interface(context)
            self.send_message(context)

        return self.States.ACTION

    def delete_text(self, context: CallbackContext):
        """Method that returns message text about asking confirmation when user about deleting object

        Args:
            context (:class:`~telegram.ext.CallbackContext`): Context from callback.

        Returns:
            (:obj:`str`): Text which would be displayed when :meth:`delete_ask` callback called.
        """

        _ = context.user_data['user'].translator
        return _("Are you sure you want to delete this object?")

    def after_delete_text(self, context: CallbackContext):
        """Method that returns message text which would be displayed after objects was deleted.

        Args:
            context (:class:`~telegram.ext.CallbackContext`): Context from callback.

        Returns:
            (:obj:`str`): Text which would be displayed when :meth:`delete` callback called.
        """
        _ = context.user_data['user'].translator
        return _("Object deleted")

    def delete_ask(self, update: Update, context: CallbackContext):
        """Callback method for asking confirmation about deletion of selected object

        Args:
            update (:class:`~telegram.Update`): Update from callback.
            context (:class:`~telegram.ext.CallbackContext`): Context from callback.
        """
        user = context.user_data['user']
        _ = context.user_data['user'].translator

        buttons = [[InlineKeyboardButton(_("Yes"), callback_data=f'delete_yes_{self.menu_name}'),
                    InlineKeyboardButton(_("No"), callback_data=f'delete_no_{self.menu_name}')]]

        send_or_edit(context, chat_id=user.chat_id, text=self.delete_text(context), reply_markup=InlineKeyboardMarkup(buttons))

        self.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def delete(self, update: Update, context: CallbackContext):
        """Callback method for making deletion of selected object

        Args:
            update (:class:`~telegram.Update`): Update from callback.
            context (:class:`~telegram.ext.CallbackContext`): Context from callback.
        """
        user_data = context.user_data
        data = update.callback_query.data

        if data == f'delete_yes_{self.menu_name}':
            objects = self._get_objects(context)
            obj = objects[user_data[self.menu_name]['selected_object']]
            session = Database().sessionmaker.object_session(obj)

            if not remove_from_db(obj, session):
                return self.conv_fallback(context)

            self.bot.answer_callback_query(update.callback_query.id, text=self.after_delete_text(context), show_alert=True)
            delete_interface(context)
            self.update_objects(context)
            user_data[self.menu_name]['selected_object'] = 0
            return self.prev_object(update, context)

        elif data == f'delete_no_{self.menu_name}':
            self.bot.answer_callback_query(update.callback_query.id)
            user_data[self.menu_name]['delete_id'] = None

        self.send_message(context)
        return self.States.ACTION

    def get_handler(self):
        search_handlers = []

        control_handlers = [CallbackQueryHandler(self.next_object, pattern=f'next_object_{self.menu_name}'),
                            CallbackQueryHandler(self.prev_object, pattern=f'prev_object_{self.menu_name}'),
                            CallbackQueryHandler(self.delete_ask, pattern=f'^delete_{self.menu_name}$'),
                            CallbackQueryHandler(self.delete, pattern=f'^delete_(yes|no)_{self.menu_name}$'),
                            CallbackQueryHandler(self.back, pattern=f"back_{self.menu_name}")]
        search_handlers.append(CallbackQueryHandler(self.exit_search, pattern=f'exit_search_{self.menu_name}'))

        search_handlers.append(MessageHandler(Filters.text, self.search))

        states = {self.States.ACTION: control_handlers +
                                      (search_handlers if self.search_key_param is not None else [])
                  }

        additional_states = self.additional_states()
        for key in additional_states:
            if key in states:
                states[key] = additional_states[key] + states[key]
            else:
                states.update({key: additional_states[key]})

        handler = ConversationHandler(entry_points=self.entry_points(),
                                      states=states,
                                      fallbacks=self.fallbacks(),
                                      allow_reentry=self.allow_reentry)

        return handler
