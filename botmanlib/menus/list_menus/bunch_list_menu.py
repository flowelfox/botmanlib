from abc import ABC

from telegram import InlineKeyboardMarkup, TelegramError, Update
from telegram.ext import ConversationHandler, CallbackQueryHandler, MessageHandler, Filters, CallbackContext

from botmanlib.messages import send_or_edit, delete_interface, remove_interface
from .list_menu import ListMenu


class BunchListMenu(ListMenu, ABC):
    menu_name = 'bunch_list_menu'
    search_key_param = None
    objects_per_page = 6
    model = None

    def message_text(self, context: CallbackContext, objects):
        """Method that returns message text for current objects page

        Args:
            context (:class:`~telegram.ext.CallbackContext`): Context from callback.
            objects: Objects of current page

        Returns:
            (:obj:`str`): Text which would be displayed in telegram message for `obj`
        """
        _ = context.user_data['user'].translator

        if objects:
            message_text = ""
            for obj in objects:
                message_text += "Object {id}\n".format(id=obj.id)
        else:
            message_text = _("There is nothing to list") + '\n'

        return message_text

    def search_message_text(self, context: CallbackContext, page_objects):
        _ = context.user_data['user'].translator
        """Method that return text when something found

        Args:
            context (:obj:`CallbackContext`): Context from callback.
            page_objects (:obj:): Objects for current page

        Returns:
            (:obj:`str`): Text which would be displayed in search message 
        """
        return None

    def _load(self, context: CallbackContext):
        user_data = context.user_data
        if self.menu_name not in user_data:
            user_data[self.menu_name] = {}

        if self.reset_on_entry or 'page' not in user_data[self.menu_name]:
            user_data[self.menu_name]['page'] = 0

        if self.reset_on_entry or 'objects' not in user_data[self.menu_name]:
            user_data[self.menu_name]['objects'] = []

        if self.reset_on_entry or 'filtered_objects' not in user_data[self.menu_name]:
            user_data[self.menu_name]['filtered_objects'] = []

        if self.reset_on_entry or 'max_page' not in user_data[self.menu_name]:
            user_data[self.menu_name]['max_page'] = 0

    def send_message(self, context: CallbackContext):
        """Collecting all message related data and sends this telegram message to user

        Args:
            context (:class:`~telegram.ext.CallbackContext`): Context from callback.

        Returns:
            (:class:`~telegram.Message`): Sent telegram message or exception
        """

        user = context.user_data['user']

        back_button = self.back_button(context)

        if context.user_data[self.menu_name]['filtered_objects']:
            back_button = self.search_back_button(context)

        objects = self._get_objects(context)

        buttons = []
        # add user defined top buttons
        top_buttons = self.top_buttons(context)
        if top_buttons:
            buttons.extend(top_buttons)

        if not objects:
            # define message text for empty menu
            message_text = None
            if context.user_data[self.menu_name]['filtered_objects']:
                message_text = self.search_message_text(context, None)

            if message_text is None:
                message_text = self.message_text(context, None)

            # define list controls
            controls = self.controls(context)
            if controls:
                buttons.append(controls)

            # add additional user buttons
            object_buttons = self.object_buttons(context, [])
            if object_buttons:
                buttons.extend(object_buttons)
        else:
            # get objects of current page
            page_objects = objects[context.user_data[self.menu_name]['page'] * self.objects_per_page: context.user_data[self.menu_name]['page'] * self.objects_per_page + self.objects_per_page]

            # define message text when objects exists
            message_text = None
            if context.user_data[self.menu_name]['filtered_objects']:
                message_text = self.search_message_text(context, page_objects)

            if message_text is None:
                message_text = self.message_text(context, page_objects)

            # define list controls
            controls = self.controls(context, o=page_objects)
            if controls:
                buttons.append(controls)

            # add additional user buttons
            object_buttons = self.object_buttons(context, page_objects)
            if object_buttons:
                buttons.extend(object_buttons)

            # add page text for message text
            message_text += self.page_text(context.user_data[self.menu_name]['page'] + 1, context.user_data[self.menu_name]['max_page'], context)

        # add back button
        if back_button:
            buttons.append([back_button])

        # add user defined bottom buttons
        bottom_buttons = self.bottom_buttons(context.user_data)
        if bottom_buttons:
            buttons.extend(bottom_buttons)

        markup = InlineKeyboardMarkup(buttons)

        # try to send message
        try:
            return send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=markup, parse_mode=self.parse_mode, disable_web_page_preview=self.disable_web_page_preview)
        except TelegramError as e:
            if e.message.startswith("Can't parse entities"):
                return send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=markup, parse_mode=None, disable_web_page_preview=self.disable_web_page_preview)
            else:
                raise e

    def next_page(self, update: Update, context: CallbackContext):
        """Switching current page to next page.
        When current page is last page then next would be first page.

        Args:
            update (:class:`~telegram.Update`): Update from callback.
            context (:class:`~telegram.ext.CallbackContext`): Context from callback.

        Returns:
            (:class:`enum.Enum`): Next state, usualy self.States.ACTION
        """
        user_data = context.user_data
        max_page = user_data[self.menu_name]['max_page'] - 1

        page = user_data[self.menu_name]['page']
        if user_data[self.menu_name]['page'] < max_page:
            user_data[self.menu_name]['page'] += 1
        else:
            user_data[self.menu_name]['page'] = 0

        if page != user_data[self.menu_name]['page']:
            self.send_message(context)

        if update.callback_query:
            self.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def prev_page(self, update: Update, context: CallbackContext):
        """Switching current page to previous page.
        When current page is first page then previous would be last page.

        Args:
            update (:class:`~telegram.Update`): Update from callback.
            context (:class:`~telegram.ext.CallbackContext`): Context from callback.

        Returns:
            (:class:`enum.Enum`): Next state, usualy self.States.ACTION
        """
        user_data = context.user_data
        max_page = user_data[self.menu_name]['max_page'] - 1

        page = user_data[self.menu_name]['page']
        if user_data[self.menu_name]['page'] > 0:
            user_data[self.menu_name]['page'] -= 1
        else:
            user_data[self.menu_name]['page'] = max_page

        if page != user_data[self.menu_name]['page']:
            self.send_message(context)

        if update.callback_query:
            self.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def search(self, update: Update, context: CallbackContext):
        """Callback method for filtering all menu objects by user text

        Args:
            update (:class:`~telegram.Update`): Update from callback.
            context (:class:`~telegram.ext.CallbackContext`): Context from callback.
        """
        user_data = context.user_data
        user = user_data['user']
        text = update.message.text

        objects = self.query_objects(context)

        filtered_objects = []
        for obj in objects:
            value = getattr(obj, self.search_key_param, False)
            if value and text.lower() in value.lower():
                filtered_objects.append(obj)

        if filtered_objects:
            user_data[self.menu_name]['filtered_objects'] = filtered_objects
            user_data[self.menu_name]['page'] = 0
            delete_interface(context)
            self.send_message(context)
        else:
            delete_interface(context)
            send_or_edit(context, chat_id=user.chat_id, text=self.empty_search_text(context))
            remove_interface(context)
            self.send_message(context)

        return self.States.ACTION

    def get_handler(self):
        search_handlers = []

        control_handlers = [CallbackQueryHandler(self.next_page, pattern=f'^next_object_{self.menu_name}$'),
                            CallbackQueryHandler(self.prev_page, pattern=f'^prev_object_{self.menu_name}$'),
                            CallbackQueryHandler(self.back, pattern=f'^back_{self.menu_name}$')]
        search_handlers.append(CallbackQueryHandler(self.exit_search, pattern=f'^exit_search_{self.menu_name}$'))

        search_handlers.append(MessageHandler(Filters.text, self.search))

        states = {self.States.ACTION: control_handlers + (search_handlers if self.search_key_param is not None else [])}

        additional_states = self.additional_states()
        for key in additional_states:
            if key in states:
                states[key] = additional_states[key] + states[key]
            else:
                states.update({key: additional_states[key]})

        handler = ConversationHandler(entry_points=self.entry_points(),
                                      states=states,
                                      fallbacks=self.fallbacks(),
                                      allow_reentry=self.allow_reentry)

        return handler
