from abc import ABC

from math import ceil

from telegram import InlineKeyboardButton, Update, ParseMode
from telegram.ext import MessageHandler, Filters, ConversationHandler, CallbackContext

from botmanlib.messages import delete_user_message
from ..basemenu import BaseMenu


class ListMenu(BaseMenu, ABC):
    """Abstract class for creating LM's

    Attributes:
        menu_name (:obj:`str`): Menu name which would be used in user_data and some buttons callback data. Defaults to :obj:`list_menu`.
        allow_reentry (:obj:`bool`): Allow reentry to menu. Parameter would be passed to :class:`~telegram.ext.ConversationHandler`. Defaults to :obj:`True`.
        disable_web_page_preview (:obj:`bool`): Disable web page preview when sending menus messages. Defaults to :obj:`True`.
        auto_hide_arrows (:obj:`bool`): Hide left/right control buttons for listing. Defaults to :obj:`False`.
        objects_per_page (:obj:`int`): How many objects display to one page. Also used in next or previous page actions. Defaults to :obj:`1`.
        parse_mode (:class:`telegram.ParseMode` or :obj:`str`): Which parse mode to use when sending messages. Defaults to :attr:`~telegram.ParseMode.HTML`
        reset_on_entry (:obj:`bool`): Clear all fields to it's defaults or loaded values. Defaults to :obj:`True`.

    """
    menu_name = 'list_menu'
    allow_reentry = True
    disable_web_page_preview = True
    auto_hide_arrows = False
    objects_per_page = 1
    parse_mode = ParseMode.HTML
    reset_on_entry = True

    def page_text(self, current_page, max_page, context: CallbackContext):
        _ = context.user_data['user'].translator

        if self.parse_mode == "MARKDOWN":
            return _("Page") + ' ' + str(current_page) + ' ' + _("of") + ' ' + str(max_page)
        elif self.parse_mode == "HTML":
            return _("Page") + ' ' + str(current_page) + ' ' + _("of") + ' ' + str(max_page)
        else:
            return ""

    def prev_button(self, context: CallbackContext):
        """Method that creates and return message previous button

        Args:
            context (:class:`~telegram.ext.CallbackContext`): Context from callback.

        Returns:
            (:class:`~telegram.InlineKeyboardButton`): Button for previous page action
        """

        return InlineKeyboardButton("◀", callback_data=f"prev_object_{self.menu_name}")

    def next_button(self, context: CallbackContext):
        """Method that creates and return message next button

        Args:
            context (:class:`~telegram.ext.CallbackContext`): Context from callback.

        Returns:
            (:class:`~telegram.InlineKeyboardButton`): Button for next page action
        """

        return InlineKeyboardButton("▶", callback_data=f"next_object_{self.menu_name}")

    def center_buttons(self, context: CallbackContext, o=None):
        """Method that creates and return message buttons which would be placed between `next_button` and `prev_button`

        Args:
            o: One or few object depending on menu type
            context (:class:`~telegram.ext.CallbackContext`): Context from callback.

        Returns:
            (List[:class:`~telegram.InlineKeyboardButton`]): List of buttons which would be inserted between arrows.
        """
        return []

    def back_button(self, context):
        """Method that creates and return message back button

        Args:
            context (:class:`~telegram.ext.CallbackContext`): Context from callback.

        Returns:
            (:class:`~telegram.InlineKeyboardButton`): Button for back action
        """
        _ = context.user_data['user'].translator
        return InlineKeyboardButton(_("Back"), callback_data=f"back_{self.menu_name}")

    def search_back_button(self, context: CallbackContext):
        """Method that creates and return button for back from search action

        Args:
            context (:class:`~telegram.ext.CallbackContext`): Context from callback.

        Returns:
            (:class:`~telegram.InlineKeyboardButton`): Button for back from search action
        """
        _ = context.user_data['user'].translator
        return InlineKeyboardButton(_("Exit search"), callback_data=f"exit_search_{self.menu_name}")

    def controls(self, context: CallbackContext, o=None):
        """Methods that collects and groups buttons for list menu control. Buttons would be placed on top, right away under message text.

        Args:
            o: One or few object depending on menu type
            context (:class:`~telegram.ext.CallbackContext`): Context from callback.

        Returns:
            (List[:class:`~telegram.InlineKeyboardButton`]): List of buttons that form first row under message.
        """
        _ = context.user_data['user'].translator
        controls = []
        obj_size = len(context.user_data[self.menu_name]['objects'])
        prev_button = self.prev_button(context)
        if prev_button and obj_size and (not self.auto_hide_arrows or (self.auto_hide_arrows and obj_size > self.objects_per_page)):
            controls.append(prev_button)

        center_buttons = self.center_buttons(context, o)
        if center_buttons:
            controls.extend(center_buttons)

        next_button = self.next_button(context)
        if next_button and obj_size and (not self.auto_hide_arrows or (self.auto_hide_arrows and obj_size > self.objects_per_page)):
            controls.append(next_button)

        return controls

    def object_buttons(self, context: CallbackContext, o):
        """Method that creates and return buttons list for current page

        Args:
            o: One or few object depending on menu type
            context (:class:`~telegram.ext.CallbackContext`): Context from callback.

        Returns:
            (List[List[:class:`~telegram.InlineKeyboardButton`]]]): List of buttons
        """
        return []

    def top_buttons(self, context: CallbackContext):
        """Method that creates and return buttons list which would be placed on top of other buttons

        Args:
            context (:class:`~telegram.ext.CallbackContext`): Context from callback.

        Returns:
            (List[List[:class:`~telegram.InlineKeyboardButton`]]]): List of buttons
        """

        return []

    def bottom_buttons(self, context: CallbackContext):
        """Method that creates and return buttons list which would be placed under other buttons

        Args:
            context (:class:`~telegram.ext.CallbackContext`): Context from callback.

        Returns:
            (List[List[:class:`~telegram.InlineKeyboardButton`]]]): List of buttons
        """

        return []

    def query_objects(self, context: CallbackContext):
        """Abstract method that must return list of objects which this menu will list for.

        Args:
            context (:class:`~telegram.ext.CallbackContext`): Context from callback.

        Returns:
            List[model] or List[:obj:`dict`]
        """
        raise NotImplementedError

    def send_message(self, context: CallbackContext):
        """Abstract method that must create and send message with current page info to telegram

        Args:
            context (:class:`~telegram.ext.CallbackContext`): Context from callback.

        Returns:
            (:class:`telegram.Message`): Message that was sent
        """
        raise NotImplementedError

    def entry(self, update: Update, context: CallbackContext):
        """Callback which must be called when user enter to menu

        Args:
            update (:class:`~telegram.Update`): Telegram update instance.
            context (:class:`~telegram.ext.CallbackContext`): Context from handler.
        """
        self._load(context)

        self.send_message(context)
        if update.callback_query:
            context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def _load(self, context: CallbackContext):
        """Initialization user-side variables which are required for the menu to work properly

        Args:
            context (:obj:`dict`): Context from callback.
        """
        raise NotImplementedError

    def entry_points(self):
        """Abstract method that must return entry point handlers for this menu. This handlers would be passed to `ConversationHandler` as `entry_points`

        Returns:
            (List[:class:`~telegram.ext.Handler`]): List of entry points for :class:`telegram.ext.ConversationHandler`
        """
        raise NotImplementedError

    def back(self, update: Update, context: CallbackContext):
        """Method that will be executed when menu will close

        Args:
            update (:class:`~telegram.Update`): Telegram update instance.
            context (:class:`~telegram.ext.CallbackContext`): Context from callback.

        Returns:
            (:obj:`telegram.ext.ConversationHandler.END`): -1 That means to exit from conversation handler
        """
        update_objects = getattr(self.parent, 'update_objects', None)
        if update_objects:
            update_objects(context)

        if update.callback_query:
            update.callback_query.answer()

        self.parent.send_message(context)
        return ConversationHandler.END

    def additional_states(self):
        """Abstract method that creates dict of states which be added before other LM states

        Note:
            States that already exists will be extended with new states. If result contains ACTION state then user-defined handlers would have more priority than LM's handlers, so you can override already existed handlers.

        Returns:
            ((dict[state, List[:class:`telegram.ext.Handler`])): Dict of state: handler pairs to be added to conversation handler states.
        """
        return {}

    def empty_search_text(self, context: CallbackContext):
        _ = context.user_data['user'].translator
        """Method that creates and return text which would be displayed when nothing found with search

        Args:
            context (:class:`~telegram.ext.CallbackContext`): Context from callback.

        Returns:
            (:obj:`str`): Text which would be displayed when nothing found
        """
        return _("Nothing found")

    def exit_search(self, update: Update, context: CallbackContext):
        """Callback which would be called when user will exit from search

        Args:
            update (:class:`~telegram.Update`): Telegram update instance.
            context (:class:`~telegram.ext.CallbackContext`): Context from handler.
        """
        context.user_data[self.menu_name]['filtered_objects'] = []
        context.user_data[self.menu_name]['selected_object'] = 0
        self.send_message(context)
        return self.States.ACTION

    def _get_objects(self, context: CallbackContext, force_update=False):
        user_data = context.user_data
        if force_update:
            return self.update_objects(context)
        else:
            if user_data[self.menu_name]['filtered_objects']:
                objects = user_data[self.menu_name]['filtered_objects']
            elif user_data[self.menu_name]['objects']:
                objects = user_data[self.menu_name]['objects']
            else:
                objects = user_data[self.menu_name]['objects'] = self.query_objects(context)

        user_data[self.menu_name]['max_page'] = len(objects) if self.objects_per_page == 1 else ceil(len(objects) / self.objects_per_page)

        if user_data[self.menu_name]['max_page'] <= 0:
            user_data[self.menu_name]['max_page'] = 1
        return objects

    def fallbacks(self):
        """Method that must return fallback handlers for this menu. This handlers would be passed to `ConversationHandler` as `fallbacks`

        Returns:
            (List[:class:`~telegram.ext.Handler`]): List of fallbacks for :class:`telegram.ext.ConversationHandler`
        """
        return [MessageHandler(Filters.all, lambda update, context: delete_user_message(update))]

    def update_objects(self, context: CallbackContext):
        """Use this method when you want update objects which this menu list.
        This method useful when callback made changes to objects and wrote this changes to database.

        Args:
            context (:class:`~telegram.ext.CallbackContext`): Context from handler.
        """
        user_data = context.user_data
        objects = user_data[self.menu_name]['objects'] = self.query_objects(context)
        user_data[self.menu_name]['filtered_objects'] = []
        user_data[self.menu_name]['selected_object'] = 0

        user_data[self.menu_name]['max_page'] = len(objects) if self.objects_per_page == 1 else ceil(len(objects) / self.objects_per_page)

        if user_data[self.menu_name]['max_page'] <= 0:
            user_data[self.menu_name]['max_page'] = 1

        return objects
