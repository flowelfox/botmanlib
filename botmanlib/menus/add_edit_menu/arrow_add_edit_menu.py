import datetime
import re
from math import ceil

import formencode
from telegram import InlineKeyboardMarkup, InlineKeyboardButton, Update, ReplyKeyboardMarkup, KeyboardButton, ParseMode
from telegram.ext import ConversationHandler, CallbackQueryHandler, MessageHandler, Filters, CallbackContext

from botmanlib.messages import send_or_edit, delete_interface, delete_user_message
from botmanlib.models import Database, MessageType
from botmanlib.utils import UpdateObjectError
from ..basemenu import BaseMenu
from ..helpers import add_to_db, group_buttons


class ArrowAddEditMenu(BaseMenu):
    class Action:
        EDIT = "EDIT"
        ADD = "ADD"

    class Field:
        def __init__(self,
                     param,
                     name,
                     validator,
                     required=False,
                     hidden=False,
                     default=None,
                     units="",
                     variants=None,
                     depend_field=None,
                     depend_value=None,
                     depend_inverse=False,
                     before_validate=None,
                     after_validate=None,
                     message_type=MessageType.text,
                     callback=None,
                     invalid_message=None,
                     variants_text=None,
                     variants_group_size=2,
                     switchable=True,
                     multiple=False):
            self.param = param
            self.name = name
            self.valdiator = validator
            self.required = required
            self.hidden = hidden
            self.default = default if default is not None else ([] if multiple else None)
            self.units = units
            self.variants = variants
            self.depend_field = depend_field
            self.depend_value = depend_value
            self.depend_inverse = depend_inverse
            self.before_validate = before_validate
            self.after_validate = after_validate
            self.message_type = message_type
            self.callback = callback
            self.invalid_message = invalid_message
            self.variants_text = variants_text
            self.variants_group_size = variants_group_size
            self.switchable = switchable
            self.multiple = multiple

        def validate(self, data):
            if self.before_validate is not None:
                data = self.before_validate(data)
            value = self.valdiator.to_python(data)
            if self.after_validate is not None:
                value = self.after_validate(value)
            return value

    menu_name = 'arrow_add_edit_menu'
    model = None  # database model to create new objects
    clear_on_entry = False  # clear all field when editing existing object
    variants_per_page = 5  # Number of variants per page
    show_arrows = True  # Display or not arrows buttons
    show_field_selectors = True
    start_with_none_selected = False  # Do not select first field when entering menu
    field_buttons_group_size = 1  # group buttons with field names, default is one button per line.
    reset_to_default = False  # Reset to default value when editing instead of obj value.
    parse_mode = ParseMode.HTML
    date_format = "%d.%m.%Y"
    datetime_format = "%H:%M %d.%m.%Y"
    time_format = "%H:%M"
    remove_user_messages = True
    force_action = None
    disable_web_page_preview = False
    message_parse_mode = None  # None or ParseMode.HTML or ParseMode.MARKDOWN

    def entry(self, update: Update, context: CallbackContext):
        self._entry(update, context)
        self.load(context)
        self.send_message(context)
        return self.States.ACTION

    def action(self, context: CallbackContext):
        return context.user_data[self.menu_name]['_action_']

    def active_field(self, context):
        return self._available_fields(context)[self.active_field_id(context)]

    def active_field_id(self, context):
        return context.user_data[self.menu_name]['active_field']

    def _entry(self, update: Update, context: CallbackContext):
        user_data = context.user_data
        if self.menu_name not in user_data:
            user_data[self.menu_name] = {}

        if self.force_action is not None:
            user_data[self.menu_name]['_action_'] = self.force_action
        else:
            if update.callback_query and 'edit_' in update.callback_query.data:
                user_data[self.menu_name]['_action_'] = self.Action.EDIT
            else:
                user_data[self.menu_name]['_action_'] = self.Action.ADD

        if 'active_field' not in user_data[self.menu_name]:
            user_data[self.menu_name]['active_field'] = None

        user_data['variations_interface'] = None
        user_data[self.menu_name]['variant_page'] = 0

    def load(self, context: CallbackContext):
        self._load(context)

    def _load(self, context: CallbackContext):
        user_data = context.user_data
        obj = self.query_object(context)

        for field in self.fields(context):
            if self.action(context) is self.Action.ADD or \
                    (self.action(context) is self.Action.EDIT and isinstance(obj, dict) and obj.get(field.param, None)) is None or \
                    (self.action(context) is self.Action.EDIT and not isinstance(obj, dict) and getattr(obj, field.param, None) is None):
                user_data[self.menu_name][field.param] = field.default
            else:
                if isinstance(obj, dict):
                    user_data[self.menu_name][field.param] = obj.get(field.param, ([] if field.multiple else None)) if not self.clear_on_entry else None
                else:
                    user_data[self.menu_name][field.param] = getattr(obj, field.param, ([] if field.multiple else None)) if not self.clear_on_entry else None

        for idx, field in enumerate(self._available_fields(context)):
            if field.switchable:
                context.user_data[self.menu_name]['active_field'] = idx
                break

        return obj

    def query_object(self, context: CallbackContext):
        raise NotImplementedError

    def fields(self, context: CallbackContext):
        """Provides fields info for displaying, validating and setting fields for object

        Returns:
            (dict[param:dict['name', 'validator', 'default', 'hidden']]): Returns dict of model.param: dict with field name,
            field validator, optional default value for field and optional hidden flag

        Note:
            param must be self.model attribute
            param can contain asterix to mark field as required
        """
        raise NotImplementedError

    def entry_points(self):
        raise NotImplementedError

    def back(self, update: Update, context: CallbackContext):
        update_objects = getattr(self.parent, 'update_objects', None)
        if update_objects:
            update_objects(context)
        delete_interface(context, 'ask_location')
        self.parent.send_message(context)
        return ConversationHandler.END

    def prev_button(self, context: CallbackContext):
        return InlineKeyboardButton("🔼", callback_data=f"prev_field_{self.menu_name}")

    def next_button(self, context: CallbackContext):
        return InlineKeyboardButton("🔽", callback_data=f"next_field_{self.menu_name}")

    def prev_variant_button(self, context: CallbackContext):
        return InlineKeyboardButton("\u21D0 Page", callback_data=f"prev_variant_{self.menu_name}")

    def next_variant_button(self, context: CallbackContext):
        return InlineKeyboardButton("Page \u21D2", callback_data=f"next_variant_{self.menu_name}")

    def back_button(self, context: CallbackContext):
        _ = context.user_data['user'].translator
        return InlineKeyboardButton(_("Back"), callback_data=f"back_{self.menu_name}")

    def back_to_edit_button(self, context: CallbackContext):
        _ = context.user_data['user'].translator
        return InlineKeyboardButton(_("Back to edit"), callback_data=f"back_to_edit")

    def save_button(self, context: CallbackContext):
        _ = context.user_data['user'].translator
        return InlineKeyboardButton(_("Save"), callback_data=f"save_{self.menu_name}")

    def center_buttons(self, context: CallbackContext):
        buttons = []
        save_button = self.save_button(context)
        if save_button:
            buttons.append(save_button)

        reset_button = self.reset_button(context)
        if reset_button:
            buttons.append(reset_button)

        return buttons

    def reset_button(self, context: CallbackContext):
        _ = context.user_data['user'].translator
        return InlineKeyboardButton(_("Reset"), callback_data="reset_field")

    def location_button(self, context: CallbackContext):
        _ = context.user_data['user'].translator
        return KeyboardButton(_("Send location"), request_location=True)

    def controls(self, context: CallbackContext):
        buttons = []
        if self.show_arrows:
            buttons.append(self.prev_button(context))
        center_buttons = self.center_buttons(context)
        if center_buttons:
            buttons.extend(center_buttons)
        if self.show_arrows:
            buttons.append(self.next_button(context))
        return [buttons]

    def bottom_buttons(self, context: CallbackContext):
        return []

    def variant_center_buttons(self, context: CallbackContext):
        buttons = []
        return buttons

    def variant_controls(self, context: CallbackContext, variants):
        buttons = []
        if len(variants) > self.variants_per_page:
            buttons.append(self.prev_variant_button(context))
        center_buttons = self.variant_center_buttons(context)

        if center_buttons:
            buttons.extend(center_buttons)

        if len(variants) > self.variants_per_page:
            buttons.append(self.next_variant_button(context))
        return [buttons]

    def _get_field(self, context: CallbackContext, name):
        for field in self.fields(context):
            if field.param == name:
                return field

    def _available_fields(self, context):
        available_fields = []
        for field in self.fields(context):
            depend_field = self._get_field(context, field.depend_field)
            if depend_field is None or (depend_field and (context.user_data[self.menu_name][depend_field.param] != field.depend_value if field.depend_inverse else context.user_data[self.menu_name][depend_field.param] == field.depend_value)):
                available_fields.append(field)
        return available_fields

    def message_top_text(self, context: CallbackContext):
        message_text = "〰〰〰〰〰〰〰〰〰〰\n"
        return message_text

    def message_bottom_text(self, context: CallbackContext):
        message_text = "〰〰〰〰〰〰〰〰〰〰\n"
        return message_text

    def location_message_text(self, context):
        _ = context.user_data['user'].translator
        return _("Please send me your location")

    def message_text(self, context: CallbackContext):
        _ = context.user_data['user'].translator

        message_text = self.message_top_text(context)
        available_fields = self._available_fields(context)
        active_field = context.user_data[self.menu_name]['active_field']

        for idx, field in enumerate(available_fields):
            if field.variants:
                message_text += ("✅" if active_field == idx else '      ')
            else:
                message_text += ("✍" if active_field == idx else '      ')

            value = context.user_data[self.menu_name][field.param]
            if isinstance(value, str):
                value = value.replace("\_", "_").replace("\*", "*").replace("\[", "[").replace("\]", "]")
            elif isinstance(value, datetime.datetime):
                value = value.strftime(self.datetime_format)
            elif isinstance(value, datetime.date):
                value = value.strftime(self.date_format)
            elif isinstance(value, datetime.time):
                value = value.strftime(self.time_format)
            elif isinstance(value, list):
                value = ', '.join(value)
            elif isinstance(value, bool):
                value = _("Yes") if value else _("No")

            if field.message_type is not MessageType.text and value is not None:
                value = _("Uploaded")
            elif field.message_type is not MessageType.text and value is None:
                value = _("Not uploaded")

            bold_start_tag = "<b>" if self.parse_mode == ParseMode.HTML else "**"
            bold_end_tag = "</b>" if self.parse_mode == ParseMode.HTML else "**"
            if active_field == idx and value is None:
                message_text += f"{bold_start_tag}{field.name}: " + _('Waiting...') + f"{bold_end_tag}\n"
            elif active_field == idx and value is not None:
                message_text += f"{bold_start_tag}{field.name}: {value}{field.units}{bold_end_tag}\n"
            else:
                message_text += f"{field.name}: {str(value) if value is not None else _('Empty')}{field.units}\n"
        message_text += self.message_bottom_text(context)

        return message_text

    def send_message(self, context: CallbackContext):
        user_data = context.user_data
        user = user_data['user']

        available_fields = self._available_fields(context)
        active_field = available_fields[user_data[self.menu_name]['active_field']] if user_data[self.menu_name]['active_field'] is not None else None

        if active_field and active_field.variants:
            markup = None
            values_buttons = []
            for idx, variant in enumerate(active_field.variants[self.variants_per_page * user_data[self.menu_name]['variant_page']:self.variants_per_page * user_data[self.menu_name]['variant_page'] + self.variants_per_page]):
                if active_field.multiple and variant in user_data[self.menu_name][active_field.param]:
                    values_buttons.append(InlineKeyboardButton("✅ " + str(variant), callback_data=f"variant_{user_data[self.menu_name]['variant_page'] * self.variants_per_page + idx}"))
                else:
                    values_buttons.append(InlineKeyboardButton(str(variant), callback_data=f"variant_{user_data[self.menu_name]['variant_page'] * self.variants_per_page + idx}"))

            variant_buttons = []
            controls_buttons = self.controls(context)
            if controls_buttons:
                variant_buttons.extend(controls_buttons)
            variant_buttons.extend(self.variant_controls(context, active_field.variants))
            variant_buttons.extend(group_buttons(values_buttons, active_field.variants_group_size))
            bottom_buttons = self.bottom_buttons(context)
            if bottom_buttons:
                variant_buttons.extend(bottom_buttons)

            back_button = self.back_button(context)
            if back_button:
                variant_buttons.append([back_button])

            variant_markup = InlineKeyboardMarkup(variant_buttons)
        else:
            variant_markup = None
            buttons = self.controls(context)
            field_buttons = []
            for field in available_fields:
                if not field.switchable:
                    continue

                depend_field = self._get_field(context, field.depend_field)
                if depend_field is None or (depend_field and (user_data[self.menu_name][depend_field.param] != field.depend_value if field.depend_inverse else user_data[self.menu_name][depend_field.param] == field.depend_value)):
                    button_text = re.sub('<[^<]+?>', '', field.name.replace("\\", ""))
                    field_buttons.append(InlineKeyboardButton(button_text, callback_data=f"field_{field.param}"))
            if self.show_field_selectors:
                buttons.extend(group_buttons(field_buttons, self.field_buttons_group_size))

            bottom_buttons = self.bottom_buttons(context)
            if bottom_buttons:
                buttons.extend(bottom_buttons)

            back_button = self.back_button(context)
            if back_button:
                buttons.append([back_button])
            markup = InlineKeyboardMarkup(buttons)

        message_text = self.message_text(context)

        if variant_markup:
            pages = ceil(len(active_field.variants) / self.variants_per_page)
            variants_text = (active_field.variants_text + (' ({0} / {1})'.format(user_data[self.menu_name]['variant_page'] + 1, pages) if pages > 1 else "")) if active_field.variants_text else None
            send_or_edit(context, chat_id=user.chat_id, text=variants_text if variants_text else message_text, reply_markup=variant_markup, parse_mode=self.parse_mode, disable_web_page_preview=self.disable_web_page_preview)
        else:
            send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=markup, parse_mode=self.parse_mode, disable_web_page_preview=self.disable_web_page_preview)

        if active_field:
            # send location ask message
            location_text = self.location_message_text(context)
            if active_field.message_type is MessageType.location and location_text is not None:
                location_button = self.location_button(context)
                location_markup = ReplyKeyboardMarkup([[location_button]], resize_keyboard=True) if location_button else None
                send_or_edit(context, 'ask_location', chat_id=user.chat_id, text=location_text, parse_mode=self.parse_mode, disable_web_page_preview=self.disable_web_page_preview, reply_markup=location_markup)
            elif active_field.message_type is not MessageType.location and context.user_data['interfaces'].get('ask_location', False):
                delete_interface(context, 'ask_location')

    def change_field(self, update: Update, context: CallbackContext):
        user_data = context.user_data
        data = update.callback_query.data.replace('field_', '')
        user_data[self.menu_name]['variant_page'] = 0

        available_fields = self._available_fields(context)
        for idx, field in enumerate(available_fields):
            if field.param == data:
                active_field = idx
                break
        else:
            active_field = 0

        if active_field != user_data[self.menu_name]['active_field']:
            user_data[self.menu_name]['active_field'] = active_field
            if self.active_field(context) and self.active_field(context).callback:
                return self.active_field(context).callback(context)
            self.send_message(context)

        self.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def prev_field(self, update: Update, context: CallbackContext):
        user_data = context.user_data
        user_data[self.menu_name]['variant_page'] = 0

        current_field = self.active_field_id(context)
        while True:
            if self.active_field_id(context) <= 0:
                user_data[self.menu_name]['active_field'] = len(self._available_fields(context)) - 1
            else:
                user_data[self.menu_name]['active_field'] -= 1

            active_field = self.active_field(context)
            if active_field.switchable:
                break
            if self.active_field_id(context) == current_field:
                user_data[self.menu_name]['active_field'] = None
                break

        if self.active_field(context) and self.active_field(context).callback:
            return self.active_field(context).callback(context)

        if update.callback_query:
            self.bot.answer_callback_query(update.callback_query.id)

        self.send_message(context)

        return self.States.ACTION

    def next_field(self, update: Update, context: CallbackContext):
        user_data = context.user_data
        user_data[self.menu_name]['variant_page'] = 0

        current_field = self.active_field_id(context)
        while True:
            if self.active_field_id(context) >= len(self._available_fields(context)) - 1:
                user_data[self.menu_name]['active_field'] = 0
            else:
                user_data[self.menu_name]['active_field'] += 1

            active_field = self.active_field(context)
            if active_field.switchable:
                break
            if self.active_field_id(context) == current_field:
                user_data[self.menu_name]['active_field'] = None
                break

        if self.active_field(context) and self.active_field(context).callback:
            return self.active_field(context).callback(context)

        if update.callback_query:
            self.bot.answer_callback_query(update.callback_query.id)

        self.send_message(context)

        return self.States.ACTION

    def prev_variant(self, update: Update, context: CallbackContext):
        user_data = context.user_data
        active_field = self._available_fields(context)[user_data[self.menu_name]['active_field']]
        variant_page = user_data[self.menu_name]['variant_page']

        if variant_page <= 0:
            user_data[self.menu_name]['variant_page'] = ceil(len(active_field.variants) / self.variants_per_page) - 1
        else:
            user_data[self.menu_name]['variant_page'] -= 1

        if update.callback_query:
            self.bot.answer_callback_query(update.callback_query.id)

        self.send_message(context)

        return self.States.ACTION

    def next_variant(self, update: Update, context: CallbackContext):
        user_data = context.user_data
        active_field = self._available_fields(context)[user_data[self.menu_name]['active_field']]
        variant_page = user_data[self.menu_name]['variant_page']

        if variant_page >= ceil(len(active_field.variants) / self.variants_per_page) - 1:
            user_data[self.menu_name]['variant_page'] = 0
        else:
            user_data[self.menu_name]['variant_page'] += 1

        if update.callback_query:
            self.bot.answer_callback_query(update.callback_query.id)

        self.send_message(context)

        return self.States.ACTION

    def fill_field(self, update: Update, context: CallbackContext):
        user_data = context.user_data
        user = user_data['user']
        _ = user.translator
        # check is there is any active field to fill
        fields = self._available_fields(context)
        active_field = user_data[self.menu_name]['active_field']
        if self.remove_user_messages:
            delete_user_message(update)
        if active_field is None:
            self.send_message(context)
            return self.States.ACTION
        if fields[active_field].variants and update.callback_query is None:
            self.send_message(context)
            return self.States.ACTION

        if fields[active_field].message_type is MessageType.document:
            self.bot.send_message(chat_id=user.chat_id, text=_("Please send me document"))
            self.send_message(context)
            return self.States.ACTION
        elif fields[active_field].message_type is MessageType.photo:
            self.bot.send_message(chat_id=user.chat_id, text=_("Please send me photo"))
            self.send_message(context)
            return self.States.ACTION
        elif fields[active_field].message_type is MessageType.video:
            self.bot.send_message(chat_id=user.chat_id, text=_("Please send me video"))
            self.send_message(context)
            return self.States.ACTION
        elif fields[active_field].message_type is MessageType.animation:
            self.bot.send_message(chat_id=user.chat_id, text=_("Please send me animation"))
            self.send_message(context)
            return self.States.ACTION
        elif fields[active_field].message_type is MessageType.sticker:
            self.bot.send_message(chat_id=user.chat_id, text=_("Please send me sticker"))
            self.send_message(context)
            return self.States.ACTION
        elif fields[active_field].message_type is MessageType.location:
            # do nothing
            return self.States.ACTION

        # obtain text
        if update.callback_query:
            data = int(update.callback_query.data.replace("variant_", ""))
            text = self.active_field(context).variants[data]
            delete_interface(context, 'variations_interface')
            self.bot.answer_callback_query(update.callback_query.id)
        else:
            if self.message_parse_mode == ParseMode.MARKDOWN:
                text = update.message.text_markdown
            elif self.message_parse_mode == ParseMode.MARKDOWN_V2:
                text = update.message.text_markdown_v2
            elif self.message_parse_mode == ParseMode.HTML:
                text = update.message.text_html
            else:
                text = update.message.text

        # write text to user_data
        available_fields = self._available_fields(context)
        field = available_fields[active_field]
        try:
            if field.multiple and field.variants:
                if user_data[self.menu_name][field.param] is None or user_data[self.menu_name][field.param] is []:
                    user_data[self.menu_name][field.param] = []
                value = field.validate(text)
                if value in user_data[self.menu_name][field.param]:
                    user_data[self.menu_name][field.param].remove(value)
                else:
                    user_data[self.menu_name][field.param].append(value)

                self.send_message(context)
                return self.States.ACTION
            else:
                user_data[self.menu_name][field.param] = field.validate(text)

            user_data[self.menu_name]['variant_page'] = 0

        except formencode.Invalid:
            delete_interface(context)

            self.bot.send_message(chat_id=user.chat_id, text=field.invalid_message if field.invalid_message else _("Wrong input, try again"), parse_mode=self.parse_mode)
            self.send_message(context)
            return self.States.ACTION

        # change selected field
        return self.next_field(update, context)

    def fill_media_field(self, update: Update, context: CallbackContext):
        user_data = context.user_data
        user = user_data['user']
        _ = user.translator

        # check is there is any active field to fill
        fields = self._available_fields(context)
        active_field = user_data[self.menu_name]['active_field']
        if active_field is None:
            return self.States.ACTION

        # obtain text
        if update.message.photo and fields[active_field].message_type is MessageType.photo:
            text = update.message.photo[-1].file_id
        elif update.message.document and fields[active_field].message_type is MessageType.document:
            text = update.message.document.file_id
        elif update.message.video and fields[active_field].message_type is MessageType.video:
            text = update.message.video.file_id
        elif update.message.animation and fields[active_field].message_type is MessageType.animation:
            text = update.message.animation.file_id
        elif update.message.sticker and fields[active_field].message_type is MessageType.sticker:
            text = update.message.sticker.file_id
        else:
            self.bot.send_message(chat_id=user.chat_id, text=_("This field does not allow upload this type of content."))
            delete_interface(context)
            self.send_message(context)
            return self.States.ACTION

        # write text to user_data
        available_fields = self._available_fields(context)
        for idx, field in enumerate(available_fields):
            if idx == active_field:
                user_data[self.menu_name][field.param] = field.validate(text)

        delete_user_message(update)

        # change selected field
        return self.next_field(update, context)

    def fill_location_field(self, update: Update, context: CallbackContext):
        user_data = context.user_data
        user = user_data['user']
        _ = user.translator

        # check is there is any active field to fill
        fields = self._available_fields(context)
        active_field = user_data[self.menu_name]['active_field']
        if active_field is None:
            return self.States.ACTION

        # obtain value
        if update.message.location and fields[active_field].message_type is MessageType.location:
            value = update.message.location
        else:
            self.bot.send_message(chat_id=user.chat_id, text=_("This field does not allow upload location."))
            return self.States.ACTION

        # write value to user_data
        available_fields = self._available_fields(context)
        for idx, field in enumerate(available_fields):
            if idx == active_field:
                user_data[self.menu_name][field.param] = field.validate(value)

        delete_user_message(update)

        # change selected field
        return self.next_field(update, context)

    def reset_field(self, update: Update, context: CallbackContext):
        user_data = context.user_data
        self.bot.answer_callback_query(update.callback_query.id)

        active_field = user_data[self.menu_name]['active_field']
        if active_field is None:
            return self.States.ACTION

        fields = self._available_fields(context)
        field = fields[active_field]

        if user_data[self.menu_name]['_action_'] == self.Action.ADD or self.reset_to_default:
            user_data[self.menu_name][field.param] = field.default
        else:
            user_data[self.menu_name][field.param] = getattr(self.query_object(context), field.param, None)

        self.send_message(context)
        return self.States.ACTION

    def back_to_edit(self, update: Update, context: CallbackContext):
        user_data = context.user_data
        delete_interface(context, 'variations_interface')
        user_data[self.menu_name]['active_field'] = None
        user_data[self.menu_name]['variant_page'] = 0
        self.bot.answer_callback_query(update.callback_query.id)
        self.send_message(context)
        return self.States.ACTION

    def check_fields(self, context: CallbackContext):
        user_data = context.user_data
        fields = self.fields(context)
        for field in fields:
            depend_field = self._get_field(context, field.depend_field)
            if depend_field is None or (depend_field and (user_data[self.menu_name][depend_field.param] != field.depend_value if field.depend_inverse else user_data[self.menu_name][depend_field.param] == field.depend_value)):
                if field.required and user_data[self.menu_name][field.param] is None:
                    return False

        return True

    def save(self, update: Update, context: CallbackContext):
        _ = context.user_data['user'].translator

        if not self.check_fields(context):
            delete_interface(context)
            self.bot.answer_callback_query(update.callback_query.id, text=_("Please fill required fields."), show_alert=True)
            self.send_message(context)
            return self.States.ACTION
        delete_interface(context, 'variations_interface')

        try:
            self.update_object(context)
            update.callback_query.answer(text=self.after_save_text(context), show_alert=False)
            return self.back(update, context)
        except UpdateObjectError as e:
            update.callback_query.answer(text=e.message, show_alert=e.show_alert)
            return self.States.ACTION

    def after_save_text(self, context):
        return None

    def update_object(self, context: CallbackContext):
        user_data = context.user_data
        if user_data[self.menu_name]['_action_'] == self.Action.ADD and self.model is not None:
            obj = self.model()
        else:
            obj = self.query_object(context)

        available_fields = self._available_fields(context)
        for field in available_fields:
            if isinstance(obj, dict):
                obj[field.param] = user_data[self.menu_name][field.param]
            elif hasattr(obj, field.param):
                setattr(obj, field.param, user_data[self.menu_name][field.param])

        if obj is None or isinstance(obj, dict):
            session = None
        else:
            session = Database().sessionmaker.object_session(obj)

        self.save_object(obj, context, session)

    def save_object(self, obj, context: CallbackContext, session=None):
        if not add_to_db(obj, session):
            return self.conv_fallback(context)

    def additional_states(self):
        return {}

    def fallbacks(self):
        return [MessageHandler(Filters.all, lambda update, context: delete_user_message(update))]

    def get_handler(self):
        states = {self.States.ACTION: [CallbackQueryHandler(self.next_field, pattern=f'^next_field_{self.menu_name}$'),
                                       CallbackQueryHandler(self.next_variant, pattern=f'^next_variant_{self.menu_name}$'),
                                       CallbackQueryHandler(self.prev_field, pattern=f'^prev_field_{self.menu_name}$'),
                                       CallbackQueryHandler(self.prev_variant, pattern=f'^prev_variant_{self.menu_name}$'),
                                       CallbackQueryHandler(self.save, pattern=f'^save_{self.menu_name}$'),
                                       CallbackQueryHandler(self.back, pattern=f'^back_{self.menu_name}$'),
                                       CallbackQueryHandler(self.back_to_edit, pattern='^back_to_edit$'),
                                       CallbackQueryHandler(self.change_field, pattern='^field_.+$'),
                                       CallbackQueryHandler(self.fill_field, pattern='^variant_\d+$'),
                                       CallbackQueryHandler(self.reset_field, pattern='^reset_field$'),
                                       MessageHandler(Filters.text, self.fill_field),
                                       MessageHandler(Filters.photo | Filters.document | Filters.animation | Filters.video | Filters.sticker, self.fill_media_field),
                                       MessageHandler(Filters.location, self.fill_location_field)]
                  }

        additional_states = self.additional_states()
        for key in additional_states:
            if key in states:
                states[key] = additional_states[key] + states[key]
            else:
                states.update({key: additional_states[key]})

        handler = ConversationHandler(entry_points=self.entry_points(),
                                      states=states,
                                      fallbacks=self.fallbacks(),
                                      allow_reentry=True)

        return handler
