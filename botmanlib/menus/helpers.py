import copy
import gettext
import json
import locale
import logging
import os
import sys
import threading
from contextlib import contextmanager
from distutils.util import strtobool
from os import path
from time import time

import dill
import requests
from sqlalchemy.exc import SQLAlchemyError, IntegrityError
from telegram.ext import MessageHandler, Filters

from botmanlib.models import Database

logger = logging.getLogger('botmanlib')


def available_languages():
    project_root = os.getenv('project_root', os.path.dirname(sys.argv[0]))

    if path.exists(path.join(project_root, 'locales')):
        languages = next(os.walk(path.join(project_root, 'locales')))[1]
    else:
        languages = []
    if 'en' not in languages:
        languages.append('en')

    return languages


def translator(language_code='en'):
    translation = gettext.translation('messages', 'locales', languages=[language_code], fallback=True)
    return translation.gettext


def unknown_command(state):
    def unk_c_fun(update, context):
        user_data = context.user_data
        if user_data and 'user' in user_data and user_data['user']:
            _ = translator(user_data['user'].language_code)
        else:
            _ = gettext.gettext

        context.bot.send_message(chat_id=update.effective_chat.id, text=_("Sorry unknown command."))
        return state

    return unk_c_fun


def to_state(state):
    def to_state_callback(update, context):
        return state

    return to_state_callback


def add_to_db(objects, session=None):
    if not objects:
        return True

    if not isinstance(objects, list):
        objects = [objects]

    if session is None:
        session = Database().sessionmaker.object_session(objects[0])
        if session is None:
            session = Database().DBSession

    try:
        for object in objects:
            session.add(object)
        session.commit()
        return True
    except (SQLAlchemyError, IntegrityError) as e:
        session.rollback()
        logger.critical(f"Database error: {str(e)}")
        return False


def remove_from_db(objects, session=None):
    if not objects:
        return True

    if not isinstance(objects, list):
        objects = [objects]

    if session is None:
        session = Database().sessionmaker.object_session(objects[0])
        if session is None:
            session = Database().DBSession

    try:
        for object in objects:
            session.delete(object)
        session.commit()
        return True
    except (SQLAlchemyError, IntegrityError) as e:
        session.rollback()
        logger.critical(f"Database error: {str(e)}")
        return False


def write_settings(new_data: dict, settings_path, recreate=False):
    if not recreate:
        data = get_settings(settings_path)
        data.update(new_data)
    else:
        data = new_data

    with open(settings_path, 'w') as f:
        json.dump(data, f)


def get_settings(settings_path):
    if not os.path.exists(settings_path):
        return {}
    with open(settings_path, 'r') as f:
        data = json.load(f)
    return data


LOCALE_LOCK = threading.Lock()


@contextmanager
def setlocale(name):
    with LOCALE_LOCK:
        saved = locale.setlocale(locale.LC_ALL)
        try:
            yield locale.setlocale(locale.LC_ALL, name)
        finally:
            locale.setlocale(locale.LC_ALL, saved)


def group_buttons(buttons, group_size=2):
    group = []
    subgroup = []
    for button in buttons:
        subgroup.append(button)
        if len(subgroup) == group_size:
            group.append(subgroup)
            subgroup = []

    if subgroup:
        group.append(subgroup)

    return group


def inline_placeholder(state, text=None, show_alert=True):
    def placeholder(update, context):
        context.bot.answer_callback_query(update.callback_query.id, text=text, show_alert=show_alert)
        return state

    return placeholder


def generate_regex_handlers(button_text, callback, left_text="", right_text="", **kwargs):
    regex_handlers = []
    for language_code in available_languages():
        gt = translator(language_code)
        regex_handlers.append(MessageHandler(Filters.regex("^" + left_text + gt(button_text) + right_text + "$"), callback))
    return regex_handlers


def check_payment_api():
    api_url = os.getenv('payments.api_url')

    response = requests.get(api_url + 'status')
    if response.status_code != 200:
        logger.error(f"Api service not running on {api_url}")
        return False
    else:
        return True


def make_payment(token, amount, description, currency='UAH', lifetime=1800, phone=None, lang=None, response_url=None):
    api_url = os.getenv('payments.api_url')

    endpoint = api_url + ("orders" if api_url.endswith('/') else "/orders")
    headers = {"Content-Type": "application/json",
               "X-API-KEY": token}

    data = {
        "amount": amount,
        "currency": currency,
        "description": description,
        "lifetime": lifetime,
    }
    if strtobool(os.getenv('payments.debug', "True")):
        data.update(sandbox=True)
    if phone:
        data.update(phone=phone)
    if lang:
        data.update(lang=lang)
    if response_url:
        data.update(response_url=response_url)

    try:
        res = requests.post(endpoint, data=json.dumps(data), headers=headers)
    except ConnectionError as e:
        logger.critical(f"Can't create order because: {str(e)}")
        return False

    res_data = json.loads(res.content.decode('utf-8'))
    try:
        if res.status_code == 200 and res_data['success']:
            logger.info("Order created")
            return res_data['data']
        else:
            logger.critical(f"Can't create order because: {res_data['message']}")
            return False
    except KeyError:
        logger.critical(f"Can't create order because: HTTP status is {res.status_code}")
        return False


def check_payment(token, order_id):
    api_url = os.getenv('payments.api_url')

    endpoint = api_url + ("orders" if api_url.endswith('/') else "/orders")

    headers = {"Content-Type": "application/json",
               "X-API-KEY": token}
    data = json.dumps({
        "order_id": order_id,
    })

    try:
        res = requests.get(endpoint, data=data, headers=headers)
    except ConnectionError as e:
        logger.critical(f"Can't create order because: {str(e)}")
        return False

    res_data = json.loads(res.content.decode('utf-8'))
    try:
        if res.status_code == 200 and res_data['success']:
            order_status = res_data['data']['status']
            logger.debug(f"Received order from api with order_id:\"{order_id}\" with status: \"{order_status}\"")
            return order_status
        else:
            logger.critical(f"Can't get order because: {res_data['message']}")
            return False
    except KeyError:
        logger.critical(f"Can't create order because: HTTP status is {res.status_code}")
        return False


def load_jobs(jq, path):
    now = time()

    with open(path, 'rb') as fp:
        while True:
            try:
                next_t, job = dill.load(fp)
            except EOFError:
                break  # Loaded all job tuples

            # Create threading primitives
            enabled = job._enabled
            removed = job._remove

            job._enabled = threading.Event()
            job._remove = threading.Event()

            if not job.context:
                job.context = {}

            job.context['session'] = Database().sessionmaker()

            if enabled:
                job._enabled.set()

            if removed:
                job._remove.set()

            next_t -= now  # Convert from absolute to relative time

            jq._put(job, next_t)
    logger.info("Jobs loaded")


def save_jobs(jq, path):
    if jq is not None:
        job_tuples = jq._queue.queue

        with open(path, 'wb') as fp:
            for next_t, job in job_tuples:
                # Back up objects
                job_to_save = copy.copy(job)

                # Replace un-pickleable threading primitives
                job_to_save._job_queue = None  # Will be reset in jq.put
                job_to_save._remove = job._remove.is_set()  # Convert to boolean
                job_to_save._enabled = job._enabled.is_set()  # Convert to boolean

                ses = None
                if job_to_save.context and 'session' in job_to_save.context:
                    ses = job_to_save.context['session']
                    del job_to_save.context['session']

                # Save the job
                dill.dump((next_t, job_to_save), fp)
                if ses:
                    job_to_save.context['session'] = ses

                job_to_save.job_queue = jq

        logger.debug("Jobs saved")
    else:
        logger.debug("Can't save jobs because job queue is empty")


def save_jobs_job(bot, job):
    save_jobs(job.job_queue, job.context['jobs_file'])


def crossed_text(text):
    result = ''
    for c in text:
        result += c + '\u0336'
    return result


def require_permission(code=None):
    def wrap(f):
        def wrapped_f(*args):
            if code:
                if len(args) == 3:
                    self, update, context = args
                elif len(args) == 2:
                    update, context = args
                else:
                    return f(*args)
                user = context.user_data.get('user', None)
                _ = user.translator if user else translator('en')
                if not user or not user.has_permission(code):
                    if update.callback_query:
                        context.bot.answer_callback_query(update.callback_query.id, text=_("You were restricted to use this menu"), show_alert=True)
                    elif update.message:
                        context.bot.send_message(chat_id=update.message.chat_id, text=_("You were restricted to use this menu"))
                    return -1
            return f(*args)

        return wrapped_f

    return wrap


def prepare_user(user_model, update, context, lang=None):
    if context.user_data.get('user', False) and context.user_data.get('_', False):
        context.user_data['user'].activate()
        add_to_db(context.user_data['user'], session=Database().DBSession)
        return context.user_data['user']

    user = Database().DBSession.query(user_model).filter(user_model.chat_id == update.effective_user.id).first()
    tuser = update.effective_user
    if lang is None and tuser.language_code:
        lang = tuser.language_code
    elif lang is None and tuser.language_code is None:
        lang = 'en'

    if not user:
        came_from = context.args[0] if context.args else None
        user = user_model(came_from=came_from)
        if hasattr(user, 'init_permissions'):
            user.init_permissions()

    user.chat_id = tuser.id
    user.first_name = tuser.first_name
    user.last_name = tuser.last_name
    user.username = tuser.username
    user.activate()
    if user.language_code is None:
        user.language_code = lang

    context.user_data['user'] = user
    _ = context.user_data['_'] = user.translator  # TODO: Deprecated, need to remove

    add_to_db(user, session=Database().DBSession)

    return user
