import datetime
import enum

import formencode
from formencode import Invalid
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import ConversationHandler, CallbackQueryHandler, MessageHandler, Filters

from botmanlib.messages import send_or_edit, delete_interface
from ..basemenu import BaseMenu
from ..helpers import to_state, unknown_command


class NumpadTimePicker(BaseMenu):
    menu_name = "time_picker"
    user_data_key = 'time'
    delimiter = ':'
    emoji_keys = True
    emoji_arrows = True

    class States(enum.Enum):
        ACTION = 1

    def entry_points(self):
        raise NotImplementedError

    def back(self, update, context):
        update_objects = getattr(self.parent, 'update_objects', None)
        if update_objects:
            update_objects(context)
        self.parent.send_message(context)
        return ConversationHandler.END

    def additional_states(self):
        return {}

    def entry(self, update, context):
        self._entry(update, context)

        self.send_message(context)
        return self.States.ACTION

    def _entry(self, update, context):
        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        if self.user_data_key not in context.user_data[self.menu_name]:
            context.user_data[self.menu_name][self.user_data_key] = None

        current_time = datetime.time(0, 0, 0, 0)
        context.user_data[self.menu_name]['time_numbers'] = []
        for ch in current_time.strftime("%H%M"):
            context.user_data[self.menu_name]['time_numbers'].append(int(ch))

        context.user_data[self.menu_name]['current_number'] = 0

    def send_message(self, context):
        user = context.user_data['user']
        _ = user.translator

        current_number = context.user_data[self.menu_name]['current_number']
        time = context.user_data[self.menu_name]['time_numbers']

        message_text = "⬜⬜⬜⬜⬜ "
        for idx, number in enumerate(time):
            delimiter = ''
            if idx == 1:
                delimiter = self.delimiter
            if current_number == idx:
                str_number = "<code>{}</code>".format(number)
            else:
                str_number = str(number)

            message_text += str_number + delimiter

        message_text += " ⬜⬜⬜⬜⬜"

        buttons = [[InlineKeyboardButton("🔙", callback_data='back'),
                    InlineKeyboardButton("✅", callback_data='save'),
                    InlineKeyboardButton("🔄", callback_data='reset')],

                   [InlineKeyboardButton("1️⃣" if self.emoji_keys else "1", callback_data="key_1"),
                    InlineKeyboardButton("2️⃣" if self.emoji_keys else "2", callback_data="key_2"),
                    InlineKeyboardButton("3️⃣" if self.emoji_keys else "3", callback_data="key_3")],
                   [InlineKeyboardButton("4️⃣" if self.emoji_keys else "4", callback_data="key_4"),
                    InlineKeyboardButton("5️⃣" if self.emoji_keys else "5", callback_data="key_5"),
                    InlineKeyboardButton("6️⃣" if self.emoji_keys else "6", callback_data="key_6")],
                   [InlineKeyboardButton("7️⃣" if self.emoji_keys else "7", callback_data="key_7"),
                    InlineKeyboardButton("8️⃣" if self.emoji_keys else "8", callback_data="key_8"),
                    InlineKeyboardButton("9️⃣" if self.emoji_keys else "9", callback_data="key_9")],
                   [InlineKeyboardButton("◀" if self.emoji_arrows else "<", callback_data="prev_number"),
                    InlineKeyboardButton("0️⃣" if self.emoji_keys else "0", callback_data="key_0"),
                    InlineKeyboardButton("▶" if self.emoji_arrows else ">", callback_data="next_number")]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

    def reset(self, update, context):
        context.user_data[self.menu_name]['time_numbers'] = [0, 0, 0, 0]
        context.user_data[self.menu_name]['current_number'] = 0

        context.bot.answer_callback_query(update.callback_query.id)
        self.send_message(context)
        return self.States.ACTION

    def save(self, update, context):
        hour = int("{0}{1}".format(context.user_data[self.menu_name]['time_numbers'][0], context.user_data[self.menu_name]['time_numbers'][1]))
        minute = int("{0}{1}".format(context.user_data[self.menu_name]['time_numbers'][2], context.user_data[self.menu_name]['time_numbers'][3]))

        context.user_data[self.menu_name][self.user_data_key] = datetime.time(hour, minute, 0, 0)
        return self.back(update, context)

    def time_text_set(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        try:
            val = formencode.validators.TimeConverter()
            value = val.to_python(update.message.text)
            str_value = "{hour:02d}{minute:02d}".format(hour=value[0], minute=value[1])

            context.user_data[self.menu_name]['time_numbers'] = []
            for ch in str_value:
                context.user_data[self.menu_name]['time_numbers'].append(int(ch))
            context.user_data[self.menu_name]['current_number'] = 0

            self.send_message(context)

            return self.States.ACTION

        except Invalid:
            delete_interface(context)
            context.bot.send_message(chat_id=user.chat_id, text=_("Wrong input, try again"))
            self.send_message(context)
            return self.States.ACTION

    def next_number(self, update, context):
        if context.user_data[self.menu_name]['current_number'] < 3:
            context.user_data[self.menu_name]['current_number'] += 1
        else:
            context.user_data[self.menu_name]['current_number'] = 0

        self.send_message(context)
        context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def prev_number(self, update, context):
        if context.user_data[self.menu_name]['current_number'] >= 0:
            context.user_data[self.menu_name]['current_number'] -= 1
        else:
            context.user_data[self.menu_name]['current_number'] = 3

        self.send_message(context)
        context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def fill_number(self, update, context):
        data = int(update.callback_query.data.replace("key_", ""))

        context.bot.answer_callback_query(update.callback_query.id)

        time = context.user_data[self.menu_name]['time_numbers']
        current_number = context.user_data[self.menu_name]['current_number']

        if current_number == 0 and data > 2:
            return self.States.ACTION
        if current_number == 1 and time[0] > 1 and data > 3:
            return self.States.ACTION
        if current_number == 2 and data > 5:
            return self.States.ACTION

        context.user_data[self.menu_name]['time_numbers'][context.user_data[self.menu_name]['current_number']] = data

        return self.next_number(update, context)

    def get_handler(self):
        states = {self.States.ACTION: [CallbackQueryHandler(self.fill_number, pattern='key_\d'),
                                       CallbackQueryHandler(self.prev_number, pattern='prev_number'),
                                       CallbackQueryHandler(self.next_number, pattern='next_number'),
                                       CallbackQueryHandler(self.reset, pattern='reset'),
                                       CallbackQueryHandler(self.back, pattern=f'back_{self.menu_name}'),
                                       CallbackQueryHandler(self.save, pattern='save'),
                                       MessageHandler(Filters.text, self.time_text_set),
                                       MessageHandler(Filters.all, to_state(self.States.ACTION))]}

        additional_states = self.additional_states()
        for key in additional_states:
            if key in states:
                states[key] = additional_states[key] + states[key]
            else:
                states.update({key: additional_states[key]})

        handler = ConversationHandler(entry_points=self.entry_points(),
                                      states=states,
                                      fallbacks=[MessageHandler(Filters.all, unknown_command(-1))],
                                      allow_reentry=True)

        return handler
