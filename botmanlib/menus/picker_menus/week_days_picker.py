import enum

import formencode
from formencode import Invalid
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import ConversationHandler, CallbackQueryHandler, MessageHandler, Filters

from botmanlib.messages import send_or_edit, delete_interface
from ..basemenu import BaseMenu
from ..helpers import to_state, unknown_command


class WeekDaysPicker(BaseMenu):
    menu_name = "days_choser"
    user_data_key = 'days'
    delimiter = '\n'

    class States(enum.Enum):
        ACTION = 1

    def entry_points(self):
        raise NotImplementedError

    def back(self, update, context):
        update_objects = getattr(self.parent, 'update_objects', None)
        if update_objects:
            update_objects(context)
        self.parent.send_message(context)
        return ConversationHandler.END

    def additional_states(self):
        return {}

    def entry(self, update, context):
        self._entry(update, context)

        self.send_message(context)
        return self.States.ACTION

    def _entry(self, update, context):
        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}
        if self.user_data_key not in context.user_data[self.menu_name]:
            context.user_data[self.menu_name][self.user_data_key] = []

    def send_message(self, context):
        user = context.user_data['user']
        _ = user.translator

        days = context.user_data[self.menu_name][self.user_data_key]

        if not days:
            message_text = _("Select days using buttons below")
        else:
            message_text = _("Selected days:\n")
            days_str = []
            for day in days:
                if day == 0:
                    day_str = _("Monday")
                elif day == 1:
                    day_str = _("Tuesday")
                elif day == 2:
                    day_str = _("Wednesday")
                elif day == 3:
                    day_str = _("Thursday")
                elif day == 4:
                    day_str = _("Friday")
                elif day == 5:
                    day_str = _("Saturday")
                elif day == 6:
                    day_str = _("Sunday")
                else:
                    day_str = _("Error")
                days_str.append(day_str)

            message_text += self.delimiter.join(days_str)

        buttons = [[InlineKeyboardButton("🔙", callback_data='back'),
                    InlineKeyboardButton("✅", callback_data='save'),
                    InlineKeyboardButton("🔄", callback_data='reset')],

                   [InlineKeyboardButton(_("Monday"), callback_data="day_0")],
                   [InlineKeyboardButton(_("Tuesday"), callback_data="day_1")],
                   [InlineKeyboardButton(_("Wednesday"), callback_data="day_2")],
                   [InlineKeyboardButton(_("Thursday"), callback_data="day_3")],
                   [InlineKeyboardButton(_("Friday"), callback_data="day_4")],
                   [InlineKeyboardButton(_("Saturday"), callback_data="day_5")],
                   [InlineKeyboardButton(_("Sunday"), callback_data="day_6")]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

    def reset(self, update, context):
        context.user_data[self.menu_name][self.user_data_key] = []

        context.bot.answer_callback_query(update.callback_query.id)
        self.send_message(context)
        return self.States.ACTION

    def save(self, update, context):

        return self.back(update, context)

    def days_text_set(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        try:
            val = formencode.validators.OneOf()
            value = val.to_python(update.effective_message.text)

            context.user_data[self.menu_name]['date_numbers'] = []
            for ch in value.strftime("%d%m%Y"):
                context.user_data[self.menu_name]['date_numbers'].append(int(ch))

            context.user_data[self.menu_name]['current_number'] = 0

            self.send_message(context)

            return self.States.ACTION

        except Invalid:
            delete_interface(context)
            context.bot.send_message(chat_id=user.chat_id, text=_("Wrong input, try again"))
            self.send_message(context)
            return self.States.ACTION

    def switch_day(self, update, context):
        data = int(update.callback_query.data.replace("day_", ""))

        context.bot.answer_callback_query(update.callback_query.id)

        if data in context.user_data[self.menu_name][self.user_data_key]:
            context.user_data[self.menu_name][self.user_data_key].remove(data)
        else:
            context.user_data[self.menu_name][self.user_data_key].append(data)
            context.user_data[self.menu_name][self.user_data_key] = sorted(context.user_data[self.menu_name][self.user_data_key])

        self.send_message(context)

        return self.States.ACTION

    def get_handler(self):
        states = {self.States.ACTION: [CallbackQueryHandler(self.switch_day, pattern='day_\d'),
                                       CallbackQueryHandler(self.reset, pattern='reset'),
                                       CallbackQueryHandler(self.back, pattern='back'),
                                       CallbackQueryHandler(self.save, pattern='save'),
                                       MessageHandler(Filters.text, self.days_text_set),
                                       MessageHandler(Filters.all, to_state(self.States.ACTION))]}

        additional_states = self.additional_states()
        for key in additional_states:
            if key in states:
                states[key] = additional_states[key] + states[key]
            else:
                states.update({key: additional_states[key]})

        handler = ConversationHandler(entry_points=self.entry_points(),
                                      states=states,
                                      fallbacks=[MessageHandler(Filters.all, unknown_command(-1))],
                                      allow_reentry=True)

        return handler
