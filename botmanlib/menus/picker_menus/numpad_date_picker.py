import datetime
import enum

import formencode
import pytz
from formencode import Invalid
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import ConversationHandler, CallbackQueryHandler, MessageHandler, Filters

from botmanlib.messages import send_or_edit, delete_interface
from ..basemenu import BaseMenu
from ..helpers import to_state, unknown_command


class NumpadDatePicker(BaseMenu):
    menu_name = "date_choser"
    user_data_key = 'date'
    delimiter = '.'
    emoji_keys = True
    emoji_arrows = True

    class States(enum.Enum):
        ACTION = 1

    def entry_points(self):
        raise NotImplementedError

    def back(self, update, context):
        update_objects = getattr(self.parent, 'update_objects', None)
        if update_objects:
            update_objects(context)
        self.parent.send_message(context)
        return ConversationHandler.END

    def additional_states(self):
        return {}

    def entry(self, update, context):
        self._entry(update, context)

        self.send_message(context)
        return self.States.ACTION

    def _entry(self, update, context):
        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}
        if self.user_data_key not in context.user_data[self.menu_name]:
            context.user_data[self.menu_name][self.user_data_key] = None

        current_date = datetime.datetime.now(pytz.timezone('Europe/Kiev')).date()
        context.user_data[self.menu_name]['date_numbers'] = []
        for ch in current_date.strftime("%d%m%Y"):
            context.user_data[self.menu_name]['date_numbers'].append(int(ch))

        context.user_data[self.menu_name]['current_number'] = 0

    def send_message(self, context):
        user = context.user_data['user']
        _ = user.translator

        current_number = context.user_data[self.menu_name]['current_number']
        date = context.user_data[self.menu_name]['date_numbers']

        message_text = "⬜⬜⬜ "
        for idx, number in enumerate(date):
            delimiter = ''
            if idx == 1 or idx == 3:
                delimiter = self.delimiter
            if current_number == idx:
                str_number = "<code>{}</code>".format(number)
            else:
                str_number = str(number)

            message_text += str_number + delimiter

        message_text += " ⬜⬜⬜"

        buttons = [[InlineKeyboardButton("🔙", callback_data='back'),
                    InlineKeyboardButton("✅", callback_data='save'),
                    InlineKeyboardButton("🔄", callback_data='reset')],

                   [InlineKeyboardButton("1️⃣" if self.emoji_keys else "1", callback_data="key_1"),
                    InlineKeyboardButton("2️⃣" if self.emoji_keys else "2", callback_data="key_2"),
                    InlineKeyboardButton("3️⃣" if self.emoji_keys else "3", callback_data="key_3")],
                   [InlineKeyboardButton("4️⃣" if self.emoji_keys else "4", callback_data="key_4"),
                    InlineKeyboardButton("5️⃣" if self.emoji_keys else "5", callback_data="key_5"),
                    InlineKeyboardButton("6️⃣" if self.emoji_keys else "6", callback_data="key_6")],
                   [InlineKeyboardButton("7️⃣" if self.emoji_keys else "7", callback_data="key_7"),
                    InlineKeyboardButton("8️⃣" if self.emoji_keys else "8", callback_data="key_8"),
                    InlineKeyboardButton("9️⃣" if self.emoji_keys else "9", callback_data="key_9")],
                   [InlineKeyboardButton("◀" if self.emoji_arrows else "<", callback_data="prev_number"),
                    InlineKeyboardButton("0️⃣" if self.emoji_keys else "0", callback_data="key_0"),
                    InlineKeyboardButton("▶" if self.emoji_arrows else ">", callback_data="next_number")]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

    def reset(self, update, context):
        context.user_data[self.menu_name][self.user_data_key] = datetime.datetime.now(pytz.timezone('Europe/Kiev')).date()

        context.user_data[self.menu_name]['date_numbers'] = []
        for ch in context.user_data[self.menu_name][self.user_data_key].strftime("%d%m%Y"):
            context.user_data[self.menu_name]['date_numbers'].append(int(ch))
        context.user_data[self.menu_name]['current_number'] = 0

        context.bot.answer_callback_query(update.callback_query.id)
        self.send_message(context)
        return self.States.ACTION

    def save(self, update, context):
        _ = context.user_data['user'].translator

        day = int("{0}{1}".format(context.user_data[self.menu_name]['date_numbers'][0], context.user_data[self.menu_name]['date_numbers'][1]))
        month = int("{0}{1}".format(context.user_data[self.menu_name]['date_numbers'][2], context.user_data[self.menu_name]['date_numbers'][3]))
        year = int("{0}{1}{2}{3}".format(context.user_data[self.menu_name]['date_numbers'][4], context.user_data[self.menu_name]['date_numbers'][5], context.user_data[self.menu_name]['date_numbers'][6], context.user_data[self.menu_name]['date_numbers'][7]))

        try:
            date = datetime.date(year, month, day)
            if date < datetime.datetime.now(pytz.timezone('Europe/Kiev')).date():
                raise ValueError()

            context.user_data[self.menu_name][self.user_data_key] = date
            return self.back(update, context)
        except ValueError:
            context.bot.answer_callback_query(update.callback_query.id, text=_("Wrong date"), show_alert=True)
            return self.States.ACTION

    def date_text_set(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        try:
            val = formencode.validators.DateConverter(month_style='dd/mm/yyyy')
            value = val.to_python(update.effective_message.text)

            context.user_data[self.menu_name]['date_numbers'] = []
            for ch in value.strftime("%d%m%Y"):
                context.user_data[self.menu_name]['date_numbers'].append(int(ch))

            context.user_data[self.menu_name]['current_number'] = 0

            self.send_message(context)

            return self.States.ACTION

        except Invalid:
            delete_interface(context)
            context.bot.send_message(chat_id=user.chat_id, text=_("Wrong input, try again"))
            self.send_message(context)
            return self.States.ACTION

    def next_number(self, update, context):
        if context.user_data[self.menu_name]['current_number'] < 7:
            context.user_data[self.menu_name]['current_number'] += 1
        else:
            context.user_data[self.menu_name]['current_number'] = 0

        self.send_message(context)
        context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def prev_number(self, update, context):
        if context.user_data[self.menu_name]['current_number'] >= 0:
            context.user_data[self.menu_name]['current_number'] -= 1
        else:
            context.user_data[self.menu_name]['current_number'] = 3

        self.send_message(context)
        context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def fill_number(self, update, context):
        data = int(update.callback_query.data.replace("key_", ""))

        context.bot.answer_callback_query(update.callback_query.id)

        date = context.user_data[self.menu_name]['date_numbers']
        current_number = context.user_data[self.menu_name]['current_number']

        if current_number == 0 and data > 3:
            return self.States.ACTION
        if current_number == 1 and date[0] >= 3 and data > 1:
            return self.States.ACTION
        if current_number == 2 and data > 1:
            return self.States.ACTION
        if current_number == 3 and date[2] >= 1 and data > 2:
            return self.States.ACTION
        if current_number == 4 and data != 2:
            return self.States.ACTION

        context.user_data[self.menu_name]['date_numbers'][context.user_data[self.menu_name]['current_number']] = data

        return self.next_number(update, context)

    def get_handler(self):
        states = {self.States.ACTION: [CallbackQueryHandler(self.fill_number, pattern='key_\d'),
                                       CallbackQueryHandler(self.prev_number, pattern='prev_number'),
                                       CallbackQueryHandler(self.next_number, pattern='next_number'),
                                       CallbackQueryHandler(self.reset, pattern='reset'),
                                       CallbackQueryHandler(self.back, pattern='back'),
                                       CallbackQueryHandler(self.save, pattern='save'),
                                       MessageHandler(Filters.text, self.date_text_set),
                                       MessageHandler(Filters.all, to_state(self.States.ACTION))]}

        additional_states = self.additional_states()
        for key in additional_states:
            if key in states:
                states[key] = additional_states[key] + states[key]
            else:
                states.update({key: additional_states[key]})

        handler = ConversationHandler(entry_points=self.entry_points(),
                                      states=states,
                                      fallbacks=[MessageHandler(Filters.all, unknown_command(-1))],
                                      allow_reentry=True)

        return handler
