import datetime
import enum

import formencode
from formencode import Invalid
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import ConversationHandler, CallbackQueryHandler, MessageHandler, Filters, CallbackContext

from botmanlib.messages import send_or_edit, delete_interface
from ..basemenu import BaseMenu
from ..helpers import to_state, unknown_command


class ButtonTimePicker(BaseMenu):
    menu_name = "time_choser"
    user_data_key = 'time'

    class States(enum.Enum):
        ACTION = 1

    def entry_points(self):
        raise NotImplementedError

    def back(self, update, context: CallbackContext):
        raise NotImplementedError

    def additional_states(self):
        return {}

    def entry(self, update, context: CallbackContext):
        self._load(context)

        self.send_message(context)
        return self.States.ACTION

    def _load(self, context: CallbackContext):
        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}
        if self.user_data_key not in context.user_data[self.menu_name] or context.user_data[self.menu_name][self.user_data_key] is None:
            context.user_data[self.menu_name][self.user_data_key] = datetime.time(0, 0, 0, 0)

    def send_message(self, context):
        user = context.user_data['user']
        _ = user.translator

        time = context.user_data[self.menu_name][self.user_data_key]

        message_text = "⬜⬜⬜⬜⬜⬜ {hour:02d} : {minute:02d} ⬜⬜⬜⬜⬜⬜".format(hour=time.hour, minute=time.minute)
        buttons = [[InlineKeyboardButton("🔙", callback_data=f'back_{self.menu_name}'),
                    InlineKeyboardButton("✅", callback_data='save'),
                    InlineKeyboardButton("🔄", callback_data='reset')],
                   [InlineKeyboardButton("🔼", callback_data="hour_up"),
                    InlineKeyboardButton("⏫", callback_data="hour_double_up"),
                    InlineKeyboardButton("⏫", callback_data="minute_double_up"),
                    InlineKeyboardButton("🔼", callback_data="minute_up")],
                   [InlineKeyboardButton("🔽", callback_data="hour_down"),
                    InlineKeyboardButton("⏬", callback_data="hour_double_down"),
                    InlineKeyboardButton("⏬", callback_data="minute_double_down"),
                    InlineKeyboardButton("🔽", callback_data="minute_down")]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons))

    def hour_up(self, update, context: CallbackContext):
        delta = datetime.timedelta(hours=1)
        t = context.user_data[self.menu_name][self.user_data_key]

        context.user_data[self.menu_name][self.user_data_key] = (datetime.datetime.combine(datetime.date(1, 1, 1), t) + delta).time()
        update.callback_query.answer()
        self.send_message(context)
        return self.States.ACTION

    def hour_double_up(self, update, context: CallbackContext):
        delta = datetime.timedelta(hours=5)
        t = context.user_data[self.menu_name][self.user_data_key]

        context.user_data[self.menu_name][self.user_data_key] = (datetime.datetime.combine(datetime.date(1, 1, 1), t) + delta).time()
        update.callback_query.answer()
        self.send_message(context)
        return self.States.ACTION

    def hour_down(self, update, context: CallbackContext):
        delta = datetime.timedelta(hours=1)
        t = context.user_data[self.menu_name][self.user_data_key]

        context.user_data[self.menu_name][self.user_data_key] = (datetime.datetime.combine(datetime.date(100, 1, 1), t) - delta).time()
        update.callback_query.answer()
        self.send_message(context)
        return self.States.ACTION

    def hour_double_down(self, update, context: CallbackContext):
        delta = datetime.timedelta(hours=5)
        t = context.user_data[self.menu_name][self.user_data_key]

        context.user_data[self.menu_name][self.user_data_key] = (datetime.datetime.combine(datetime.date(100, 1, 1), t) - delta).time()
        update.callback_query.answer()
        self.send_message(context)
        return self.States.ACTION

    def minute_up(self, update, context: CallbackContext):
        delta = datetime.timedelta(seconds=60)
        t = context.user_data[self.menu_name][self.user_data_key]

        context.user_data[self.menu_name][self.user_data_key] = (datetime.datetime.combine(datetime.date(1, 1, 1), t) + delta).time()
        update.callback_query.answer()
        self.send_message(context)
        return self.States.ACTION

    def minute_double_up(self,update, context: CallbackContext):
        delta = datetime.timedelta(seconds=60 * 10)
        t = context.user_data[self.menu_name][self.user_data_key]

        context.user_data[self.menu_name][self.user_data_key] = (datetime.datetime.combine(datetime.date(1, 1, 1), t) + delta).time()
        update.callback_query.answer()
        self.send_message(context)
        return self.States.ACTION

    def minute_down(self, update, context: CallbackContext):
        delta = datetime.timedelta(seconds=60)
        t = context.user_data[self.menu_name][self.user_data_key]

        context.user_data[self.menu_name][self.user_data_key] = (datetime.datetime.combine(datetime.date(100, 1, 1), t) - delta).time()
        update.callback_query.answer()
        self.send_message(context)
        return self.States.ACTION

    def minute_double_down(self, update, context: CallbackContext):
        delta = datetime.timedelta(seconds=60 * 10)
        t = context.user_data[self.menu_name][self.user_data_key]

        context.user_data[self.menu_name][self.user_data_key] = (datetime.datetime.combine(datetime.date(100, 1, 1), t) - delta).time()
        update.callback_query.answer()
        self.send_message(context)
        return self.States.ACTION

    def reset(self, update, context: CallbackContext):
        context.user_data[self.menu_name][self.user_data_key] = datetime.time(0, 0, 0, 0)
        update.callback_query.answer()
        self.send_message(context)
        return self.States.ACTION

    def save(self, update, context: CallbackContext):
        return self.back(update, context)

    def time_text_set(self, update, context: CallbackContext):
        user = context.user_data['user']
        _ = user.translator

        try:
            val = formencode.validators.TimeConverter()
            value = val.to_python(update.message.text)

            context.user_data[self.menu_name][self.user_data_key] = datetime.time(value[0], value[1], 0, 0)
            self.send_message(context)

            return self.States.ACTION

        except Invalid:
            delete_interface(context)
            context.bot.send_message(chat_id=user.chat_id, text=_("Wrong input, try again"))
            self.send_message(context)
            return self.States.ACTION

    def get_handler(self):
        states = {self.States.ACTION: [CallbackQueryHandler(self.hour_up, pattern='hour_up'),
                                       CallbackQueryHandler(self.hour_double_up, pattern='hour_double_up'),
                                       CallbackQueryHandler(self.hour_down, pattern='hour_down'),
                                       CallbackQueryHandler(self.hour_double_down, pattern='hour_double_down'),

                                       CallbackQueryHandler(self.minute_up, pattern='minute_up'),
                                       CallbackQueryHandler(self.minute_double_up, pattern='minute_double_up'),
                                       CallbackQueryHandler(self.minute_down, pattern='minute_down'),
                                       CallbackQueryHandler(self.minute_double_down, pattern='minute_double_down'),

                                       CallbackQueryHandler(self.reset, pattern='reset'),
                                       CallbackQueryHandler(self.back, pattern=f'back_{self.menu_name}'),
                                       CallbackQueryHandler(self.save, pattern='save'),
                                       MessageHandler(Filters.text, self.time_text_set),
                                       MessageHandler(Filters.all, to_state(self.States.ACTION))]}

        additional_states = self.additional_states()
        for key in additional_states:
            if key in states:
                states[key] = additional_states[key] + states[key]
            else:
                states.update({key: additional_states[key]})

        handler = ConversationHandler(entry_points=self.entry_points(),
                                      states=states,
                                      fallbacks=[MessageHandler(Filters.all, unknown_command(-1))],
                                      allow_reentry=True)

        return handler
