import datetime
from abc import ABC

from dateutil.relativedelta import relativedelta
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, Update, ParseMode
from telegram.ext import CallbackQueryHandler, ConversationHandler, MessageHandler, Filters, CallbackContext

from botmanlib.menus import BaseMenu
from botmanlib.messages import send_or_edit, delete_interface, delete_user_message


class CalendarDatePickerMenu(BaseMenu, ABC):
    menu_name = 'calendar_date_picker_menu'
    only_future_dates = False
    only_past_dates = False
    include_today = True
    disable_web_page_preview = False
    parse_mode = ParseMode.HTML

    def entry(self, update: Update, context: CallbackContext):
        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        today = datetime.date.today()
        context.user_data[self.menu_name]['day'] = today.day
        context.user_data[self.menu_name]['month'] = today.month
        context.user_data[self.menu_name]['year'] = today.year

        self.send_message(context)
        return self.States.ACTION

    def send_message(self, context: CallbackContext):
        user = context.user_data['user']

        message_text = self.message_text(context)

        buttons = []

        calendar_buttons = self.calendar_buttons(context)
        if calendar_buttons:
            buttons.extend(calendar_buttons)

        control_row = []
        prev_button = self.prev_button(context)
        if prev_button:
            control_row.append(prev_button)

        back_button = self.back_button(context)
        if back_button:
            control_row.append(back_button)

        next_button = self.next_button(context)
        if next_button:
            control_row.append(next_button)

        if control_row:
            buttons.append(control_row)

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode=self.parse_mode, disable_web_page_preview=self.disable_web_page_preview)

    def message_text(self, context: CallbackContext):
        _ = context.user_data['user'].translator
        return _("Please select a date:")

    def entry_points(self):
        raise NotImplementedError

    def back(self, update: Update, context: CallbackContext):
        update_objects = getattr(self.parent, 'update_objects', None)
        if update_objects:
            update_objects(context)
        delete_interface(context, 'ask_location')
        self.parent.send_message(context)
        return ConversationHandler.END

    def month_name(self, context: CallbackContext, month: int):
        _ = context.user_data['user'].translator
        if month == 1:
            return _("January")
        elif month == 2:
            return _("February")
        elif month == 3:
            return _("March")
        elif month == 4:
            return _("April")
        elif month == 5:
            return _("May")
        elif month == 6:
            return _("June")
        elif month == 7:
            return _("July")
        elif month == 8:
            return _("August")
        elif month == 9:
            return _("September")
        elif month == 10:
            return _("October")
        elif month == 11:
            return _("November")
        elif month == 12:
            return _("December")
        else:
            return _("Unknown")

    def back_button(self, context: CallbackContext):
        _ = context.user_data['user'].translator
        return InlineKeyboardButton(_("Back"), callback_data=f"back_{self.menu_name}")

    def prev_button(self, context: CallbackContext):
        return InlineKeyboardButton("◀", callback_data=f"prev_month_{self.menu_name}")

    def next_button(self, context: CallbackContext):
        return InlineKeyboardButton("▶", callback_data=f"next_month_{self.menu_name}")

    def month_button(self, context: CallbackContext):
        button_text = f"{self.month_name(context, context.user_data[self.menu_name]['month'])} {context.user_data[self.menu_name]['year']}"
        return InlineKeyboardButton(button_text, callback_data=f"month_{context.user_data[self.menu_name]['month']}")

    def weekdays_buttons(self, context: CallbackContext):
        _ = context.user_data['user'].translator
        day_names_row = [InlineKeyboardButton(_("Mon."), callback_data="weekday_0"),
                         InlineKeyboardButton(_("Tue."), callback_data="weekday_1"),
                         InlineKeyboardButton(_("Wed."), callback_data="weekday_2"),
                         InlineKeyboardButton(_("Thu."), callback_data="weekday_3"),
                         InlineKeyboardButton(_("Fri."), callback_data="weekday_4"),
                         InlineKeyboardButton(_("Sat."), callback_data="weekday_5"),
                         InlineKeyboardButton(_("Sun."), callback_data="weekday_6")]

        return day_names_row

    def current_date(self, context: CallbackContext):
        return datetime.date(context.user_data[self.menu_name]['year'],
                             context.user_data[self.menu_name]['month'],
                             context.user_data[self.menu_name]['day'] if context.user_data[self.menu_name]['day'] is not None else 1)

    def calendar_buttons(self, context: CallbackContext):
        user = context.user_data['user']
        _ = user.translator
        current_date = self.current_date(context)
        excluded_dates = self.excluded_dates(context)
        hidden_dates = self.hidden_dates(context)
        marked_dates = self.marked_dates(context)
        buttons = []

        month_name_button = self.month_button(context)
        if month_name_button:
            buttons.append([month_name_button])

        week_days_buttons = self.weekdays_buttons(context)
        if week_days_buttons:
            buttons.append(week_days_buttons)

        next_month = current_date.replace(day=28) + datetime.timedelta(days=4)
        last_date_of_month = (next_month - datetime.timedelta(days=next_month.day))
        days_matrix = []
        days_row = []

        # previous month dates
        for i in range(0, current_date.replace(day=1).weekday()):
            days_row.append(InlineKeyboardButton(" ", callback_data="nothing"))

        # current month dates
        for day in range(1, last_date_of_month.day + 1):
            date = datetime.date(context.user_data[self.menu_name]['year'],
                                 context.user_data[self.menu_name]['month'],
                                 day)

            if date.weekday() == 0:
                days_matrix.append(days_row)
                days_row = []

            if self.only_future_dates and context.user_data[self.menu_name]['day'] is not None and date < current_date:
                days_row.append(InlineKeyboardButton(" ", callback_data=f"nothing"))
            elif self.only_past_dates and context.user_data[self.menu_name]['day'] is not None and date > current_date:
                days_row.append(InlineKeyboardButton(" ", callback_data=f"nothing"))
            elif date in excluded_dates:
                days_row.append(InlineKeyboardButton("❌", callback_data=f"nothing"))
            elif date in hidden_dates:
                days_row.append(InlineKeyboardButton(" ", callback_data=f"nothing"))
            elif not self.include_today and day == context.user_data[self.menu_name]['day']:
                days_row.append(InlineKeyboardButton("🔸", callback_data=f"nothing"))
            else:
                if date in marked_dates:
                    button_text = marked_dates[date]
                elif context.user_data[self.menu_name]['day'] is not None and day == context.user_data[self.menu_name]['day']:
                    button_text = f"[{day}]"
                else:
                    button_text = str(day)
                days_row.append(InlineKeyboardButton(button_text, callback_data=f"day_{day}"))

        # next month dates
        for i in range(last_date_of_month.weekday(), 6):
            days_row.append(InlineKeyboardButton(" ", callback_data="nothing"))

        if days_row:
            days_matrix.append(days_row)

        buttons.extend(days_matrix)

        return buttons

    def prev_month(self, update: Update, context: CallbackContext):
        _ = context.user_data['user'].translator
        today = datetime.date.today()

        # create date object
        current_date = self.current_date(context)

        if self.only_future_dates and current_date <= today:
            update.callback_query.answer(text=_("You can select only future date"))
            return self.States.ACTION

        # substract date
        current_date += relativedelta(months=-1)

        # update current date variables
        context.user_data[self.menu_name]['year'] = current_date.year
        context.user_data[self.menu_name]['month'] = current_date.month
        context.user_data[self.menu_name]['day'] = current_date.day

        # remove current day
        if context.user_data[self.menu_name]['month'] == today.month and context.user_data[self.menu_name]['year'] == today.year:
            context.user_data[self.menu_name]['day'] = today.day
        else:
            context.user_data[self.menu_name]['day'] = None

        self.send_message(context)
        update.callback_query.answer()
        return self.States.ACTION

    def next_month(self, update: Update, context: CallbackContext):
        _ = context.user_data['user'].translator
        today = datetime.date.today()

        # create date object
        current_date = self.current_date(context)
        if self.only_past_dates and current_date >= today:
            update.callback_query.answer(text=_("You can select only past date"))
            return self.States.ACTION

        # add date
        current_date += relativedelta(months=1)

        # update current date variables
        context.user_data[self.menu_name]['year'] = current_date.year
        context.user_data[self.menu_name]['month'] = current_date.month
        context.user_data[self.menu_name]['day'] = current_date.day

        # remove current day
        if context.user_data[self.menu_name]['month'] == today.month and context.user_data[self.menu_name]['year'] == today.year:
            context.user_data[self.menu_name]['day'] = today.day
        else:
            context.user_data[self.menu_name]['day'] = None

        self.send_message(context)
        update.callback_query.answer()
        return self.States.ACTION

    def reset_current_date(self, update: Update, context: CallbackContext):
        today = datetime.date.today()
        context.user_data[self.menu_name]['day'] = today.day
        context.user_data[self.menu_name]['month'] = today.month
        context.user_data[self.menu_name]['year'] = today.year

        self.send_message(context)
        update.callback_query.answer()
        return self.States.ACTION

    def additional_states(self):
        return {}

    def fallbacks(self):
        return [MessageHandler(Filters.all, lambda update, context: delete_user_message(update))]

    def nothing(self, update: Update, context: CallbackContext):
        update.callback_query.answer()
        return self.States.ACTION

    def excluded_dates(self, context: CallbackContext):
        return []

    def hidden_dates(self, context: CallbackContext):
        return []

    def marked_dates(self, context: CallbackContext):
        return {}

    def day_selected(self, update: Update, context: CallbackContext):
        update.callback_query.answer()
        day = int(update.callback_query.data.replace("day_", ""))
        selected_date = self.current_date(context).replace(day=day)
        raise NotImplementedError

    def get_handler(self):
        states = {self.States.ACTION: [CallbackQueryHandler(self.day_selected, pattern=r'^day_\d+$'),
                                       CallbackQueryHandler(self.nothing, pattern=r'^weekday_\d$'),
                                       CallbackQueryHandler(self.reset_current_date, pattern=r'^month_\d+$'),
                                       CallbackQueryHandler(self.nothing, pattern=r'^nothing$'),
                                       CallbackQueryHandler(self.prev_month, pattern=f'^prev_month_{self.menu_name}$'),
                                       CallbackQueryHandler(self.next_month, pattern=f'^next_month_{self.menu_name}$'),
                                       CallbackQueryHandler(self.back, pattern=f'^back_{self.menu_name}$'),
                                       ]
                  }

        additional_states = self.additional_states()
        for key in additional_states:
            if key in states:
                states[key] = additional_states[key] + states[key]
            else:
                states.update({key: additional_states[key]})

        handler = ConversationHandler(entry_points=self.entry_points(),
                                      states=states,
                                      fallbacks=self.fallbacks(),
                                      allow_reentry=True)

        return handler
