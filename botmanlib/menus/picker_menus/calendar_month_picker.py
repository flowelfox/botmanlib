import datetime
from abc import ABC

from dateutil.relativedelta import relativedelta
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, Update, ParseMode
from telegram.ext import CallbackQueryHandler, ConversationHandler, MessageHandler, Filters, CallbackContext

from botmanlib.menus import BaseMenu
from botmanlib.menus.helpers import group_buttons
from botmanlib.messages import send_or_edit, delete_interface, delete_user_message


class CalendarMonthPickerMenu(BaseMenu, ABC):
    menu_name = 'calendar_month_picker_menu'
    only_future_dates = False
    only_past_dates = False
    disable_web_page_preview = False
    parse_mode = ParseMode.HTML

    def entry(self, update: Update, context: CallbackContext):
        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        today = datetime.date.today()
        context.user_data[self.menu_name]['day'] = None
        context.user_data[self.menu_name]['month'] = today.month
        context.user_data[self.menu_name]['year'] = today.year

        self.send_message(context)
        return self.States.ACTION

    def send_message(self, context: CallbackContext):
        user = context.user_data['user']

        message_text = self.message_text(context)

        buttons = []

        calendar_buttons = self.calendar_buttons(context)
        if calendar_buttons:
            buttons.extend(calendar_buttons)

        control_row = []
        prev_button = self.prev_button(context)
        if prev_button:
            control_row.append(prev_button)

        back_button = self.back_button(context)
        if back_button:
            control_row.append(back_button)

        next_button = self.next_button(context)
        if next_button:
            control_row.append(next_button)

        if control_row:
            buttons.append(control_row)

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode=self.parse_mode, disable_web_page_preview=self.disable_web_page_preview)

    def message_text(self, context: CallbackContext):
        _ = context.user_data['user'].translator
        return _("Please select a month:")

    def entry_points(self):
        raise NotImplementedError

    def back(self, update: Update, context: CallbackContext):
        update_objects = getattr(self.parent, 'update_objects', None)
        if update_objects:
            update_objects(context)
        delete_interface(context, 'ask_location')
        self.parent.send_message(context)
        return ConversationHandler.END

    def month_name(self, context: CallbackContext, month: int):
        _ = context.user_data['user'].translator
        if month == 1:
            return _("January")
        elif month == 2:
            return _("February")
        elif month == 3:
            return _("March")
        elif month == 4:
            return _("April")
        elif month == 5:
            return _("May")
        elif month == 6:
            return _("June")
        elif month == 7:
            return _("July")
        elif month == 8:
            return _("August")
        elif month == 9:
            return _("September")
        elif month == 10:
            return _("October")
        elif month == 11:
            return _("November")
        elif month == 12:
            return _("December")
        else:
            return _("Unknown")

    def back_button(self, context: CallbackContext):
        _ = context.user_data['user'].translator
        return InlineKeyboardButton(_("Back"), callback_data=f"back_{self.menu_name}")

    def prev_button(self, context: CallbackContext):
        return InlineKeyboardButton("◀", callback_data=f"prev_year_{self.menu_name}")

    def next_button(self, context: CallbackContext):
        return InlineKeyboardButton("▶", callback_data=f"next_year_{self.menu_name}")

    def month_button(self, context: CallbackContext):
        button_text = f"{context.user_data[self.menu_name]['year']}"
        return InlineKeyboardButton(button_text, callback_data=f"year_{context.user_data[self.menu_name]['year']}")

    def current_date(self, context: CallbackContext):
        return datetime.date(context.user_data[self.menu_name]['year'],
                             context.user_data[self.menu_name]['month'],
                             context.user_data[self.menu_name]['day'] if context.user_data[self.menu_name]['day'] is not None else 1)

    def calendar_buttons(self, context: CallbackContext):
        user = context.user_data['user']
        _ = user.translator
        current_date = self.current_date(context)
        excluded_months = self.excluded_months(context)
        hidden_months = self.hidden_months(context)
        marked_months = self.marked_months(context)
        today = datetime.date.today()
        buttons = []

        month_name_button = self.month_button(context)
        if month_name_button:
            buttons.append([month_name_button])

        # month buttons
        months_buttons = []
        for month in range(1, 13):
            date = datetime.date(context.user_data[self.menu_name]['year'],
                                 month,
                                 1)

            if self.only_future_dates and date < current_date:
                months_buttons.append(InlineKeyboardButton(f"[{self.month_name(context, month)}]", callback_data=f"nothing"))
            elif self.only_past_dates and date > current_date:
                months_buttons.append(InlineKeyboardButton(f"[{self.month_name(context, month)}]", callback_data=f"nothing"))
            elif (date.year, date.month) in excluded_months:
                months_buttons.append(InlineKeyboardButton(f"❌{self.month_name(context, month)}❌", callback_data=f"nothing"))
            elif (date.year, date.month) in hidden_months:
                pass
            else:
                if (date.year, date.month) in marked_months:
                    button_text = f"{marked_months[(date.year, date.month)]}{self.month_name(context, month)}{marked_months[(date.year, date.month)]}"
                elif context.user_data[self.menu_name]['month'] == month and today.year == context.user_data[self.menu_name]['year']:
                    button_text = f"[{self.month_name(context, month)}]"
                else:
                    button_text = self.month_name(context, month)
                months_buttons.append(InlineKeyboardButton(button_text, callback_data=f"month_{month}"))

        buttons.extend(group_buttons(months_buttons, 3))
        return buttons

    def prev_year(self, update: Update, context: CallbackContext):
        _ = context.user_data['user'].translator
        today = datetime.date.today()

        # create date object
        current_date = self.current_date(context)

        if self.only_future_dates and current_date <= today:
            update.callback_query.answer(text=_("You can select only future date"))
            return self.States.ACTION

        # substract date
        current_date += relativedelta(years=-1)

        # update current date variables
        context.user_data[self.menu_name]['year'] = current_date.year
        context.user_data[self.menu_name]['month'] = current_date.month
        context.user_data[self.menu_name]['day'] = current_date.day

        self.send_message(context)
        update.callback_query.answer()
        return self.States.ACTION

    def next_year(self, update: Update, context: CallbackContext):
        _ = context.user_data['user'].translator
        today = datetime.date.today()

        # create date object
        current_date = self.current_date(context)
        if self.only_past_dates and current_date >= today:
            update.callback_query.answer(text=_("You can select only past date"))
            return self.States.ACTION

        # add date
        current_date += relativedelta(years=1)

        # update current date variables
        context.user_data[self.menu_name]['year'] = current_date.year
        context.user_data[self.menu_name]['month'] = current_date.month
        context.user_data[self.menu_name]['day'] = current_date.day

        self.send_message(context)
        update.callback_query.answer()
        return self.States.ACTION

    def year_selected(self, update: Update, context: CallbackContext):
        return self.reset_current_date(update, context)

    def reset_current_date(self, update: Update, context: CallbackContext):
        today = datetime.date.today()
        context.user_data[self.menu_name]['day'] = today.day
        context.user_data[self.menu_name]['month'] = today.month
        context.user_data[self.menu_name]['year'] = today.year

        self.send_message(context)
        update.callback_query.answer()
        return self.States.ACTION

    def additional_states(self):
        return {}

    def fallbacks(self):
        return [MessageHandler(Filters.all, lambda update, context: delete_user_message(update))]

    def nothing(self, update: Update, context: CallbackContext):
        update.callback_query.answer()
        return self.States.ACTION

    def excluded_months(self, context: CallbackContext):
        return []

    def hidden_months(self, context: CallbackContext):
        return []

    def marked_months(self, context: CallbackContext):
        return {}

    def month_selected(self, update: Update, context: CallbackContext):
        update.callback_query.answer()
        month = int(update.callback_query.data.replace("month_", ""))
        selected_date = self.current_date(context).replace(month=month)
        raise NotImplementedError

    def get_handler(self):
        states = {self.States.ACTION: [CallbackQueryHandler(self.month_selected, pattern=r'^month_\d+$'),
                                       CallbackQueryHandler(self.year_selected, pattern=r'^year_\d+$'),
                                       CallbackQueryHandler(self.nothing, pattern=r'^nothing$'),
                                       CallbackQueryHandler(self.prev_year, pattern=f'^prev_year_{self.menu_name}$'),
                                       CallbackQueryHandler(self.next_year, pattern=f'^next_year_{self.menu_name}$'),
                                       CallbackQueryHandler(self.back, pattern=f'^back_{self.menu_name}$'),
                                       ]
                  }

        additional_states = self.additional_states()
        for key in additional_states:
            if key in states:
                states[key] = additional_states[key] + states[key]
            else:
                states.update({key: additional_states[key]})

        handler = ConversationHandler(entry_points=self.entry_points(),
                                      states=states,
                                      fallbacks=self.fallbacks(),
                                      allow_reentry=True)

        return handler
