import datetime
import enum
import logging
from abc import ABC

from telegram import Update, CallbackQuery, Message, Chat, User as TUser
from telegram.ext import ConversationHandler

from botmanlib.menus.helpers import prepare_user
from botmanlib.messages import get_user_data, send_or_edit, remove_interface, delete_interface, remove_interface_markup, delete_user_message, resend_interface


class States(enum.Enum):
    ACTION = 1


class BaseMenu(ABC):
    """Abstract base class for creating botmanlib menus

    Note:
        `Bot` and `dispatcher` args must be passing when parent is not provided,
        otherwise bot and dispatcher parameters would automatically
        get parent menu instances for this args

    Args:
        parent: Parent menu
        bot: Telegram bot instance
        dispatcher: Dispatcher instance
    """

    def __init__(self, parent=None, bot=None, dispatcher=None):
        self.parent = parent

        if bot is None:
            menu = self
            self.bot = None
            while menu.parent is not None:
                if menu.parent and menu.parent.bot:
                    self.bot = menu.parent.bot
                    break
                else:
                    menu = menu.parent
            if self.bot is None:
                raise ValueError("`bot` must be passed at least to root menu.")
        else:
            self.bot = bot

        if dispatcher is None:
            menu = self
            self.dispatcher = None
            while menu.parent is not None:
                if menu.parent and menu.parent.dispatcher:
                    self.dispatcher = menu.parent.dispatcher
                    break
                else:
                    menu = menu.parent
            if self.dispatcher is None:
                raise ValueError("`dispatcher` must be passed at least to root menu.")
        else:
            self.dispatcher = dispatcher

        if not hasattr(self, "States"):
            self.States = States

        if not hasattr(self.States, 'ACTION'):
            self.States = enum.Enum("States", [m.name for m in self.States] + ['ACTION'])

        self.logger = logging.getLogger(self.__class__.__name__)
        self.handler = self.get_handler()
        self.update_queue = self.dispatcher.update_queue
        self.job_queue = self.dispatcher.job_queue

    def fake_callback_update(self, user, callback_data, callback_query_id=0):
        """Create fake callback update. Created update would be added to :attr:`~telegram.ext.Updater.update_queue`.

        Warning:
            Fake update has id equal to zero.

        Args:
            user (:class:`~botmanlib.models.BaseUser` instance): User which would be set as :attr:`~telegram.Update.effective_user` of update.
            callback_data (:obj:`str`): Callback data string which would be set as :attr:`~telegram.CallbackQuery.data`.
            callback_query_id (:obj:`int`, optional): Callback query id which would be set as :attr:`~telegram.CallbackQuery.id`.
        """
        update = Update(0)
        update._effective_user = TUser(user.chat_id, user.first_name, False, user.last_name, user.username, user.language_code, bot=self.bot)
        update._effective_chat = Chat(user.chat_id, Chat.PRIVATE)
        update._effective_message = None
        update.callback_query = CallbackQuery(callback_query_id, update.effective_user, update.effective_chat, Message(0, update.effective_user, datetime.datetime.now(), update.effective_chat), bot=self.bot, data=callback_data)
        self.update_queue.put(update)

    def fake_message_update(self, user, text, message_id=0):
        """Create fake message update. Created update would be added to :attr:`~telegram.ext.Updater.update_queue`.

        Warning:
            Fake update has id equal to zero.

        Args:
            user (:class:`~botmanlib.models.BaseUser` instance): User which would be set as :attr:`~telegram.Update.effective_user` of update.
            text (:obj:`str`): Text string which would be set as :attr:`~telegram.Message.text`.
            message_id (:obj:`int`, optional): Message id which would be set as :attr:`~telegram.Message.id`.
        """
        update = Update(0)
        update._effective_user = TUser(user.chat_id, user.first_name, False, user.last_name, user.username, user.language_code, bot=self.bot)
        update._effective_chat = Chat(user.chat_id, Chat.PRIVATE)
        update._effective_message = None
        update.message = Message(message_id, update.effective_user, datetime.datetime.utcnow(), update.effective_chat, text=text, bot=self.bot)
        self.update_queue.put(update)

    def conv_fallback(self, context):
        """
        This function was called when some error in bot occurs.
        :param context:
        :return: -1
        """
        user_data = context.user_data
        if 'keyboard' in user_data and user_data['keyboard']:
            self.bot.send_message(chat_id=user_data['user'].chat_id, text="Something went wrong, try again later.", reply_markup=user_data['keyboard'])
            del user_data['keyboard']
        else:
            self.bot.send_message(chat_id=user_data['user'].chat_id, text="Something went wrong, try again later.")

        return ConversationHandler.END

    def get_handler(self):
        raise NotImplementedError

    def send_message(self, context):
        raise NotImplementedError

    def clear_states(self, context, menu_instance=None, chat_id=None):
        if chat_id:
            user_data = get_user_data(self.dispatcher, chat_id)
        else:
            user_data = context.user_data

        if menu_instance is None:
            menu_instance = self

        user = user_data['user']
        key = (user.chat_id, user.chat_id)
        if key in menu_instance.handler.conversations:
            del menu_instance.handler.conversations[key]

    def clear_all_states(self, context, chat_id=None):
        if chat_id:
            user_data = get_user_data(self.dispatcher, chat_id)
        else:
            user_data = context.user_data

        user = user_data['user']
        key1 = (user.chat_id, user.chat_id)
        key2 = (user.chat_id, )

        def delete_state(handlers):
            for handler in [h for h in handlers if isinstance(h, ConversationHandler)]:
                if key1 in handler.conversations:
                    del handler.conversations[key1]
                if key2 in handler.conversations:
                    del handler.conversations[key2]
                inner_handlers = []
                [inner_handlers.extend(handler.states[state]) for state in handler.states]
                delete_state(inner_handlers)

        delete_state(self.dispatcher.handlers[0])

    def back_to_menu(self, update, context):
        self.send_message(context)
        if update.callback_query:
            update.callback_query.answer()
        return self.States.ACTION

    def send_or_edit(self, context, interface_name='interface', user_id=None, dispatcher=None, **kwargs):
        from warnings import warn
        warn("You should use botmanlib.messages.send_or_edit function instead")
        return send_or_edit(context, interface_name, user_id, dispatcher, **kwargs)

    def prepare_user(self, user_model, update, context, lang=None):
        from warnings import warn
        warn("You should use botmanlib.messages.prepare_user function instead")
        return prepare_user(user_model, update, context, lang)

    def delete_interface(self, context, interface_name='interface', user_id=None, dispatcher=None):
        from warnings import warn
        warn("You should use botmanlib.messages.delete_interface function instead")
        return delete_interface(context, interface_name, user_id, dispatcher)

    def remove_interface(self, context, interface_name='interface', user_id=None, dispatcher=None):
        from warnings import warn
        warn("You should use botmanlib.messages.remove_interface function instead")
        return remove_interface(context, interface_name, user_id, dispatcher)

    def remove_interface_markup(self, context, interface_name='interface', user_id=None, dispatcher=None):
        from warnings import warn
        warn("You should use botmanlib.messages.remove_interface_markup function instead")
        return remove_interface_markup(context, interface_name, user_id, dispatcher)

    def resend_interface(self, context, interface_name='interface', user_id=None, dispatcher=None):
        from warnings import warn
        warn("You should use botmanlib.messages.resend_interface function instead")
        return resend_interface(context, interface_name, user_id, dispatcher)

    def delete_user_message(self, update):
        from warnings import warn
        warn("You should use botmanlib.messages.delete_user_message function instead")
        return delete_user_message(update)
