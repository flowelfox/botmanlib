from sqlalchemy import or_
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, Update, ParseMode
from telegram.ext import ConversationHandler, CallbackQueryHandler, MessageHandler, Filters, CallbackContext

from botmanlib.menus import BaseMenu, OneListMenu, BunchListMenu
from botmanlib.menus.helpers import to_state, add_to_db, group_buttons, require_permission
from botmanlib.messages import send_or_edit, delete_interface, remove_interface
from botmanlib.models import Database, BasePermission


class PermissionsMenu(BaseMenu):
    menu_name = 'permissions_menu'

    def __init__(self, user_model, permissions_model, parent=None, bot=None, dispatcher=None):
        self.user_model = user_model
        self.permissions_model = permissions_model
        super(PermissionsMenu, self).__init__(parent, bot, dispatcher)

    @require_permission(BasePermission.view_permission)
    def entry(self, update, context):
        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        user = context.user_data['user']

        self.clear_states(context, self.user_permissions_menu, user.chat_id)
        self.send_message(context)
        return self.States.ACTION

    def send_message(self, context):
        _ = context.user_data['user'].translator

        buttons = [[InlineKeyboardButton(_("Back"), callback_data='back_from_permissions')]]

        send_or_edit(context, chat_id=context.user_data['user'].chat_id, text=_("Enter username, id or name:"), reply_markup=InlineKeyboardMarkup(buttons))

    def back(self, update, context):
        self.parent.send_message(context)
        return ConversationHandler.END

    def get_handler(self):
        self.user_permissions_menu = UserPermissionsMenu(self.user_model, self.permissions_model, parent=self)

        handler = ConversationHandler(entry_points=[CallbackQueryHandler(self.entry, pattern='^permissions$')],
                                      states={
                                          self.States.ACTION: [
                                              self.user_permissions_menu.handler,
                                              CallbackQueryHandler(self.back, pattern='^back_from_permissions$'),
                                              MessageHandler(Filters.all, to_state(self.States.ACTION))],
                                      },
                                      fallbacks=[],
                                      allow_reentry=True)

        return handler


class UserPermissionsMenu(OneListMenu):
    menu_name = 'permissions_menu'
    parse_mode = ParseMode.HTML
    allow_reentry = False

    def __init__(self, user_model, permissions_model, parent=None, bot=None, dispatcher=None):
        self.user_model = user_model
        self.permissions_model = permissions_model
        super(UserPermissionsMenu, self).__init__(parent, bot, dispatcher)

    def entry(self, update, context):
        user_data = context.user_data
        text = update.effective_message.text
        if text.isdigit():
            user_data[self.menu_name]['users'] = Database().DBSession.query(self.user_model).filter(or_(self.user_model.id == int(text), self.user_model.chat_id == int(text))).all()
        else:
            user_data[self.menu_name]['users'] = Database().DBSession.query(self.user_model).filter(or_(self.user_model.first_name.ilike(f"%{text}%"), self.user_model.last_name.ilike(f"%{text}%"), self.user_model.username == text)).all()
        delete_interface(context)
        return super(UserPermissionsMenu, self).entry(update, context)

    def query_objects(self, context):
        return context.user_data[self.menu_name]['users']

    def entry_points(self):
        return [MessageHandler(Filters.text, self.entry)]

    def message_text(self, context, obj):
        user = context.user_data['user']
        _ = user.translator

        if obj:
            message_text = _("ID: ") + str(obj.id) + '\n'
            message_text += _("Name: ") + f"<a href=\"tg://user?id={obj.chat_id}\">{obj.get_name()}</a>" + '\n'
            message_text += _("Username: ") + (obj.username if obj.username else _("Unknown")) + '\n'
            message_text += _("Permissions: ") + '\n'
            if obj.permissions:
                for permission in obj.permissions:
                    if user.language_code in permission.name:
                        lang = user.language_code
                    else:
                        lang = 'en'
                    message_text += '  - ' + permission.name[lang].replace("\\'", "'") + '\n'
            else:
                message_text += '    ' + _("Empty") + '\n'

        else:
            message_text = _("Nobody found") + '\n'

        return message_text

    def page_text(self, current_page, max_page, context):
        _ = context.user_data['user'].translator

        if len(context.user_data[self.menu_name]['users']) > 0:
            return "<i>" + str(current_page) + f' {_("user")} ' + _("out of") + ' ' + str(max_page) + ' ' + _("found") + "</i>"
        return ""

    def object_buttons(self, context, obj):
        user = context.user_data['user']
        _ = user.translator
        buttons = []
        if obj and (not obj.has_permission('superuser') or user.has_permission('superuser')):
            if user.has_permission(BasePermission.add_permission):
                buttons.append(InlineKeyboardButton(_("Add permission"), callback_data='add_permission'))
            if user.has_permission(BasePermission.delete_permission):
                buttons.append(InlineKeyboardButton(_("Remove permission"), callback_data='remove_permission'))

        if buttons:
            buttons = group_buttons(buttons, 1)
        return buttons

    def additional_states(self):
        permission_add_menu = UserPermissionsAddMenu(self.user_model, self.permissions_model, parent=self)
        permission_remove_menu = UserPermissionsRemoveMenu(self.user_model, self.permissions_model, parent=self)
        return {self.States.ACTION: [permission_add_menu.handler,
                                     permission_remove_menu.handler]}


class UserPermissionsAddMenu(BunchListMenu):
    menu_name = 'add_permission_menu'
    parse_mode = ParseMode.HTML
    search_key_param = 'name'

    def __init__(self, user_model, permissions_model, parent=None, bot=None, dispatcher=None):
        self.user_model = user_model
        self.permissions_model = permissions_model
        super(UserPermissionsAddMenu, self).__init__(parent, bot, dispatcher)

    @require_permission(BasePermission.add_permission)
    def entry(self, update: Update, context: CallbackContext):
        return super(UserPermissionsAddMenu, self).entry(update, context)

    def query_objects(self, context):
        user = self.parent.selected_object(context)
        return Database().DBSession.query(self.permissions_model).filter(~ self.permissions_model.code.in_([p.code for p in user.permissions])).filter(self.permissions_model.code != 'superuser').all()

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern=r'^add_permission$')]

    def message_text(self, context, obj):
        _ = context.user_data['user'].translator

        user = self.parent.selected_object(context)
        if obj:
            message_text = _("Select permission to add to {user}.").format(user=user.get_name()) + '\n'
        else:
            message_text = _("User {user} has all permissions").format(user=user.get_name()) + '\n'

        return message_text

    def object_buttons(self, context, objects):
        user = context.user_data['user']
        _ = user.translator
        buttons = []
        if objects:
            for obj in objects:
                if user.language_code in obj.name:
                    lang = user.language_code
                else:
                    lang = 'en'
                buttons.append([InlineKeyboardButton(obj.name[lang], callback_data=f"perm__{obj.code}")])

        return buttons

    def center_buttons(self, context, o=None):
        _ = context.user_data['user'].translator
        buttons = []
        if context.user_data['user'].has_permission('superuser'):
            buttons.append(InlineKeyboardButton(_("Add all"), callback_data=f"add_all_permissions"))
        return buttons

    def add_all_permissions(self, update, context):
        _ = context.user_data['user'].translator

        permissions = Database().DBSession.query(self.permissions_model).filter(self.permissions_model.code != 'superuser').all()
        user = self.parent.selected_object(context)
        for permission in permissions:
            if not user.has_permission(permission.code, ignore_superuser=True):
                user.permissions.append(permission)
        add_to_db(user)
        self.update_objects(context)
        self.send_message(context)
        self.bot.answer_callback_query(update.callback_query.id, text=_("All permissions added"))
        return self.States.ACTION

    def add_permission(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        code = update.callback_query.data.replace('perm__', "")

        permission = Database().DBSession.query(self.permissions_model).get(code)
        user = self.parent.selected_object(context)
        if not user.has_permission(permission.code, ignore_superuser=True):
            user.permissions.append(permission)
            add_to_db(user)

        self.update_objects(context)
        search_key = context.user_data[self.menu_name].get('search_key', None)
        if search_key:
            self.research(context)
        else:
            self.send_message(context)

        self.bot.answer_callback_query(update.callback_query.id, text=_("Permission added"))
        return self.States.ACTION

    def research(self, context):
        user = context.user_data['user']
        text = context.user_data[self.menu_name]['search_key']

        objects = self.query_objects(context)

        filtered_objects = []
        for obj in objects:
            value = getattr(obj, self.search_key_param, False)
            if value and text.lower() in "|".join(value.values()).lower():
                filtered_objects.append(obj)

        if filtered_objects:
            context.user_data[self.menu_name]['filtered_objects'] = filtered_objects
            context.user_data[self.menu_name]['page'] = 0
            self.send_message(context)
        else:
            send_or_edit(context, chat_id=user.chat_id, text=self.empty_search_text(context))
            remove_interface(context)
            self.send_message(context)

        return self.States.ACTION

    def search(self, update, context):
        user = context.user_data['user']
        text = update.message.text
        context.user_data[self.menu_name]['search_key'] = text

        objects = self.query_objects(context)

        filtered_objects = []
        for obj in objects:
            value = getattr(obj, self.search_key_param, False)
            if value and text.lower() in "|".join(value.values()).lower():
                filtered_objects.append(obj)

        if filtered_objects:
            context.user_data[self.menu_name]['filtered_objects'] = filtered_objects
            context.user_data[self.menu_name]['page'] = 0
            delete_interface(context)
            self.send_message(context)
        else:
            delete_interface(context)
            send_or_edit(context, chat_id=user.chat_id, text=self.empty_search_text(context))
            remove_interface(context)
            self.send_message(context)

        return self.States.ACTION

    def exit_search(self, update, context):
        context.user_data[self.menu_name]['search_key'] = None
        return super(UserPermissionsAddMenu, self).exit_search(update, context)

    def empty_search_text(self, context: CallbackContext):
        _ = context.user_data['user'].translator
        return _("Can't find any permission by your query")

    def additional_states(self):
        return {self.States.ACTION: [CallbackQueryHandler(self.add_permission, pattern=r"^perm__[\w_]+$"),
                                     CallbackQueryHandler(self.add_all_permissions, pattern=r"^add_all_permissions$")]}


class UserPermissionsRemoveMenu(BunchListMenu):
    menu_name = 'remove_permission_menu'
    parse_mode = ParseMode.HTML
    search_key_param = 'name'

    def __init__(self, user_model, permissions_model, parent=None, bot=None, dispatcher=None):
        self.user_model = user_model
        self.permissions_model = permissions_model
        super(UserPermissionsRemoveMenu, self).__init__(parent, bot, dispatcher)

    @require_permission(BasePermission.delete_permission)
    def entry(self, update: Update, context: CallbackContext):
        return super(UserPermissionsRemoveMenu, self).entry(update, context)

    def query_objects(self, context):
        user = self.parent.selected_object(context)
        return Database().DBSession.query(self.permissions_model).filter(self.permissions_model.code.in_([p.code for p in user.permissions])).filter(self.permissions_model.code != 'superuser').all()

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern=r'^remove_permission$')]

    def message_text(self, context, obj):
        _ = context.user_data['user'].translator

        user = self.parent.selected_object(context)
        if obj:
            message_text = _("Select permission to remove from {user}.").format(user=user.get_name()) + '\n'
        else:
            message_text = _("User {user} has no permissions").format(user=user.get_name()) + '\n'

        return message_text

    def object_buttons(self, context, objects):
        user = context.user_data['user']
        _ = user.translator
        buttons = []
        if objects:
            for obj in objects:
                if user.language_code in obj.name:
                    lang = user.language_code
                else:
                    lang = 'en'
                buttons.append([InlineKeyboardButton(obj.name[lang], callback_data=f"perm__{obj.code}")])

        return buttons

    def center_buttons(self, context, o=None):
        _ = context.user_data['user'].translator
        buttons = []
        if context.user_data['user'].has_permission('superuser'):
            buttons.append(InlineKeyboardButton(_("Remove all"), callback_data=f"remove_all_permissions"))
        return buttons

    def remove_all_permissions(self, update, context):
        _ = context.user_data['user'].translator

        permissions = Database().DBSession.query(self.permissions_model).filter(self.permissions_model.code != 'superuser').all()
        user = self.parent.selected_object(context)
        for permission in permissions:
            if user.has_permission(permission.code, ignore_superuser=True):
                user.permissions.remove(permission)
        add_to_db(user)
        self.update_objects(context)
        self.send_message(context)
        self.bot.answer_callback_query(update.callback_query.id, text=_("All permissions removed"))
        return self.States.ACTION

    def remove_permission(self, update, context):
        _ = context.user_data['user'].translator
        code = update.callback_query.data.replace('perm__', "")

        permission = Database().DBSession.query(self.permissions_model).get(code)
        user = self.parent.selected_object(context)
        if user.has_permission(permission.code, ignore_superuser=True):
            user.permissions.remove(permission)
            add_to_db(user)

        self.update_objects(context)
        search_key = context.user_data[self.menu_name].get('search_key', None)
        if search_key:
            self.research(context)
        else:
            self.send_message(context)
        self.bot.answer_callback_query(update.callback_query.id, text=_("Permission removed"))
        return self.States.ACTION

    def research(self, context):
        user = context.user_data['user']
        text = context.user_data[self.menu_name]['search_key']

        objects = self.query_objects(context)

        filtered_objects = []
        for obj in objects:
            value = getattr(obj, self.search_key_param, False)
            if value and text.lower() in "|".join(value.values()).lower():
                filtered_objects.append(obj)

        if filtered_objects:
            context.user_data[self.menu_name]['filtered_objects'] = filtered_objects
            context.user_data[self.menu_name]['page'] = 0
            self.send_message(context)
        else:
            send_or_edit(context, chat_id=user.chat_id, text=self.empty_search_text(context))
            remove_interface(context)
            self.send_message(context)

        return self.States.ACTION

    def search(self, update, context):
        user = context.user_data['user']
        text = update.message.text
        context.user_data[self.menu_name]['search_key'] = text

        objects = self.query_objects(context)

        filtered_objects = []
        for obj in objects:
            value = getattr(obj, self.search_key_param, False)
            if value and text.lower() in "|".join(value.values()).lower():
                filtered_objects.append(obj)

        if filtered_objects:
            context.user_data[self.menu_name]['filtered_objects'] = filtered_objects
            context.user_data[self.menu_name]['page'] = 0
            delete_interface(context)
            self.send_message(context)
        else:
            delete_interface(context)
            send_or_edit(context, chat_id=user.chat_id, text=self.empty_search_text(context))
            remove_interface(context)
            self.send_message(context)

        return self.States.ACTION

    def exit_search(self, update, context):
        context.user_data[self.menu_name]['search_key'] = None
        return super(UserPermissionsRemoveMenu, self).exit_search(update, context)

    def empty_search_text(self, context: CallbackContext):
        _ = context.user_data['user'].translator
        return _("Can't find any permission by your query")

    def additional_states(self):
        return {self.States.ACTION: [CallbackQueryHandler(self.remove_permission, pattern=r"^perm__[\w_]+$"),
                                     CallbackQueryHandler(self.remove_all_permissions, pattern=r"^remove_all_permissions$")]}
