import csv
import datetime
import enum
from time import sleep

import requests
from io import StringIO
from formencode import Invalid
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.error import TimedOut, BadRequest
from telegram.ext import ConversationHandler, CallbackQueryHandler, MessageHandler, Filters

from botmanlib.menus import BaseMenu
from botmanlib.messages import delete_interface, send_or_edit, delete_user_message


class CSVLoaderMenu(BaseMenu):
    class States(enum.Enum):
        ACTION = 1
        END = 2

    class ColumnError:
        def __init__(self, line, param, message, lang=None):
            self.line = line
            self.param = param
            self.message = message
            self.lang = lang

    class ColumnWarning:
        def __init__(self, line, param, message, lang=None):
            self.line = line
            self.param = param
            self.message = message
            self.lang = lang

    class Column:
        def __init__(self, param, names, validator, required=False, multilang=False, is_image=False, after_validate=None):
            self.param = param
            self.names = names
            self.validator = validator
            self.required = required
            self.multilang = multilang
            self.after_validate = after_validate
            self.is_image = is_image

    menu_name = 'csv_loader_menu'
    model = None
    translation_model = None
    translation_model_foreign_key = "parent_id"
    session = None
    image_download_max_tries = 3
    image_error_sleep_time = 3
    encoding = "utf-8"

    languages = {"en": ["EN", "ENG", "АНГ"],
                 "ru": ["RU", "RUS", "РУС"],
                 "uk": ["UK", "UKR", "УКР", "UA"]}

    def columns(self):
        raise NotImplementedError

    def load_csv_file(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        delete_interface(context)
        message_text_template = _("Please wait, processing row №{line}...")
        send_or_edit(context, chat_id=user.chat_id, text=message_text_template.format(line=1))

        file = StringIO(update.message.document.get_file().download_as_bytearray().decode(self.encoding))
        dr = csv.DictReader(file)
        file_data = []
        file_errors = []
        file_warnings = []
        last_update = datetime.datetime.utcnow()

        for line, row in enumerate(dr):
            row_errors = []
            row_warnings = []
            line += 1

            # parse row, get raw data
            errors, warnings, row_data = self.parse_row(context, line, row)
            row_errors.extend(errors)
            row_warnings.extend(warnings)

            # validate raw data
            errors, warnings = self.validate_data(context, line, row_data)
            row_errors.extend(errors)
            row_warnings.extend(warnings)

            # check image links
            errors, warnings = self.check_images(context, line, row_data)
            row_errors.extend(errors)
            row_warnings.extend(warnings)

            # convert file_links to file_id's
            errors, warnings = self.convert_images(context, line, row_data)
            row_errors.extend(errors)
            row_warnings.extend(warnings)

            # save errors and warnings
            file_warnings.extend(row_warnings)
            if row_errors:
                file_errors.extend(row_errors)
            else:
                file_data.append(row_data)

            # update user message
            now = datetime.datetime.utcnow()
            if last_update + datetime.timedelta(seconds=1) < now:
                last_update = now
                send_or_edit(context, chat_id=user.chat_id, text=message_text_template.format(line=line+1))

        # send message with import result
        if not file_errors:
            errors, warnings = self.create_objects(context, file_data)
            file_errors.extend(errors)
            file_warnings.extend(warnings)

        self.send_errors_and_warnings_message(context, file_errors, file_warnings)

        return self.States.ACTION

    def get_column_by_name(self, name):
        for column in self.columns():
            if name.lower() in [col_name.lower() for col_name in column.names]:
                return column

        return None

    def get_column_by_param(self, param):
        for column in self.columns():
            if column.param == param:
                return column

        return None

    def entry(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        if not user.has_permission(self.model.import_permission):
            self.bot.send_message(chat_id=user.chat_id, text=_("You were restricted to use this menu"))
            return self.States.ACTION

        self.send_message(context)
        return self.States.ACTION

    def back_button(self, context):
        _ = context.user_data['user'].translator
        return InlineKeyboardButton(_("Back"), callback_data='back_from_csv')

    def send_message(self, context):
        user = context.user_data['user']
        _ = user.translator
        message_text = _("Please send me CSV file.")

        buttons = []
        back_button = self.back_button(context)
        if back_button:
            buttons.append([back_button])

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons) if buttons else None)

    def back(self, update, context):
        update_objects = getattr(self.parent, 'update_objects', None)
        if update_objects:
            update_objects(context)
        self.parent.send_message(context)
        return ConversationHandler.END

    def parse_multilang_column(self, key, data):
        for lang in self.languages:
            for lang_variant in self.languages[lang]:
                if lang_variant in key:
                    return lang, data
        return None, data

    def parse_row(self, context, line, row):
        warnings = []
        errors = []
        _ = context.user_data['user'].translator

        row_data = {column_info.param: {} for column_info in self.columns()}
        # parse row data
        for key, value in row.items():
            lower_key = key.lower()

            column = self.get_column_by_name(lower_key)
            if column is None:
                warnings.append(self.ColumnWarning(line, key, _("Unknown column, skipping...")))
                continue

            if column.multilang:
                lang, data = self.parse_multilang_column(key, row[key])
                if lang is None:
                    continue
                row_data[column.param][lang] = data
            else:
                row_data[column.param] = value

        return errors, warnings, row_data

    def validate_data(self, context, line, row_data):
        errors = []
        warnings = []
        _ = context.user_data['user'].translator

        for column_param, column_data in row_data.items():
            column = self.get_column_by_param(column_param)
            try:
                if column.multilang:
                    # validate multilang column
                    for lang in column_data:
                        validated_value = column.validator.to_python(column_data[lang])
                        if column.after_validate:
                            # apply after validate function
                            col_errors, col_warnings, validated_value = column.after_validate(column, line, validated_value)
                            errors.extend(col_errors)
                            warnings.extend(col_warnings)

                        row_data[column.param][lang] = validated_value
                        if column.required and validated_value is None:
                            errors.append(self.ColumnError(line, column.param, _("Parameter is required but missing"), lang=lang.upper()))

                else:
                    # validate regular column
                    validated_value = column.validator.to_python(column_data)
                    if column.after_validate:
                        # apply after validate function
                        col_errors, col_warnings, validated_value = column.after_validate(column, line, validated_value)
                        errors.extend(col_errors)
                        warnings.extend(col_warnings)

                    row_data[column.param] = validated_value
                    if column.required and validated_value is None:
                        errors.append(self.ColumnError(line, column.param, _("Parameter is required but missing")))
            except Invalid as e:
                if column.required:
                    errors.append(self.ColumnError(line, column_param, e.msg))
                else:
                    warnings.append(self.ColumnWarning(line, column_param, e.msg))

        return errors, warnings

    def check_images(self, context, line, row_data):
        warnings = []
        errors = []
        _ = context.user_data['user'].translator

        for column_param, column_data in row_data.items():
            column = self.get_column_by_param(column_param)
            if column.is_image:
                for lang, link in column_data.items() if column.multilang else [(None, column_data)]:
                    if link is None:
                        continue

                    # try to access image link
                    res = requests.get(link)
                    if res.status_code != 200:
                        if column.required:
                            errors.append(self.ColumnError(line, column.param, _("Image link returned {status_code} code").format(status_code=res.status_code), lang=lang.upper()))
                        else:
                            warnings.append(self.ColumnWarning(line, column.param, _("Image link returned {status_code} code").format(status_code=res.status_code), lang=lang.upper()))
                        if lang:
                            row_data[column.param][lang] = None
                        else:
                            row_data[column.param] = None

        return errors, warnings

    def convert_images(self, context, line, row_data):
        warnings = []
        errors = []
        _ = context.user_data['user'].translator

        for column_param, column_data in row_data.items():
            column = self.get_column_by_param(column_param)
            if column.is_image:
                for lang, link in (column_data.items() if column.multilang else [(None, column_data)]):
                    if link is not None:
                        image_id = self.get_image_id(context, link)
                    else:
                        image_id = None

                    if image_id is None and column.required:
                        errors.append(self.ColumnError(line, column.param, _("Telegram error occurred while processing image link"), lang=lang.upper()))

                    if lang:
                        row_data[column_param][lang] = image_id
                    else:
                        row_data[column_param] = image_id

        return errors, warnings

    def create_objects(self, context, file_data):
        warnings = []
        errors = []
        _ = context.user_data['user'].translator

        with self.session.no_autoflush:
            for row_data in file_data:
                obj = self.query_object(row_data)
                if obj is None:
                    obj = self.model()

                row_errors = []
                row_warnings = []
                for column_param, column_data in row_data.items():
                    column = self.get_column_by_param(column_param)
                    if column.multilang:
                        # process multilang data
                        for lang, data in column_data.items():
                            if lang is None or data is None:
                                continue
                            is_new = False
                            translation = self.query_translation(obj, lang)
                            if translation is None:
                                for t in obj.translations:
                                    if t.lang == lang:
                                        translation = t
                                        break

                            if translation is None:
                                translation = self.translation_model(lang=lang)
                                is_new = True

                            if hasattr(translation, column.param):
                                setattr(translation, column.param, data)
                            else:
                                row_warnings.append(self.ColumnWarning(obj.id, column.param, _("Translation has no such attribute as \"{param}\"").format(param=column_param), lang=lang.upper()))
                                continue
                            if is_new:
                                obj.translations.append(translation)
                    else:
                        # processing single language data
                        if hasattr(obj, column.param):
                            setattr(obj, column.param, column_data)
                        else:
                            row_warnings.append(self.ColumnWarning(obj.id, column.param, _("{model} has no such attribute as \"{param}\"").format(model=self.model.__name__, param=column_param)))
                            continue

                errors.extend(row_errors)
                warnings.extend(row_warnings)
                if row_errors:
                    continue

                self.session.add(obj)
            self.session.commit()
        return errors, warnings

    def query_object(self, row_data):
        raise NotImplementedError

    def query_translation(self, obj, lang):
        raise NotImplementedError

    def get_image_id(self, context, image_link):
        user = context.user_data['user']
        send_tries = 0
        delete_tries = 0
        image_id = None
        mes = None

        while image_id is None and send_tries <= self.image_download_max_tries:
            try:
                mes = self.bot.send_photo(chat_id=user.chat_id, photo=image_link, disable_notification=True)
                image_id = mes.photo[-1].file_id
            except (TimedOut, BadRequest):
                sleep(self.image_error_sleep_time)

            send_tries += 1

        while image_id is not None and mes is not None and delete_tries <= self.image_download_max_tries:
            try:
                mes.delete()
                mes = None
            except (TimedOut, BadRequest):
                sleep(self.image_error_sleep_time)

        return image_id

    def send_errors_and_warnings_message(self, context, errors, warnings):
        user = context.user_data['user']
        _ = user.translator

        buttons = []
        back_button = self.back_button(context)
        if back_button:
            buttons.append([back_button])

        if not warnings and not errors:
            send_or_edit(context, chat_id=user.chat_id, text=_("Data imported successfully"), reply_markup=InlineKeyboardMarkup(buttons) if buttons else None)
        else:
            warnings_text = _("⚠{warnings} warnings occurred while importing file:").format(warnings=len(warnings)) + '\n'
            for idx, warning in enumerate(warnings):
                warnings_text += f'{idx + 1}. {_("Row")} №{warning.line}, '
                warnings_text += f'{_("Column")} {warning.param if warning.param else "??"}{" " + warning.lang if warning.lang else ""}: '
                warnings_text += warning.message + '\n'

            if not errors:
                warnings_text += '\n' + _("Data imported successfully.")
                send_or_edit(context, chat_id=user.chat_id, text=warnings_text, reply_markup=InlineKeyboardMarkup(buttons) if buttons else None)
            else:
                send_or_edit(context, chat_id=user.chat_id, text=warnings_text)

                errors_text = _("❌{errors} errors occurred while importing file:").format(errors=len(errors)) + '\n'
                for idx, error in enumerate(errors):
                    errors_text += f'{idx + 1}. {_("Row")} №{error.line}, '
                    errors_text += f'{_("Column")} {error.param if error.param else "??"}{" " + error.lang if error.lang else ""}: '
                    errors_text += error.message + '\n'

                errors_text += '\n' + _("File not imported")

                send_or_edit(context, chat_id=user.chat_id, text=errors_text, reply_markup=InlineKeyboardMarkup(buttons) if buttons else None)

    def entry_points(self):
        raise NotImplementedError

    def additional_states(self):
        return {}

    def fallbacks(self):
        return [MessageHandler(Filters.all, lambda update, context: delete_user_message(update))]

    def get_handler(self):
        states = {self.States.ACTION: [CallbackQueryHandler(self.back, pattern="^back_from_csv$"),
                                       MessageHandler(Filters.document.mime_type('text/csv'), self.load_csv_file),
                                       ],
                  self.States.END: [CallbackQueryHandler(self.back, pattern="^back_from_csv$")]
                  }

        additional_states = self.additional_states()
        for key in additional_states:
            if key in states:
                states[key] = additional_states[key] + states[key]
            else:
                states.update({key: additional_states[key]})

        handler = ConversationHandler(entry_points=self.entry_points(),
                                      states=states,
                                      fallbacks=self.fallbacks(),
                                      allow_reentry=True)

        return handler
