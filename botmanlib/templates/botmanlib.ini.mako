[botmanlib]
sqlalchemy.url = driver://user:pass@localhost/dbname
sqlalchemy.pool_size = 5
sqlalchemy.max_overflow = 10

bot.entry_point = python_package.subpackage.python_file:python_function_name
bot.token = INSERT BOT TOKEN HERE
bot.name = INSERT BOT NAME HERE
bot.secret = ${secret}
bot.save_messages = True
bot.autostart = True

;payments.api_url = https://botman.pp.ua/api/
;payments.debug = True
;payments.base_token = main_token

;you can define your own external scripts like this:
;script.run_command = python_package.subpackage.python_file:python_function_name
;to run script you can use "botmanlib run_commnd".

;api.active = False
;api.host = 0.0.0.0
;api.port = 47610
;api.debug = False
;api.use_reloader = False
;api.bot_entry_point = src.app:main
;api.documentation = True
;api.token = ${token}
;api.cert = /path/to/bot.crt
;api.cert_key = /path/to/bot.key
