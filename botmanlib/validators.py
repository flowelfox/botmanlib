import datetime

import formencode
from formencode import validators, Invalid
from formencode.validators import TimeConverter, DateConverter


class TelegramUsername(validators.FancyValidator):
    usernames = []
    messages = {'not_exists': 'Password not exists in \'usernames\''}

    def _to_python(self, value, state):
        return value.strip("@ ")

    def validate_python(self, value, state):
        if value not in [u.strip("@ ") for u in self.usernames]:
            raise Invalid(self.message("not_exists", state), value, state)


class PhoneNumber(formencode.national.InternationalPhoneNumber):
    messages = dict(phoneFormat='Please enter a number, with area code, in the form +##-###-#######.')

    def _convert_to_python(self, value, state):
        if value.startswith('+') and 14 >= len(value) >= 12 and value[1:].isdigit():
            value = '+' + value[1:3] + '-' + value[3:6] + '-' + value[6:]
        elif not value.startswith('+') and len(value) <= 12 and value.isdigit():
            value = '+' + value[:2] + '-' + value[2:5] + '-' + value[5:]

        value = super(PhoneNumber, self)._convert_to_python(value, state)
        return value


class DateTimeConverter(validators.FancyValidator):
    use_seconds = False
    use_datetime = True
    month_style = 'dmy'

    messages = {'invalid_format': 'Please enter time and date'}

    def _convert_to_python(self, value, state):
        values = value.split(' ')
        if len(values) != 2:
            raise Invalid(self.message("invalid_format", state), value, state)
        else:
            time_string, date_string = values
            time_validator = TimeConverter(use_seconds=self.use_seconds, use_datetime=self.use_datetime)
            time = time_validator.to_python(time_string)

            date_validator = DateConverter(month_style=self.month_style)
            date = date_validator.to_python(date_string)

            dt = datetime.datetime.combine(date, time)

            return dt
