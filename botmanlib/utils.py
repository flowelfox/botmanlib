import ast
import datetime
import os
from distutils.util import strtobool

from sqlalchemy import orm


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


def json_converter(o):
    if isinstance(o, datetime.datetime) or isinstance(o, datetime.date):
        return o.isoformat()
    else:
        return o


def asdict(obj):
    return {col.name: getattr(obj, col.name)
            for col in orm.class_mapper(obj.__class__).mapped_table.c}


def parse_environ_settings(vars):
    for key in vars:
        if key in os.environ:
            if os.environ[key] == "None":
                vars[key] = None
                continue

            if os.environ[key].startswith('"') and os.environ[key].endswith('"'):
                vars[key] = os.environ[key].replace('"', "")
                continue

            if os.environ[key].startswith('[') and os.environ[key].endswith(']'):
                vars[key] = ast.literal_eval(os.environ[key])
                continue

            if '.' in os.environ[key]:
                try:
                    vars[key] = float(os.environ[key])
                    continue
                except ValueError:
                    pass

            try:
                vars[key] = int(os.environ[key])
                continue
            except ValueError:
                pass

            try:
                vars[key] = bool(strtobool(os.environ[key]))
                continue
            except ValueError:
                pass

                vars[key] = os.environ[key]


class BotmanlibError(Exception):
    def __init__(self, *args, **kwargs):
        Exception.__init__(self, args, kwargs)


class UpdateObjectError(BotmanlibError):
    def __init__(self, message, show_alert=True):
        BotmanlibError.__init__(self, message)
        self.message = message
        self.show_alert = show_alert
