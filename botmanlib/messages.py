import html
import logging
import os
from distutils.util import strtobool

from telegram import ReplyKeyboardMarkup, InlineKeyboardMarkup, InputMediaPhoto, InputMediaDocument, TelegramError, InputFile, InputMediaVideo, InputMediaAnimation
from telegram.utils.promise import Promise

from .models import Message as ChatMessage, Sender

logger = logging.getLogger("sender")


class Interface:
    """
    This class represents telegram message interface.
    Usually user must interact only with one last telegram message.
    This class store message, keyboard and other useful info about telegram message


    """

    def __init__(self, name, message=None):
        self.name = name
        self._message = message
        self.reply_markup = None
        self.parse_mode = None
        self.disable_web_page_preview = None
        self.disable_notification = None

    @property
    def message(self):
        return self._message

    @message.setter
    def message(self, value):
        self._message = value

    def __getattr__(self, attr):
        try:
            return super(Interface, self).__getattr__(attr)
        except AttributeError:
            return getattr(self.message, attr) if self._message else None

    def extend(self, data):
        self.reply_markup = data.get('reply_markup', None)
        self.parse_mode = data.get('parse_mode', None)
        self.disable_web_page_preview = data.get('disable_web_page_preview', None)
        self.disable_notification = data.get('disable_notification', None)

    @property
    def reply_markup_type(self):
        if self.reply_markup:
            return self.reply_markup.__class__

    def save(self, user_data):
        """Save interface to user_data

        Args:
            user_data (:str:) User data from context where interface would be saved
        """
        user_data['interfaces'][self.name] = self
        if strtobool(os.getenv('bot.save_messages', "False")):
            if 'user' in user_data:
                lang = user_data['user'].language_code
            else:
                lang = None
            tmessage = ChatMessage.create(self.message, Sender.bot, lang, reply_markup=self.reply_markup)
            tmessage.save()


def _send_telegram_text(bot, *args, **kwargs):
    mes = bot.send_message(*args, **kwargs)
    if isinstance(mes, Promise):
        mes.done.wait()
        mes = mes.result()
    return mes


def _send_telegram_photo(bot, *args, **kwargs):
    mes = bot.send_photo(*args, **kwargs)
    if isinstance(mes, Promise):
        mes.done.wait()
        mes = mes.result()
    return mes


def _send_telegram_animation(bot, *args, **kwargs):
    mes = bot.send_animation(*args, **kwargs)
    if isinstance(mes, Promise):
        mes.done.wait()
        mes = mes.result()
    return mes


def _send_telegram_video(bot, *args, **kwargs):
    mes = bot.send_video(*args, **kwargs)
    if isinstance(mes, Promise):
        mes.done.wait()
        mes = mes.result()
    return mes


def _send_telegram_document(bot, *args, **kwargs):
    mes = bot.send_document(*args, **kwargs)
    if isinstance(mes, Promise):
        mes.done.wait()
        mes = mes.result()
    return mes


def _send_telegram_location(bot, *args, **kwargs):
    mes = bot.send_location(*args, **kwargs)
    if isinstance(mes, Promise):
        mes.done.wait()
        mes = mes.result()
    return mes


def _send_telegram_sticker(bot, *args, **kwargs):
    mes = bot.send_sticker(*args, **kwargs)
    if isinstance(mes, Promise):
        mes.done.wait()
        mes = mes.result()
    return mes


def _send_telegram_message(context, **kwargs):
    if 'text' in kwargs:
        return _send_telegram_text(context.bot, **kwargs)
    elif 'photo' in kwargs:
        if InputFile.is_file(kwargs['photo']):
            kwargs['photo'].seek(0)
        return _send_telegram_photo(context.bot, **kwargs)
    elif 'animation' in kwargs:
        return _send_telegram_animation(context.bot, **kwargs)
    elif 'video' in kwargs:
        return _send_telegram_video(context.bot, **kwargs)
    elif 'document' in kwargs:
        return _send_telegram_document(context.bot, **kwargs)
    elif 'location' in kwargs:
        return _send_telegram_location(context.bot, **kwargs)
    elif 'sticker' in kwargs:
        return _send_telegram_sticker(context.bot, **kwargs)


def _init_interfaces(user_data):
    if 'interfaces' not in user_data:
        user_data['interfaces'] = {}


def get_user_data(dispatcher, chat_id):
    key = chat_id
    user_data = dispatcher.user_data.get(key, None)
    if user_data is None:
        dispatcher.user_data[key] = {}
        user_data = dispatcher.user_data[key]

    return user_data


def send_or_edit(context, interface_name='interface', user_id=None, dispatcher=None, **kwargs):
    """Use this function to send any telegram text,document,photo or location messages

    Args:
        context (:class:`~telegram.ext.CallbackContext`): Context from callback.
        interface_name (:obj:`str`, optional): Interface name which can be lately used to update interface with that name.
        user_id (:obj:`str`, optional): User chat_id, if you need to send telegram message to another user.
        dispatcher (:class:`telegram.ext.Dispatcher`, optional): Dispatcher, needed to send message to another user.
        kwargs (:obj:`dict`, optional): Any keyword arguments which will be passed to bot.send_message method.
    """
    if user_id and dispatcher is None:
        logger.error("You must pass dispatcher with user_id")
        return
    if user_id and dispatcher:
        user_data = get_user_data(dispatcher, user_id)
    else:
        user_data = context.user_data

    if user_data is None:
        logger.error("Can't send message to user because user_data not found")
        return
    _init_interfaces(user_data)

    interface = user_data['interfaces'].get(interface_name, Interface(interface_name))

    if len(kwargs.get('text', '')) > 4090:
        chunk_size = 4090
        chunks = len(kwargs['text'])
        message_list = [kwargs['text'][i:i + chunk_size] for i in range(0, chunks, chunk_size)]

        if 'reply_markup' in kwargs:
            reply_markup = kwargs['reply_markup']
            kwargs['reply_markup'] = None
        else:
            reply_markup = None

        for idx, message in enumerate(message_list):
            kwargs['text'] = message
            if idx == len(message_list) - 1:
                kwargs['reply_markup'] = reply_markup

            interface.message = _send_telegram_message(context, **kwargs)
            interface.extend(kwargs)
            interface.save(user_data)

        return interface

    if interface.message:
        new_reply_markup = kwargs.get('reply_markup', None)
        new_message_reply_markup_type = new_reply_markup.__class__ if new_reply_markup else None

        if interface.reply_markup_type is not None \
                and new_message_reply_markup_type is not None \
                and new_message_reply_markup_type is not interface.reply_markup_type:  # if reply_markup differs
            remove_interface_markup(context, interface_name)
            interface.message = _send_telegram_message(context, **kwargs)
        elif new_message_reply_markup_type is ReplyKeyboardMarkup:  # if reply_markup is ReplyKeyboardMarkup always send new message
            """Please note, that it is currently only possible to edit messages without reply_markup or with inline keyboards."""
            interface.message = _send_telegram_message(context, **kwargs)
        else:
            text_same = False
            markup_same = True

            new_text = kwargs.get('text', '').strip(" \n\t")
            if interface.text_html and html.unescape(interface.text_html) == new_text:
                text_same = True
            if interface.text_markdown_v2 and html.unescape(interface.text_markdown_v2) == new_text:
                text_same = True

            if new_message_reply_markup_type is InlineKeyboardMarkup:  # is markup in new message
                new_keyboard = new_reply_markup.inline_keyboard
                if interface.reply_markup:  # is markup in previous message
                    try:
                        for y, row in enumerate(interface.reply_markup.inline_keyboard):
                            for x, button in enumerate(row):
                                if new_keyboard[y][x].text != button.text or \
                                        (hasattr(new_keyboard[y][x], 'callback_data') and hasattr(button, 'callback_data') and new_keyboard[y][x].callback_data != button.callback_data) or \
                                        (hasattr(new_keyboard[y][x], 'url') and hasattr(button, 'url') and new_keyboard[y][x].url != button.url):
                                    raise IndexError
                    except IndexError:
                        markup_same = False
                else:
                    markup_same = False
            elif new_message_reply_markup_type is ReplyKeyboardMarkup:  # is markup in new message
                new_keyboard = new_reply_markup.keyboard
                if interface.reply_markup:  # is markup in previous message
                    try:
                        for y, row in enumerate(interface.reply_markup.keyboard):
                            for x, button in enumerate(row):
                                if new_keyboard[y][x] != button:
                                    raise IndexError
                    except IndexError:
                        markup_same = False
                else:
                    markup_same = False

            if markup_same and text_same:
                interface.save(user_data)
                return interface
            elif text_same and not markup_same:
                interface.message = interface.message.edit_reply_markup(reply_markup=new_reply_markup)
                interface.reply_markup = new_reply_markup
                interface.save(user_data)
                return interface

            _chat_id = None  # saving chat id before edit
            try:
                if 'text' in kwargs and interface.text:
                    _chat_id = kwargs.pop('chat_id')
                    interface.message = interface.message.edit_text(**kwargs)
                elif 'photo' in kwargs and interface.photo:
                    _chat_id = kwargs.pop('chat_id')
                    media = InputMediaPhoto(kwargs.get('photo', None), kwargs.get('caption', None), kwargs.get('parse_mode', None))
                    interface.message = interface.message.edit_media(media=media, **kwargs)
                elif 'document' in kwargs and interface.document:
                    _chat_id = kwargs.pop('chat_id')
                    media = InputMediaDocument(kwargs.get('document', None), kwargs.get('caption', None), kwargs.get('parse_mode', None))
                    interface.message = interface.message.edit_media(media=media, **kwargs)
                elif 'video' in kwargs and interface.video:
                    _chat_id = kwargs.pop('chat_id')
                    media = InputMediaVideo(kwargs.get('video', None), kwargs.get('caption', None), kwargs.get('parse_mode', None))
                    interface.message = interface.message.edit_media(media=media, **kwargs)
                elif 'animation' in kwargs and interface.animation:
                    _chat_id = kwargs.pop('chat_id')
                    media = InputMediaAnimation(kwargs.get('animation', None), kwargs.get('caption', None), kwargs.get('parse_mode', None))
                    interface.message = interface.message.edit_media(media=media, **kwargs)

                # if message type changed
                elif ('text' in kwargs and not interface.text) or \
                        ('photo' in kwargs and not interface.photo) or \
                        ('document' in kwargs and not interface.document) or \
                        ('video' in kwargs and not interface.video) or \
                        ('animation' in kwargs and not interface.animation) or \
                        ('sticker' in kwargs) or \
                        ('location' in kwargs):
                    if interface.message:
                        interface.message.delete()
                    interface.message = _send_telegram_message(context, **kwargs)
                else:
                    interface.message = _send_telegram_message(context, **kwargs)

            except (TelegramError, AttributeError) as e:
                logger.warning(f"Can't edit message: {e}")
                if _chat_id:  # recovering chat_id if it's exists
                    kwargs['chat_id'] = _chat_id
                interface.message = _send_telegram_message(context, **kwargs)

    else:
        delete_interface(context, interface_name=interface_name)
        interface.message = _send_telegram_message(context, **kwargs)

    interface.extend(kwargs)
    interface.save(user_data)
    return interface


def remove_interface(context, interface_name='interface', user_id=None, dispatcher=None):
    """Use this function to remove interface from user_data

    Args:
        context (:class:`~telegram.ext.CallbackContext`): Context from callback.
        interface_name (:obj:`str`, optional): Interface name which can be lately used to update interface with that name.
        user_id (:obj:`str`, optional): User chat_id, if you need to send telegram message to another user.
        dispatcher (:class:`telegram.ext.Dispatcher`, optional): Dispatcher, needed to send message to another user.
    """
    if user_id and dispatcher is None:
        logger.error("You must pass dispatcher with user_id")
        return
    if user_id and dispatcher:
        user_data = get_user_data(dispatcher, user_id)
    else:
        user_data = context.user_data

    if user_data is None:
        logger.error("Can't send message to user because user_data not found")
        return

    _init_interfaces(user_data)

    if interface_name in user_data['interfaces']:
        del user_data['interfaces'][interface_name]


def delete_interface(context, interface_name='interface', user_id=None, dispatcher=None):
    """Use this function to delete telegram message from user and remove interface from user_data

    Args:
        context (:class:`~telegram.ext.CallbackContext`): Context from callback.
        interface_name (:obj:`str`, optional): Interface name which can be lately used to update interface with that name.
        user_id (:obj:`str`, optional): User chat_id, if you need to send telegram message to another user.
        dispatcher (:class:`telegram.ext.Dispatcher`, optional): Dispatcher, needed to send message to another user.
    """
    if user_id and dispatcher is None:
        logger.error("You must pass dispatcher with user_id")
        return
    if user_id and dispatcher:
        user_data = get_user_data(dispatcher, user_id)
    else:
        user_data = context.user_data

    if user_data is None:
        logger.error("Can't send message to user because user_data not found")
        return

    _init_interfaces(user_data)

    interface = user_data['interfaces'].get(interface_name, None)
    if interface and interface.message:
        try:
            interface.message.delete()
        except (TelegramError, AttributeError) as e:
            logger.warning(f"Can't delete interface message: {e}")

    remove_interface(context, interface_name, user_id, dispatcher)


def remove_interface_markup(context, interface_name='interface', user_id=None, dispatcher=None):
    """Use this function to remove telegram message reply markup from telegram message

    Args:
        context (:class:`~telegram.ext.CallbackContext`): Context from callback.
        interface_name (:obj:`str`, optional): Interface name which can be lately used to update interface with that name.
        user_id (:obj:`str`, optional): User chat_id, if you need to send telegram message to another user.
        dispatcher (:class:`telegram.ext.Dispatcher`, optional): Dispatcher, needed to send message to another user.
    """
    if user_id and dispatcher is None:
        logger.error("You must pass dispatcher with user_id")
        return
    if user_id and dispatcher:
        user_data = get_user_data(dispatcher, user_id)
    else:
        user_data = context.user_data

    if user_data is None:
        logger.error("Can't send message to user because user_data not found")
        return

    _init_interfaces(user_data)

    interface = user_data['interfaces'].get(interface_name, None)

    if interface:
        try:
            if interface.reply_markup and interface.reply_markup_type is ReplyKeyboardMarkup:
                delete_interface(context, interface_name, user_id, dispatcher)
            else:
                interface.message.edit_reply_markup()
        except (TelegramError, AttributeError) as e:
            logger.warning(f"Can't remove keyboard markup in interface message: {e}")

    remove_interface(context, interface_name, user_id, dispatcher)


def resend_interface(context, interface_name='interface', user_id=None, dispatcher=None):
    """Use this function to delete previous interface message and send same message agai

    Args:
        context (:class:`~telegram.ext.CallbackContext`): Context from callback.
        interface_name (:obj:`str`, optional): Interface name which can be lately used to update interface with that name.
        user_id (:obj:`str`, optional): User chat_id, if you need to send telegram message to another user.
        dispatcher (:class:`telegram.ext.Dispatcher`, optional): Dispatcher, needed to send message to another user.
    """
    if user_id and dispatcher is None:
        logger.error("You must pass dispatcher with user_id")
        return
    if user_id and dispatcher:
        user_data = get_user_data(dispatcher, user_id)
    else:
        user_data = context.user_data

    if user_data is None:
        logger.error("Can't send message to user because user_data not found")
        return

    _init_interfaces(user_data)

    interface = user_data['interfaces'].get(interface_name, None)

    if interface is None:
        logger.debug("Can't resend user interface because it's not exist.")
        return
    delete_interface(context, interface_name, user_id, dispatcher)

    send_or_edit(context,
                 interface_name,
                 user_id,
                 dispatcher,
                 chat_id=interface.chat_id,
                 text=interface.text,
                 reply_markup=interface.reply_markup,
                 disable_web_page_preview=interface.disable_web_page_preview,
                 parse_mode=interface.parse_mode,
                 disable_notification=interface.disable_notification)


def get_interface(context, name):
    if 'interfaces' in context.user_data and name in context.user_data['interfaces']:
        return context.user_data['interfaces'][name]


def delete_user_message(update):
    """Use this function to delete message sent by user in update

    Args:
        update(:py:class:`~telegram.Update`): Context from callback.
    """
    if update.message and update.message.message_id:
        update.message.delete()
