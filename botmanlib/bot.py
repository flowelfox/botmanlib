import logging

from telegram import Bot
from telegram.error import Unauthorized
from telegram.ext import messagequeue

from .models import Database, BaseUser

logger = logging.getLogger(__name__)


class BotmanBot(Bot):
    def __init__(self, *args, is_queued_def=True, mqueue=None, **kwargs):
        super(BotmanBot, self).__init__(*args, **kwargs)
        # below 2 attributes should be provided for decorator usage
        self._is_messages_queued_default = is_queued_def
        self._msg_queue = mqueue or messagequeue.MessageQueue()

    def stop(self):
        try:
            self._msg_queue.stop()
        except Exception as e:
            self.logger.error(str(e))

    @messagequeue.queuedmessage
    def send_message(self, *args, **kwargs):
        try:
            message = super(BotmanBot, self).send_message(*args, **kwargs)
        except Unauthorized:
            # try to deactivate user
            database = Database()
            user_model = BaseUser.get_mapped_model()
            if user_model is not None:
                chat_id = kwargs.get('chat_id', None)
                if not chat_id:
                    chat_id = args[0]
                user = database.DBSession.query(user_model).filter_by(chat_id=chat_id).first()
                if user:
                    user.deactivate()
                    Database().DBSession.add(user)
                    Database().DBSession.commit()

            self.logger.debug("Can't send message to user. Unauthorized.")
            return None

        return message

    @messagequeue.queuedmessage
    def send_photo(self, *args, **kwargs):
        '''Wrapped method would accept new `queued` and `isgroup`
        OPTIONAL arguments'''
        return super(BotmanBot, self).send_photo(*args, **kwargs)