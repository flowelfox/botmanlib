import configparser
import datetime
import hashlib
import importlib
import json
import logging
import os
import random
import ssl
import traceback
from argparse import ArgumentParser
from distutils.util import strtobool

import requests
from mako.template import Template

from botmanlib import __version__
from botmanlib.api.helpers import Bot
from . import package_dir

logger = logging.getLogger(__name__)


class ReverseProxied(object):
    '''Wrap the application in this middleware and configure the
    front-end server to add these headers, to let you quietly bind
    this to a URL other than / and to an HTTP scheme that is
    different than what is used locally.

    In nginx:
    location /myprefix {
        proxy_pass http://192.168.0.1:5001;
        proxy_set_header Host $host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Scheme $scheme;
        proxy_set_header X-Script-Name /myprefix;
        }

    :param app: the WSGI application
    '''

    def __init__(self, app):
        self.app = app

    def __call__(self, environ, start_response):
        script_name = environ.get('HTTP_X_SCRIPT_NAME', '')
        if script_name:
            environ['SCRIPT_NAME'] = script_name
            path_info = environ['PATH_INFO']
            if path_info.startswith(script_name):
                environ['PATH_INFO'] = path_info[len(script_name):]

        scheme = environ.get('HTTP_X_SCHEME', '')
        if scheme:
            environ['wsgi.url_scheme'] = scheme
        return self.app(environ, start_response)


def main():
    config_parser = ArgumentParser(add_help=False)
    config_parser.add_argument("-c", "--config",
                               dest='path',
                               type=str,
                               default="botmanlib.ini",
                               help="Alternate config file")

    config_parser.add_argument("-s", "--section",
                               dest="section",
                               type=str,
                               default="botmanlib",
                               help="Name of section in .ini file to "
                                    "use for Botman Api config")
    parser = ArgumentParser()
    parser.add_argument("-c", "--config",
                        dest='path',
                        type=str,
                        default="botmanlib.ini",
                        help="Alternate config file")

    parser.add_argument("-s", "--section",
                        dest="section",
                        type=str,
                        default="botmanlib",
                        help="Name of section in .ini file to "
                             "use for Botman Api config")
    parser.add_argument("-v", "--version", action='version',
                        version='%(prog)s {version}'.format(version=__version__))

    config, unknown = config_parser.parse_known_args()
    here = os.path.abspath(os.path.dirname(config.path))
    config_path = os.path.join(here, config.path)
    os.environ['project_root'] = here

    if os.access(config.path, os.F_OK):
        configs = configparser.ConfigParser()
        configs.read(config_path)
        for key in configs[config.section]:
            if not os.getenv(key, None):
                os.environ[key] = configs[config.section][key]

    sub = parser.add_subparsers(dest='cmd')
    sub.required = True
    sub.add_parser('init', help="Creates config file for library")
    sub.add_parser('startbot', help="Starts bot as normal")
    sub.add_parser('start', help="Start API wrapper for bot")
    sub.add_parser('create_tables', help="Creates bot's and library's database tables")
    sub.add_parser('drop_tables', help="Drops all bot and library's tables from database")

    register_sp = sub.add_parser('register', help="Registers bot in payments API")
    register_sub = register_sp.add_subparsers(dest='payment_system')

    register_sub.required = True
    register_sub.add_parser('fondy')
    register_sub.add_parser('liqpay')
    register_sub.add_parser('freekassa')
    register_sub.add_parser('qiwi')
    register_sub.add_parser('wayforpay')
    register_sp.add_argument('-i', '--id', dest='merchant_id', type=str, help="Merchant id", required=True)
    register_sp.add_argument('-k', '--key', dest='merchant_key', type=str, help="Merchant key", required=True)
    register_sp.add_argument('-w', '--webhook', dest='webhook_url', type=str, help="Webhook url", required=False, default=None)

    scripts = [key.replace('script.', "") for key in os.environ if key.startswith('script')]
    for script_name in scripts:
        sub.add_parser(script_name)

    options = parser.parse_args()
    if options.cmd == 'init':
        init(config_path)
    elif os.access(config.path, os.F_OK):
        if options.cmd == 'start':
            start()
        elif options.cmd == 'create_tables':
            create_tables()
        elif options.cmd == 'drop_tables':
            drop_tables()
        elif options.cmd == 'register':
            register(options.payment_system, options.merchant_id, options.merchant_key, options.webhook_url)
        elif options.cmd == 'startbot':
            startbot()
        else:
            module, func = os.getenv(f"script.{options.cmd}").split(':')
            try:
                lib = importlib.import_module(module)
                if hasattr(lib, func):
                    startfunc = getattr(lib, func)
                    startfunc()
                else:
                    logger.error(f"Can't find {func} function in {module} module")
            except ModuleNotFoundError as e:
                logger.error(str(e))
                exit(1)
    else:
        logger.error(f"Config file \"{config_path}\" does not exits, do you run \"botmanlib init\" command?")
        exit(1)


def generate_token():
    token = hashlib.sha256(str(datetime.datetime.now().timestamp()).encode("utf-8")).hexdigest()
    return token[16:48]


def generate_secret():
    token = hashlib.sha256(str(datetime.datetime.now().timestamp() + random.randint(-10000, 10000)).encode("utf-8")).hexdigest()
    return token[48:55]


def create_tables():
    from .models import create_database_tables
    module, func = os.getenv('bot.entry_point').split(':')
    try:
        importlib.import_module(module)
        create_database_tables()
        print("Database initialized.")
    except ModuleNotFoundError as e:
        traceback.print_exc()
        exit(1)


def drop_tables():
    answer = input("Are you sure to drop all database tables, this action can't be undone? (y/n):")
    while answer != 'y' and answer != 'n':
        print("Please type 'n' or 'y'.")
        answer = input("Are you sure to drop all database tables, this action can't be undone? (y/n):")

    if answer == 'y':
        from botmanlib.models import Database
        Database().drop_all_tables()
        print("All database table deleted")

    if answer == 'n':
        print("Operation canceled")


def init(config_path):
    if not os.access(config_path, os.F_OK):
        template = os.path.join(package_dir, 'templates', 'botmanlib.ini.mako')
        mytemplate = Template(filename=template)

        with open(config_path, 'w') as new_file:
            new_file.write(mytemplate.render(token=generate_token(), secret=generate_secret()))
        logger.info("Created file: \"" + str(config_path) + "\"")

    else:
        logger.warning("File: \"" + str(config_path) + "\" already exist")


def startbot():
    module, func = os.getenv('bot.entry_point').split(':')
    try:
        lib = importlib.import_module(module)
        if hasattr(lib, func):
            startfunc = getattr(lib, func)
            startfunc()
        else:
            logger.error(f"Can't find {func} function in {module} module")
    except ModuleNotFoundError as e:
        traceback.print_exc()
        exit(1)


def register(payment_system, merchant_id, merchant_key, webhook_url=None):
    from botmanlib.menus.helpers import check_payment_api

    if not check_payment_api():
        logger.critical("Api don't working")
        exit(1)

    print("Trying to register bot")
    headers = {"Content-Type": "application/json",
               "X-API-KEY": os.getenv('payments.base_token', "")}

    data = {
        "bot_name": os.getenv('bot.name'),
        "secret": os.getenv('bot.secret'),
        "merchant_id": merchant_id,
        "merchant_key": merchant_key,
        "payment_system": payment_system,
    }
    if webhook_url:
        data.update(webhook_url=webhook_url)

    endpoint = os.getenv('payments.api_url')
    endpoint += ("bots" if endpoint.endswith('/') else "/bots")

    response = requests.post(endpoint, headers=headers, data=json.dumps(data))
    data = json.loads(response.content.decode("utf-8"))
    try:
        if response.status_code == 200 and data['success']:
            print(data['message']['message'])
            print(f"Token: \"{data['data']['token']}\"")
        else:
            print(data['message'])
    except KeyError:
        print(f"Payment service returned {response.status_code} code")


def start():
    from flask import Flask
    from flask_cors import CORS
    from flask_restplus import Api
    from .api.routes import ns
    from .api.security import authorizations

    if not strtobool(os.getenv('api.active', "False")):
        print("api.active was set to False, can't start api")
        return

    if strtobool(os.getenv('bot.save_messages', "False")):
        from .models import create_database_tables
        create_database_tables()
        print("Api tables created.")
    else:
        print("bot.save_messages was set to False, can't create tables")

    app = Flask(__name__)
    app.wsgi_app = ReverseProxied(app.wsgi_app)
    CORS(app)
    api = Api(app,
              version=__version__,
              title='API for bot',
              authorizations=authorizations,
              security=os.getenv('api.token'),
              doc="/" if strtobool(os.getenv('api.documentation')) else False)

    api.namespaces.pop(0)
    api.add_namespace(ns)

    print("DEBUG :" + str(strtobool(os.getenv('api.debug', "False"))))
    print("RELOADER :" + str(strtobool(os.getenv('api.use_reloader', "False"))))

    context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
    context.load_cert_chain(os.getenv('api.cert'), os.getenv('api.cert_key'))

    kwargs = {"host": os.getenv('api.host', "0.0.0.0"),
              "port": os.getenv('api.port'),
              "debug": strtobool(os.getenv('api.debug', "False")),
              "use_reloader": strtobool(os.getenv('api.use_reloader', "False")),
              'ssl_context': context
              }

    if strtobool(os.getenv('bot.autostart', "True")):
        bot = Bot()
        bot.start()
        print("Bot started...")

    print("Api service started...")
    # t = Thread(target=api.app.run, kwargs=kwargs).start()
    app.run(**kwargs)


if __name__ == '__main__':
    main()
