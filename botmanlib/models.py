import datetime
import enum
import gettext
import json
import logging
import os
import traceback
import zlib

from sqlalchemy import Column, Integer, String, Enum, DateTime, orm, text, Table, ForeignKey, BigInteger, ARRAY, TypeDecorator, Text, func, extract
from sqlalchemy import create_engine
from sqlalchemy.event import listen
from sqlalchemy.exc import SQLAlchemyError, DatabaseError
from sqlalchemy.ext.declarative import declarative_base, declared_attr
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import sessionmaker, relationship, object_session
from sqlalchemy.orm.attributes import flag_modified
from sqlalchemy.util import classproperty
from telegram import Message as TelegramMessage, User as TelegramUser
from telegram.ext import Filters

from .utils import Singleton

logger = logging.getLogger(__name__)


class Database(metaclass=Singleton):
    def __init__(self):
        self.logger = logging.getLogger("botmanlib.database")
        pool_size = int(os.getenv('sqlalchemy.pool_size', 5))
        max_overflow = int(os.getenv('sqlalchemy.max_overflow', 10))

        self.database_url = os.getenv('sqlalchemy.url', None)
        self.engine = create_engine(self.database_url, pool_size=pool_size, max_overflow=max_overflow) if self.database_url else None

        self.sessionmaker = sessionmaker(bind=self.engine)
        self.sessions = {}
        self.MessageSession = self.create_session("MessageSession")
        self.DBSession = self.create_session("DBSession")

        self.Base = declarative_base(bind=self.engine)
        self.metadata = self.Base.metadata

    def create_session(self, name):
        self.engine.dispose()  # disposing of all db connections before any work begins, maybe need better solution
        if name in self.sessions:
            return self.sessions[name]
        else:
            new_session = self.sessionmaker()
            self.sessions.update({name: new_session})
            return new_session

    def get_class_by_tablename(self, tablename):
        for c in self.Base._decl_class_registry.values():
            if hasattr(c, '__tablename__') and c.__tablename__ == tablename:
                return c

    def get_class_by_columnname(self, table, columnname):
        if hasattr(table, 'columns'):
            for c in table.columns:
                if str(c) == f"{table.name}.{columnname}":
                    return c

    def drop_all_tables(self):
        sequence_sql = "SELECT sequence_name FROM information_schema.sequences WHERE sequence_schema='public'"

        table_sql = "SELECT table_name FROM information_schema.tables WHERE table_schema='public' AND table_type != 'VIEW' AND table_name NOT LIKE 'pg_ts_%%'"

        for table in [name for (name,) in self.engine.execute(text(table_sql))]:
            try:
                self.engine.execute(text('DROP TABLE %s CASCADE' % table))
            except SQLAlchemyError as e:
                self.logger.error(str(e))

        for seq in [name for (name,) in self.engine.execute(text(sequence_sql))]:
            try:
                self.engine.execute(text('DROP SEQUENCE %s CASCADE' % seq))
            except SQLAlchemyError as e:
                self.logger.error(str(e))


class MessageType(enum.Enum):
    command = 'command'
    text = 'text'
    reply = 'reply'
    audio = 'audio'
    document = 'document'
    photo = 'photo'
    animation = 'animation'
    sticker = 'sticker'
    video = 'video'
    voice = 'voice'
    video_note = 'video_note'
    contact = 'contact'
    location = 'location'
    venue = 'venue'
    forwarded = 'forwarded'
    game = 'game'


class KeyboardType(enum.Enum):
    text = 'text'
    inline = 'inline'


class Sender(enum.Enum):
    user = 'user'
    bot = 'bot'


class TextPickleType(TypeDecorator):
    impl = Text()

    def process_bind_param(self, value, dialect):
        if value is not None:
            value = json.dumps(value)

        return value

    def process_result_value(self, value, dialect):
        if value is not None:
            value = json.loads(value)
        return value


# noinspection PyMethodParameters
class ModelPermissionsBase:

    @classproperty
    def view_permission(cls):
        return f"{cls.__tablename__}_menu_access"

    @classproperty
    def list_permission(cls):
        return f"{cls.__tablename__}_list_menu_access"

    @classproperty
    def add_permission(cls):
        return f"add_{cls.__tablename__}_menu_access"

    @classproperty
    def edit_permission(cls):
        return f"edit_{cls.__tablename__}_menu_access"

    @classproperty
    def delete_permission(cls):
        return f"allow_delete_{cls.__tablename__}"

    @classproperty
    def import_permission(cls):
        return f"import_{cls.__tablename__}_menu_access"

    @classproperty
    def export_permission(cls):
        return f"export_{cls.__tablename__}_menu_access"


class Message(Database().Base):
    __tablename__ = 'messages'

    id = Column(BigInteger, primary_key=True)
    message_id = Column(Integer)
    chat_id = Column(Integer)
    text = Column(String)
    type = Column(Enum(MessageType), nullable=False)
    sender = Column(Enum(Sender), nullable=False)
    audio_name = Column(String)
    video_name = Column(String)
    menu_name = Column(String)  # if sender is bot

    keyboard_id = Column(BigInteger, ForeignKey('keyboards.id'))
    keyboard = relationship("Keyboard", back_populates="messages", uselist=False)

    reply_to_id = Column(BigInteger, ForeignKey('messages.id'))
    reply_to = relationship("Message", back_populates="replies", remote_side=[id], uselist=False)
    replies = relationship("Message", back_populates="reply_to", remote_side=[reply_to_id])

    pressed_button = Column(TextPickleType())

    timestamp = Column(DateTime, default=datetime.datetime.utcnow)

    menu_id = Column(BigInteger, nullable=False)

    def to_dict(self):
        result = {}

        for col in orm.class_mapper(self.__class__).mapped_table.c:
            col_value = getattr(self, col.name)
            if isinstance(col_value, MessageType) or isinstance(col_value, Sender):
                col_value = col_value.value
            elif isinstance(col_value, datetime.datetime) or isinstance(col_value, datetime.date):
                col_value = col_value.isoformat()

            result.update({col.name: col_value})
        return result

    def to_json(self):
        return json.dumps(self.to_dict(), ensure_ascii=False)

    @classmethod
    def create(cls, message, sender, lang, menu_name=None, reply_markup=None):
        session = Database().DBSession

        if message is None or not isinstance(message, TelegramMessage):
            return

        message_obj = cls(chat_id=message.chat_id,
                          text=message.text,
                          sender=sender,
                          message_id=message.message_id,
                          menu_name=menu_name)
        if Filters.text.filter(message):
            # text
            message_obj.type = MessageType.text
        elif Filters.command.filter(message):
            # command
            message_obj.type = MessageType.command
        elif Filters.reply.filter(message):
            # reply
            message_obj.type = MessageType.reply
        elif Filters.audio.filter(message):
            # audio
            message_obj.type = MessageType.audio
        elif Filters.document.filter(message):
            # document
            message_obj.type = MessageType.document
        elif Filters.photo.filter(message):
            # photo
            message_obj.type = MessageType.photo
        elif Filters.sticker.filter(message):
            # sticker
            message_obj.type = MessageType.sticker
        elif Filters.video.filter(message):
            # video
            message_obj.type = MessageType.video
        elif Filters.voice.filter(message):
            # voice
            message_obj.type = MessageType.voice
        elif Filters.video_note.filter(message):
            # video_note
            message_obj.type = MessageType.video_note
        elif Filters.contact.filter(message):
            # contact
            message_obj.type = MessageType.contact
        elif Filters.location.filter(message):
            # location
            message_obj.type = MessageType.location
        elif Filters.venue.filter(message):
            # venue
            message_obj.type = MessageType.venue
        elif Filters.forwarded.filter(message):
            # forwarded
            message_obj.type = MessageType.forwarded
        elif Filters.game.filter(message):
            # game
            message_obj.type = MessageType.game
        else:
            return

        if reply_markup:
            keyboard = Keyboard.create(reply_markup, lang)
            keyboard_obj = session.query(Keyboard).get(keyboard.generate_id())
            if keyboard_obj is not None:
                message_obj.keyboard = keyboard_obj
            else:
                message_obj.keyboard = keyboard

        return message_obj

    @classmethod
    def register_click(cls, callback_query):
        session = Database().DBSession

        # generate keyboard id to find message with same keyboard
        keyboard = Keyboard.create(callback_query.message.reply_markup, callback_query.from_user.language_code)

        message = session.query(Message) \
            .filter(Message.message_id == callback_query.message.message_id) \
            .filter(Message.keyboard_id == keyboard.generate_id()) \
            .order_by(Message.timestamp.desc()) \
            .first()

        if message:
            for button_row in message.keyboard.buttons:
                for button in button_row:
                    if button.get('data', None) == callback_query.data:
                        message.pressed_button = button
                        flag_modified(message, 'pressed_button')
                        session.add(message)
                        break

        try:
            session.commit()
        except DatabaseError as e:
            traceback.print_exc()
            session.rollback()

        return message

    def save(self):
        session = Database().DBSession
        try:
            session.add(self)
            session.commit()
        except DatabaseError as e:
            traceback.print_exc()
            session.rollback()

    def generate_menu_id(self):
        fields = "|".join([self.text, self.type.value, self.sender.value, str(self.audio_name), str(self.video_name), str(self.keyboard_id)])
        self.menu_id = zlib.adler32(fields.encode('utf-8'))


def generate_message_menu_id(mapper, connect, target):
    target.generate_menu_id()


listen(Message, 'before_insert', generate_message_menu_id)


class Keyboard(Database().Base):
    __tablename__ = 'keyboards'

    id = Column(BigInteger, primary_key=True, autoincrement=False)
    type = Column(Enum(KeyboardType), nullable=False)
    language_code = Column(String, nullable=False)
    buttons = Column(ARRAY(TextPickleType()))
    messages = relationship("Message", back_populates='keyboard', uselist=True)

    def generate_id(self):
        self.id = zlib.adler32(f"{self.type.value}|{str(self.buttons)}".encode('utf-8'))
        return self.id

    @classmethod
    def create(cls, reply_markup, lang):
        if hasattr(reply_markup, 'inline_keyboard'):
            buttons_matrix = reply_markup.inline_keyboard
            keyboard = Keyboard(type=KeyboardType.inline, language_code=lang)
        else:
            buttons_matrix = reply_markup.keyboard
            keyboard = Keyboard(type=KeyboardType.text, language_code=lang)

        width = max([len(row) for row in buttons_matrix])
        our_kb_matrix = []
        for y, buttons_row in enumerate(buttons_matrix):
            our_kb_row = []
            for x, button in enumerate(buttons_row):
                kb = {'text': getattr(button, 'text', None),
                      'data': getattr(button, 'callback_data', None),
                      'url': getattr(button, 'url', None)}
                our_kb_row.append(kb)
            if len(our_kb_row) < width:
                for i in range(width - len(our_kb_row)):
                    our_kb_row.append({})
            our_kb_matrix.append(our_kb_row)
        keyboard.buttons = our_kb_matrix
        return keyboard


def generate_keyboard_id(mapper, connect, target):
    target.generate_id()


listen(Keyboard, 'before_insert', generate_keyboard_id)


class UserSessionsMixin:

    @declared_attr
    def sessions(cls):
        return relationship("UserSession", back_populates="user", cascade='all, delete')

    def last_session(self):
        real_user_session = BaseUserSession.get_mapped_model()
        if real_user_session is None:
            return None

        session = object_session(self)
        return session.query(real_user_session).filter(real_user_session.user_id == self.id).order_by(real_user_session.id.desc()).first()

    def update_session(self):
        real_user_session = BaseUserSession.get_mapped_model()
        if real_user_session is None:
            return None

        last_session = self.last_session()
        if last_session:
            session = object_session(last_session)
        else:
            session = object_session(self)

        if last_session is None or datetime.datetime.utcnow() - last_session.end > datetime.timedelta(minutes=30):
            new_session = real_user_session(user_id=self.id)
            session.add(new_session)
        else:
            # update old session
            last_session.end = datetime.datetime.utcnow()
            last_session.actions += 1
            session.add(last_session)
        session.commit()


class BaseUser:
    id = Column(Integer, primary_key=True)
    chat_id = Column(Integer)
    first_name = Column(String)
    last_name = Column(String)
    username = Column(String)
    language_code = Column(String)
    join_date = Column(DateTime, default=datetime.datetime.utcnow)
    block_date = Column(DateTime)
    came_from = Column(String)

    @declared_attr
    def __tablename__(cls):
        return "users"

    @hybrid_property
    def is_active(self):
        return not self.block_date

    @is_active.expression
    def is_active(cls):
        return cls.block_date == None

    def activate(self):
        self.block_date = None

    def deactivate(self):
        self.block_date = datetime.datetime.utcnow()

    @property
    def name(self):
        if self.first_name and self.last_name:
            return f"{self.first_name} {self.last_name}"
        elif self.first_name and not self.last_name:
            return f"{self.first_name}"
        else:
            return ""

    def get_name(self):
        return self.name if self.name else (self.username if self.username else f"User {self.chat_id}")

    @property
    def mention_url(self):
        return f"tg://user?id={self.chat_id}"

    def to_telegram_user(self, bot, **kwargs):
        return TelegramUser(self.chat_id, self.first_name, False, self.last_name, self.username, self.language_code, bot, **kwargs)

    @classmethod
    def get_mapped_model(cls):
        return next((c for c in Database().Base._decl_class_registry.values() if hasattr(c, '__tablename__') and c.__tablename__ == 'users'), None)

    @property
    def translator(self):
        if self.language_code is None:
            translation = gettext.translation('messages', 'locales', languages=['en'], fallback=True)
        else:
            translation = gettext.translation('messages', 'locales', languages=[self.language_code], fallback=True)
        return translation.gettext


class BasePermission(ModelPermissionsBase):
    code = Column(String, primary_key=True)
    _name = Column(String, nullable=False)

    @declared_attr
    def __tablename__(cls):
        return "permissions"

    @property
    def name(self):
        return json.loads(self._name)

    @name.setter
    def name(self, value):
        self._name = json.dumps(value)

    @classmethod
    def create(cls, code, eng_name, session=None):
        from botmanlib.menus.helpers import available_languages, translator
        real_permission = cls.get_mapped_model()
        if real_permission is None:
            return None
        if session is None:
            session = Database().DBSession

        perm = session.query(real_permission).get(code)
        name_dict = {}
        for lang in available_languages():
            t = translator(lang)
            name_dict.update({lang: t(eng_name)})
        if perm is None:
            perm = real_permission(code=code)
            perm.name = name_dict
            session.add(perm)
            return perm
        else:
            perm.name = name_dict
            return perm

    @classmethod
    def get_mapped_model(cls):
        return next((c for c in Database().Base._decl_class_registry.values() if hasattr(c, '__tablename__') and c.__tablename__ == 'permissions'), None)


class UserPermissionsMixin:
    @classproperty
    def users_permissions(cls):
        return Table('users_permissions', Database().metadata,
                     Column('user_id', Integer, ForeignKey('users.id')),
                     Column('permission_code', String, ForeignKey('permissions.code')),
                     extend_existing=True
                     )

    @declared_attr
    def permissions(cls):
        return relationship("Permission",
                            secondary=lambda: cls.users_permissions,
                            primaryjoin=lambda: BaseUser.get_mapped_model().id == cls.users_permissions.c.user_id,
                            secondaryjoin=lambda: BasePermission.get_mapped_model().code == cls.users_permissions.c.permission_code,
                            lazy='joined')

    def has_permission(self, code, ignore_superuser=False):
        permission_codes = [p.code for p in self.permissions]
        if ignore_superuser:
            return code in permission_codes
        else:
            return code in permission_codes or 'superuser' in permission_codes

    def init_permissions(self):
        raise NotImplementedError


class BaseUserSession:
    id = Column(Integer, primary_key=True)
    start = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)
    end = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)
    actions = Column(BigInteger, default=0)

    @declared_attr
    def __tablename__(cls):
        return "user_sessions"

    @hybrid_property
    def duration(self):
        return self.end - self.start

    @duration.expression
    def duration(cls):
        return func.trunc((extract('epoch', cls.end) - extract('epoch', cls.start)) / 60)

    @declared_attr
    def user(cls):
        return relationship("User", back_populates="sessions")

    @declared_attr
    def user_id(cls):
        return Column(Integer, ForeignKey('users.id'))

    @classmethod
    def get_mapped_model(cls):
        return next((c for c in Database().Base._decl_class_registry.values() if hasattr(c, '__tablename__') and c.__tablename__ == 'user_sessions'), None)


def create_database_tables():
    Database().metadata.create_all(Database().engine)
