import os
from distutils.util import strtobool

from telegram import Update, TelegramError
from telegram.error import BadRequest
from telegram.ext import Dispatcher, CallbackContext, DispatcherHandlerStop, CallbackQueryHandler

from botmanlib.menus.helpers import translator
from botmanlib.models import Database, BaseUser, Message


def close_message(update, context):
    try:
        update.effective_message.delete()
    except BadRequest:
        if 'user' in context.user_data:
            _ = context.user_data['user'].translator
        else:
            _ = translator()
        update.callback_query.answer(text=_("Message can't be deleted by bot because it's more than 48 hours old.\nYou can delete it by yourself"), show_alert=True)


class BotmanDispatcher(Dispatcher):

    def __init__(self,
                 bot,
                 update_queue,
                 workers=4,
                 exception_event=None,
                 job_queue=None,
                 persistence=None,
                 use_context=False,
                 use_sessions=False):
        super(BotmanDispatcher, self).__init__(
            bot,
            update_queue,
            workers=workers,
            exception_event=exception_event,
            job_queue=job_queue,
            persistence=persistence,
            use_context=use_context)

        self.use_sessions = use_sessions
        self.add_handler(CallbackQueryHandler(close_message, pattern="^close_message$"))

    def process_update(self, update):
        """Processes a single update.

        Args:
            update (:obj:`str` | :class:`telegram.Update` | :class:`telegram.TelegramError`):
                The update to process.

        """

        def update_user_session(update):
            """Update user session

            Args:
            update (:class:`telegram.Update`):
                The update to process.

            """
            user_model = BaseUser.get_mapped_model()

            if user_model is not None and user_model.sessions and update.effective_chat:
                chat_id = update.effective_chat.id
                user = Database().DBSession.query(user_model).filter_by(chat_id=chat_id).first()
                if user:
                    user.update_session()

        def persist_update(update):
            """Persist a single update.

            Args:
            update (:class:`telegram.Update`):
                The update to process.

            """
            if self.persistence and isinstance(update, Update):
                if self.persistence.store_chat_data and update.effective_chat:
                    chat_id = update.effective_chat.id
                    try:
                        self.persistence.update_chat_data(chat_id,
                                                          self.chat_data[chat_id])
                    except Exception as e:
                        try:
                            self.dispatch_error(update, e)
                        except Exception:
                            message = 'Saving chat data raised an error and an ' \
                                      'uncaught error was raised while handling ' \
                                      'the error with an error_handler'
                            self.logger.exception(message)
                if self.persistence.store_user_data and update.effective_user:
                    user_id = update.effective_user.id
                    try:
                        self.persistence.update_user_data(user_id,
                                                          self.user_data[user_id])
                    except Exception as e:
                        try:
                            self.dispatch_error(update, e)
                        except Exception:
                            message = 'Saving user data raised an error and an ' \
                                      'uncaught error was raised while handling ' \
                                      'the error with an error_handler'
                            self.logger.exception(message)

        # An error happened while polling
        if isinstance(update, TelegramError):
            try:
                self.dispatch_error(None, update)
            except Exception:
                self.logger.exception('An uncaught error was raised while handling the error')
            return

        context = None

        for group in self.groups:
            try:
                for handler in self.handlers[group]:
                    check = handler.check_update(update)
                    if check is not None and check is not False:
                        if not context and self.use_context:
                            context = CallbackContext.from_update(update, self)
                        handler.handle_update(update, self, check, context)
                        persist_update(update)
                        if self.use_sessions:
                            update_user_session(update)
                        if strtobool(os.getenv('bot.save_messages', "False")) and update.callback_query:
                            Message.register_click(update.callback_query)
                        break

            # Stop processing with any other handler.
            except DispatcherHandlerStop:
                self.logger.debug('Stopping further handlers due to DispatcherHandlerStop')
                persist_update(update)
                break

            # Dispatch any error.
            except Exception as e:
                try:
                    self.dispatch_error(update, e)
                except DispatcherHandlerStop:
                    self.logger.debug('Error handler stopped further handlers')
                    break
                # Errors should not stop the thread.
                except Exception:
                    self.logger.exception('An error was raised while processing the update and an '
                                          'uncaught error was raised while handling the error '
                                          'with an error_handler')
