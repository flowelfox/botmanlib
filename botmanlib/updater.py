import logging
from multiprocessing import Event

from telegram.ext import Updater

from botmanlib.dispatcher import BotmanDispatcher

logger = logging.getLogger(__name__)


class BotmanUpdater(Updater):

    def __init__(self,
                 token=None,
                 base_url=None,
                 workers=4,
                 bot=None,
                 private_key=None,
                 private_key_password=None,
                 user_sig_handler=None,
                 request_kwargs=None,
                 persistence=None,
                 use_context=False,
                 use_sessions=False):
        super(BotmanUpdater, self).__init__(token=token,
                                            base_url=base_url,
                                            workers=workers,
                                            bot=bot,
                                            private_key=private_key,
                                            private_key_password=private_key_password,
                                            user_sig_handler=user_sig_handler,
                                            request_kwargs=request_kwargs,
                                            persistence=persistence,
                                            use_context=use_context)

        self.__exception_event = Event()
        self.dispatcher = BotmanDispatcher(
            self.bot,
            self.update_queue,
            job_queue=self.job_queue,
            workers=workers,
            exception_event=self.__exception_event,
            persistence=persistence,
            use_context=use_context,
            use_sessions=use_sessions)
        self.job_queue.set_dispatcher(self.dispatcher)
