import os
from functools import wraps

from flask import request

from .helpers import create_response

authorizations = {
    'token': {
        'type': 'apiKey',
        'in': 'header',
        'name': 'X-API-KEY'
    }
}


def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):

        token = None
        api_token = os.getenv('api.token')

        if 'X-API-KEY' in request.headers:
            token = request.headers['X-API-KEY']

        if not token:
            return create_response(False, {}, 1001), 401

        if token != api_token:
            return create_response(False, {}, 1002), 401

        return f(*args, **kwargs)

    return decorated
