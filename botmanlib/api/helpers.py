# <1000 Info messages
# >1000 Error messages
import datetime
import importlib
import logging
import os
import signal
from enum import Enum
from multiprocessing import Process

from ..utils import Singleton

logger = logging.getLogger(__name__)

messages = {
    1: "Success",
    2: "Bot is running",
    3: "Bot is stopped",

    1001: "Token is missing",
    1002: "Token is wrong",
    1003: "Missing required parameters",
    1004: "User not found",
    1005: "Wrong parameter type or value",
    1006: "Date or time already passed",
    1007: "Table not found",
    1008: "Table column not found",
    1100: "Unknown error"
}


class Bot(metaclass=Singleton):
    def __init__(self):
        func_path = os.getenv('api.bot_entry_point')

        modules_string, func_name = func_path.split(':')

        mod = importlib.import_module(modules_string)

        self.start_func = getattr(mod, func_name)
        self.start_date = None
        self.process = None

    @property
    def running(self):
        return bool(self.process and self.process.is_alive())

    @property
    def uptime(self):
        if self.start_date is not None:
            return datetime.datetime.utcnow() - self.start_date
        else:
            return datetime.timedelta(seconds=0)

    def start(self):
        if self.process is None:
            self.start_date = datetime.datetime.utcnow()
            self.process = Process(name="Bot", target=self.start_func)
            self.process.start()

    def stop(self):
        if self.process:
            os.kill(self.process.pid, signal.SIGINT)
            self.process.join()
            self.process = None
            self.start_date = None

    def kill(self):
        if self.process:
            os.kill(self.process.pid, signal.SIGTERM)
            self.process.join()
            self.process = None
            self.start_date = None


def create_response(success, data, message_code):
    response_data = {
        "success": success,
        "data": data,
        "message": {"code": message_code, "message": messages[message_code]}
    }

    if message_code > 1000:
        logger.debug(f"Error response details:\n{response_data}")

    return response_data


def prepare_column_value(value):
    # prepare column data for json serialization
    if isinstance(value, datetime.datetime) or isinstance(value, datetime.date):
        return value.isoformat()
    elif isinstance(value, Enum):
        return value.value
    else:
        return value