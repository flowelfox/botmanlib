import datetime
import json
import logging
import os
import traceback
from distutils.util import strtobool

from dateutil.parser import parse
from dateutil.tz import tzutc
from flask_restplus import Resource, reqparse
from sqlalchemy import func
from sqlalchemy.exc import DatabaseError
from . import APISession as session
from telegram import Bot as TelegramBot
from telegram.error import BadRequest, TimedOut

from . import ns
from ..helpers import create_response
from ..security import token_required
from ...models import Database

logger = logging.getLogger(__name__)


@ns.route('/bot/distribution')
class Distribution(Resource):

    @token_required
    @ns.doc(security="token", params={'text': 'Message text to send',
                                      'when': "Date and time in ISO 8601 format, example 2019-04-23T18:25:43.511Z+00:00",
                                      'web_preview': "True means enable web preview, False means disable web preview",
                                      'notification': "True means enable notification for message, False means to disable notification for message.",
                                      'parse_mode': "MARKDOWN or HTML"})
    @ns.response(201, "Created")
    @ns.response(401, "Your token is missing or incorrect")
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('text', default=None)
        parser.add_argument('when', default=None)
        parser.add_argument('web_preview', default="True")
        parser.add_argument('notification', default="True")
        parser.add_argument('parse_mode', default=None)

        try:
            args = parser.parse_args()
            if not args.text:
                raise AttributeError
            text = args.text
            now = datetime.datetime.utcnow().replace(tzinfo=tzutc())
            when = parse(args.when) if args.when is not None else now
            if when < now:
                return create_response(False, {}, 1006), 400
            web_preview = strtobool(args.web_preview)
            notification = strtobool(args.notification)
            parse_mode = args.parse_mode
            if parse_mode not in [None, "HTML", "MARKDOWN"]:
                raise ValueError
        except ValueError:
            return create_response(False, {}, 1005), 400
        except AttributeError:
            return create_response(False, {}, 1003), 400

        bot_token = os.getenv('bot.token')
        bot = TelegramBot(token=bot_token)
        User = Database().get_class_by_tablename('users')

        try:
            users = session.query(User).filter(User.block_date == None).all()
            for user in users:
                try:
                    bot.send_message(user.chat_id, text, disable_web_page_preview=not web_preview, disable_notification=not notification, parse_mode=parse_mode)
                except (BadRequest, TimedOut):
                    logger.warning(f"Can't send message to {user.username}")
            return create_response(True, {}, 1), 201
        except DatabaseError:
            session.rollback()
            return create_response(False, {}, 1100), 500
        finally:
            session.close()


@ns.route('/bot/messages')
class MessagesHistory(Resource):

    @token_required
    @ns.doc(security="token", params={'chat_id': "Chat id of user to get messages history from",
                                      'date': 'Messages date in YYYY-MM-DD format',
                                      'limit': "How many messages return"})
    @ns.response(200, "Success")
    @ns.response(401, "Your token is missing or incorrect")
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('chat_id')
        parser.add_argument('date', default=datetime.date.today().strftime("%Y-%m-%d"))
        parser.add_argument('limit', default=10)

        try:
            args = parser.parse_args()
            if not args.chat_id or not args.date:
                raise AttributeError
            chat_id = args.chat_id
            date = datetime.datetime.strptime(args.date, "%Y-%m-%d").date()
            limit = int(args.limit)
            if limit <= 0:
                raise ValueError
        except ValueError:
            return create_response(False, {}, 1005), 400
        except AttributeError:
            return create_response(False, {}, 1003), 400

        User = Database().get_class_by_tablename('users')
        if User is None:
            return create_response(False, {}, 1007)

        try:
            user = session.query(User) \
                .filter(User.chat_id == chat_id) \
                .first()

            if user:
                HistoryMessage = Database().get_class_by_tablename('messages')

                messages = session.query(HistoryMessage) \
                    .filter(HistoryMessage.chat_id == chat_id) \
                    .filter(func.date(HistoryMessage.timestamp) == date) \
                    .order_by(HistoryMessage.timestamp.desc()) \
                    .limit(limit)

                response_data = json.dumps([m.to_dict() for m in messages], ensure_ascii=False)
                return create_response(True, response_data, 1)
            else:
                return create_response(False, {}, 1004)
        except DatabaseError as e:
            session.rollback()
            logger.error(traceback.format_exc())
            return create_response(False, {}, 1100)
        finally:
            session.close()


@ns.route('/bot/messages/send')
class SendMessage(Resource):

    @token_required
    @ns.doc(security="token", params={'chat_id': "Chat id of user to send message to", 'text': 'Message text to send'})
    @ns.response(201, "Created")
    @ns.response(401, "Your token is missing or incorrect")
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('chat_id')
        parser.add_argument('text')
        try:
            args = parser.parse_args()
            if not args.text or not args.chat_id:
                raise AttributeError
            chat_id = args.chat_id
            text = args.text
        except ValueError:
            return create_response(False, {}, 1005), 400
        except AttributeError:
            return create_response(False, {}, 1003), 400

        bot_token = os.getenv('bot.token', None)

        bot = TelegramBot(token=bot_token)
        User = Database().get_class_by_tablename('users')

        try:
            user = session.query(User) \
                .filter(User.is_active == True) \
                .filter(User.chat_id == chat_id) \
                .first()
            if user:
                try:
                    bot.send_message(user.chat_id, text)
                    return create_response(True, {}, 1), 201
                except (BadRequest, TimedOut):
                    return create_response(False, {}, 1100), 408
            else:
                return create_response(False, {}, 1004), 404
        except DatabaseError:
            session.rollback()
            return create_response(False, {}, 1100), 500
        finally:
            session.close()
