import logging

from flask_restplus import Resource

from ..helpers import create_response
from . import ns

logger = logging.getLogger(__name__)


@ns.route('/status')
class Status(Resource):

    @ns.doc(security=None)
    @ns.response(200, "Success")
    def get(self):
        return create_response(True, {"status": True}, 1)
