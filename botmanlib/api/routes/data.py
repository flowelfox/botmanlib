import logging

from flask_restplus import Resource, reqparse
from sqlalchemy import func, inspect
from sqlalchemy.exc import DatabaseError
from . import APISession as session

from . import ns
from ..helpers import create_response, prepare_column_value
from ..security import token_required
from ...models import Database

logger = logging.getLogger(__name__)


@ns.route('/bot/data/<string:table>')
class Data(Resource):
    max_limit = 500

    @token_required
    @ns.doc(security="token", params={'columns': 'List of columns to get data from (Must be "," between values)',
                                      'limit': 'How many table entries to return',
                                      'offset': 'How many entries to skip',
                                      'ordering': "1 For ascending, -1 for descending"})
    @ns.response(200, "Success")
    @ns.response(401, "Your token is missing or incorrect")
    def get(self, table):
        parser = reqparse.RequestParser()
        parser.add_argument('columns', default=None)
        parser.add_argument('limit', default=self.max_limit)
        parser.add_argument('offset', default=0)
        parser.add_argument('ordering', default=1)

        try:
            args = parser.parse_args()
            if args.columns:
                columns = sorted([column.strip() for column in args.columns.split(",")])
            else:
                columns = None

            limit = int(args.limit)
            if limit > self.max_limit:
                limit = self.max_limit
            offset = int(args.offset)
            ordering = int(args.ordering)
        except ValueError:
            return create_response(False, {}, 1005), 400
        except AttributeError:
            return create_response(False, {}, 1003), 400

        model = Database().get_class_by_tablename(table)
        if model is None:
            return create_response(False, {}, 1007), 400

        model_ins = inspect(model)
        primary_key = model_ins.primary_key[0]
        if columns:
            model_columns = [c for c in model_ins.columns if c.key in columns]
        else:
            model_columns = list(model_ins.columns)

        try:
            result = []
            data_query = session.query(*model_columns)
            if ordering > 0:
                data_query = data_query.order_by(primary_key.asc())
            else:
                data_query = data_query.order_by(primary_key.desc())

            if limit:
                data_query = data_query.limit(limit)
            if offset:
                data_query = data_query.offset(offset)

            db_data = data_query.all()
            if not db_data:
                return create_response(True, [], 1)

            for row in db_data:
                one_data_dict = {}
                for idx, col in enumerate(row):
                    col = prepare_column_value(col)
                    one_data_dict.update({model_columns[idx].key: col})
                result.append(one_data_dict)
            return create_response(True, result, 1)
        except DatabaseError:
            session.rollback()
            return create_response(False, {}, 1100)
        finally:
            session.close()


@ns.route('/bot/data/<table>/<table_id>')
class SpecificData(Resource):

    @token_required
    @ns.doc(security="token", params={'columns': 'List of columns to get data from (Must be "," between values)'})
    @ns.response(200, "Success")
    @ns.response(401, "Your token is missing or incorrect")
    def get(self, table, table_id):
        parser = reqparse.RequestParser()
        parser.add_argument('columns', default=None)

        try:
            args = parser.parse_args()
            table_id = int(table_id)
            columns = args.columns
        except ValueError:
            return create_response(False, {}, 1005), 400
        except AttributeError:
            return create_response(False, {}, 1003), 400

        model = Database().get_class_by_tablename(table)
        if model is None:
            return create_response(False, {}, 1007), 400

        model_ins = inspect(model)
        primary_key = model_ins.primary_key[0]
        if columns:
            model_columns = [c for c in model_ins.columns if c.key in columns]
        else:
            model_columns = list(model_ins.columns)

        try:
            data_query = session.query(*model_columns)
            data_query = data_query.filter(primary_key == table_id)
            row = data_query.first()
            if not row:
                return create_response(True, {}, 1)

            one_data_dict = {}
            for idx, col in enumerate(row):
                col = prepare_column_value(col)

                one_data_dict.update({model_columns[idx].key: col})
            return create_response(True, one_data_dict, 1)
        except DatabaseError:
            session.rollback()
            return create_response(False, {}, 1100)
        finally:
            session.close()


@ns.route('/bot/data/<table>/count')
class CountData(Resource):

    @token_required
    @ns.doc(security="token", params={})
    @ns.response(200, "Success")
    @ns.response(401, "Your token is missing or incorrect")
    def get(self, table):
        model = Database().get_class_by_tablename(table)
        if model is None:
            return create_response(False, {}, 1007), 400

        model_ins = inspect(model)
        primary_key = model_ins.primary_key[0]
        try:
            data_query = session.query(func.count(primary_key))
            db_data = data_query.scalar()

            if not db_data:
                return create_response(True, {}, 1)

            return create_response(True, db_data, 1)
        except DatabaseError:
            session.rollback()
            return create_response(False, {}, 1100)
        finally:
            session.close()
