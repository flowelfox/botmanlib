import logging

from flask_restplus import Resource, reqparse
from sqlalchemy import inspect
from sqlalchemy.exc import DatabaseError
from . import APISession as session

from . import ns
from ..helpers import create_response, prepare_column_value
from ..security import token_required
from ...models import Database

logger = logging.getLogger(__name__)


@ns.route('/bot/users')
class Users(Resource):
    max_limit = 1000

    @token_required
    @ns.doc(security="token", params={'limit': 'How many table entries to return',
                                      'offset': 'How many entries to skip',
                                      'ordering': "1 For ascending, -1 for descending"})
    @ns.response(200, "Success")
    @ns.response(401, "Your token is missing or incorrect")
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('limit', default=self.max_limit)
        parser.add_argument('offset', default=0)
        parser.add_argument('ordering', default=1)

        try:
            args = parser.parse_args()
            limit = int(args.limit)
            if limit > self.max_limit:
                limit = self.max_limit
            offset = int(args.offset)
            ordering = int(args.ordering)
        except ValueError:
            return create_response(False, {}, 1005), 400
        except AttributeError:
            return create_response(False, {}, 1003), 400

        model = Database().get_class_by_tablename('users')
        if model is None:
            return create_response(False, {}, 1007), 400

        model_ins = inspect(model)
        primary_key = model_ins.primary_key[0]
        model_columns = list(model_ins.columns)
        try:
            result = []
            users_query = session.query(*model_columns)
            if ordering > 0:
                users_query = users_query.order_by(primary_key.asc())
            else:
                users_query = users_query.order_by(primary_key.desc())

            if limit:
                users_query = users_query.limit(limit)
            if offset:
                users_query = users_query.offset(offset)

            users = users_query.all()
            if not users:
                return create_response(True, [], 1)

            for row in users:
                one_data_dict = {}
                for idx, col in enumerate(row):
                    col = prepare_column_value(col)
                    one_data_dict.update({model_columns[idx].key: col})
                result.append(one_data_dict)
            return create_response(True, result, 1)
        except DatabaseError:
            session.rollback()
            return create_response(False, {}, 1100)
        finally:
            session.close()


@ns.route('/bot/users/<user_id>')
class SpecificUser(Resource):

    @token_required
    @ns.doc(security="token", params={})
    @ns.response(200, "Success")
    @ns.response(401, "Your token is missing or incorrect")
    def get(self, user_id):
        parser = reqparse.RequestParser()
        try:
            args = parser.parse_args()
            user_id = int(user_id)
        except ValueError:
            return create_response(False, {}, 1005), 400
        except AttributeError:
            return create_response(False, {}, 1003), 400

        model = Database().get_class_by_tablename("users")
        if model is None:
            return create_response(False, {}, 1007), 400

        model_ins = inspect(model)
        primary_key = model_ins.primary_key[0]
        model_columns = list(model_ins.columns)

        try:
            users_query = session.query(*model_columns)
            users_query = users_query.filter(primary_key == user_id)
            row = users_query.first()
            if not row:
                return create_response(True, {}, 1)

            one_data_dict = {}
            for idx, col in enumerate(row):
                col = prepare_column_value(col)
                one_data_dict.update({model_columns[idx].key: col})
            return create_response(True, one_data_dict, 1)
        except DatabaseError:
            session.rollback()
            return create_response(False, {}, 1100)
        finally:
            session.close()