import logging

from flask_restplus import Resource
from sqlalchemy import func, extract
from sqlalchemy.exc import DatabaseError
from . import APISession as session

from . import ns
from ..helpers import create_response, Bot
from ..security import token_required
from ...models import Database, BaseUser, BaseUserSession

logger = logging.getLogger(__name__)


@ns.route('/bot/start')
class Start(Resource):

    @token_required
    @ns.doc(security="token")
    @ns.response(200, "Success")
    @ns.response(401, "Your token is missing or incorrect")
    def post(self):
        bot = Bot()
        if bot.running:
            return create_response(False, {}, 2)
        else:
            bot.start()
            return create_response(True, {}, 1)


@ns.route('/bot/stop')
class Stop(Resource):

    @token_required
    @ns.doc(security="token")
    @ns.response(200, "Success")
    @ns.response(401, "Your token is missing or incorrect")
    def post(self):
        bot = Bot()
        if bot.running:
            bot.stop()
            return create_response(True, {}, 1)
        else:
            return create_response(False, {}, 3)


@ns.route('/bot/kill')
class Kill(Resource):

    @token_required
    @ns.doc(security="token")
    @ns.response(200, "Success")
    @ns.response(401, "Your token is missing or incorrect")
    def post(self):
        bot = Bot()
        if bot.running:
            bot.kill()
            return create_response(True, {}, 1)
        else:
            return create_response(False, {}, 3)


@ns.route('/bot/restart')
class Restart(Resource):

    @token_required
    @ns.doc(security="token")
    @ns.response(200, "Success")
    @ns.response(401, "Your token is missing or incorrect")
    def post(self):
        bot = Bot()
        if bot.running:
            bot.stop()
            bot.start()
            return create_response(True, {}, 1)
        else:
            return create_response(False, {}, 3)


@ns.route('/bot/status')
class Status(Resource):

    @token_required
    @ns.doc(security="token")
    @ns.response(200, "Success")
    @ns.response(401, "Your token is missing or incorrect")
    def get(self):
        bot = Bot()
        if bot.running:
            return create_response(True, {"status": True}, 1)
        else:
            return create_response(True, {"status": False}, 1)


@ns.route('/bot/uptime')
class Status(Resource):

    @token_required
    @ns.doc(security="token")
    @ns.response(200, "Success")
    @ns.response(401, "Your token is missing or incorrect")
    def get(self):
        bot = Bot()
        return create_response(True, {"uptime": round(bot.uptime.total_seconds())}, 1)


@ns.route('/bot/start-date')
class Status(Resource):

    @token_required
    @ns.doc(security="token")
    @ns.response(200, "Success")
    @ns.response(401, "Your token is missing or incorrect")
    def get(self):
        bot = Bot()
        return create_response(True, {"start_date": bot.start_date.isoformat() if bot.start_date else None}, 1)


@ns.route('/bot/tables')
class Tables(Resource):

    @token_required
    @ns.doc(security="token")
    @ns.response(200, "Success")
    @ns.response(401, "Your token is missing or incorrect")
    def get(self):
        tables = {}
        for t in Database().metadata.sorted_tables:
            tables.update({t.name: [str(n).replace(f"{t.name}.", '') for n in t.columns]})

        return create_response(True, tables, 1)


@ns.route('/bot/stats')
class Stats(Resource):
    @ns.doc(security="token")
    @token_required
    @ns.response(200, "Success")
    @ns.response(401, "Your token is missing or incorrect")
    def get(self):
        users_model = BaseUser.get_mapped_model()
        session_model = BaseUserSession.get_mapped_model()

        if users_model is None:
            return create_response(False, {"table": "users"}, 1007), 400
        if session_model is None:
            return create_response(False, {"table": "user_sessions"}, 1007), 400

        try:
            number_of_users = session.query(func.count(users_model.id)).scalar()
            number_of_users_who_blocked_bot = session.query(func.count(users_model.id)).filter(users_model.block_date != None).scalar()
            number_of_sessions = session.query(func.count(session_model.id)).scalar()
            avg_session_duration = session.query(func.avg(func.trunc((extract('epoch', session_model.end) - extract('epoch', session_model.start)) / 60)).label('duration_avg')).scalar()
            avg_session_actions = session.query(func.avg(session_model.actions).label('actions_avg')).scalar()

            number_of_users = 0 if number_of_users is None else number_of_users
            number_of_users_who_blocked_bot = 0 if number_of_users_who_blocked_bot is None else number_of_users_who_blocked_bot
            number_of_sessions = 0 if number_of_sessions is None else number_of_sessions
            avg_session_duration = 0 if avg_session_duration is None else avg_session_duration
            avg_session_actions = 0 if avg_session_actions is None else avg_session_actions

            stats = {'number_of_users': number_of_users,
                     'number_of_users_who_blocked_bot': number_of_users_who_blocked_bot,
                     'number_of_sessions': number_of_sessions,
                     'avg_session_duration': float(avg_session_duration),
                     'avg_session_actions': float(avg_session_actions)
                     }

            return create_response(True, stats, 1)
        except DatabaseError as e:
            logger.error(e)
            session.rollback()
            return create_response(False, {}, 1100)
        finally:
            session.close()
