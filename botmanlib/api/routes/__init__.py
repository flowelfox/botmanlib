from flask_restplus import Namespace
from botmanlib.models import Database

ns = Namespace('v1', 'Api CLIENT')

APISession = Database().create_session("APISession")
from . import api
from . import bot
from . import data
from . import users
from . import messages
