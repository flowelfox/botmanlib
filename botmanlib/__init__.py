from os import path

__version__ = '1.8.5'

package_dir = path.abspath(path.dirname(__file__))
