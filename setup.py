from setuptools import setup, find_packages
from botmanlib import __version__


def install_deps():
    default = open('requirements.txt', 'r').readlines()
    new_pkgs = []
    dependency_links = []
    for resource in default:
        if 'git+ssh' in resource or 'git+https' in resource:
            dependency_links.append(resource.strip())
            new_pkgs.append(resource[resource.index('egg=')+4:].strip())
        else:
            new_pkgs.append(resource.strip())
    return new_pkgs, dependency_links


pkgs, dependency_links = install_deps()

setup(
    name='botmanlib',
    version=__version__,
    description='Library for botman bots',
    url='https://gitlab.com/botman_bots/botmanlib',
    author='Flowelcat',
    author_email='flowelcat@gmail.com',
    license='GNU',
    packages=find_packages(),
    package_data={'botmanlib': ['templates/*.mako']},
    include_package_data=True,
    install_requires=pkgs,
    dependency_links=dependency_links,
    python_requires='>3.6.0',
    entry_points=dict(console_scripts=[
        'botmanlib = botmanlib.app:main',
    ]),
)
