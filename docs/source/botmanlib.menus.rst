Menus
=====
.. toctree::
    botmanlib.menus.base_menu
    botmanlib.menus.list_menus
    botmanlib.menus.ready_to_use
